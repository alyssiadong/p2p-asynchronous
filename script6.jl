# include("PowerNetwork.jl")
# include("CommunicationDelays.jl")
# include("market_sim.jl")

using PlotlyJS
using ORCA
using JLD, HDF5
using DataFrames
using Statistics
using Printf

include("write_results.jl")

# # Verify the convergence rate of the points saved with sampling_period = 1
# # compared to the ones saved with sampling_period = 10.

######################################
# # Test with sampling_period = 10
######################################

# JLDfile = WR.JLDFile("n110_p30_c80_0x000f_all.jld",
#       "D:/Julia/n110_p30_c80_0x000f_all.jld", 
#       "n110_p30_c80_0x000f.csv")

# Eprim_rel2 = 1e-9
# par = PowerNetwork.power_network(JLDfile.testcasefile, "full_P2P")
# pmin = par.Pmin
# pmax = par.Pmax
# optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
# Eprim_abs2 = Eprim_rel2*optimal_power_exchange

# sigmas = range(0.0, 1.0, length=10)
# df = Df[(Df.tirages .==1) .& (Df.sigma .== sigmas[3]) .& (Df.delta .==1),:]
# res_dict = WR.readRes(JLDfile, df, ["primal_res"])

# NID = df.NID[1]
# prim_res = res_dict[NID]["primal_res"]

# sum_prim_res = dropdims(sum(prim_res, dims=1), dims=1)

# plot(log10.(sum_prim_res), )

######################################
# # Test with sampling_period = 1
######################################

# JLDfile = WR.JLDFile("n110_p30_c80_0x000f_deltasweep_test.jld",
#       "D:/Julia/n110_p30_c80_0x000f_deltasweep_test.jld", 
#       "n110_p30_c80_0x000f.csv")

# Df = JLDfile.df_params
# dict_res = WR.readRes(JLDfile, Df, ["primal_res"])

# data = GenericTrace[]
# for (nid, dic) in dict_res
# 	prim_res = log10.(dropdims(sum(dic["primal_res"], dims=1), dims=1))
# 	trace = scatter(;y=prim_res, name = string(nid))
# 	push!(data, trace)
# end
# plot(data)

######################################
# # Test with the uncorrect saved data at convergence
######################################

Eprim_rel2 = 1e-9
par = PowerNetwork.power_network(JLDfile.testcasefile, "full_P2P")
pmin = par.Pmin
pmax = par.Pmax
optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
Eprim_abs2 = Eprim_rel2*optimal_power_exchange

fid = jldopen("D:/Julia/n110_p30_c80_0x000f_deltasweep.jld", "r")
	Df = read(fid, "param_dataframe")
	dict_res = read(fid, "202")
close(fid)