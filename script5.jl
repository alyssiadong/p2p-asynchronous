# include("PowerNetwork.jl")
# include("CommunicationDelays.jl")
# include("market_sim.jl")
include("write_results.jl")
using DataFrames
using JLD

market_name = "n9_p3_c6_0x0004"

function inSet(a::Number, A::Array{<:Number,1})
	return a ∈ A
end

JLDfile = WR.JLDFile(string(market_name,".jld"); add_param_values=false)
df = JLDfile.df_params
df2 = df[df.sigma.==0.5,:]
df3 = df2[map(x->inSet(x, [0.1, 0.3, 0.5, 0.7, 1]), df2.delta), :]

pl = WR.readResidual(JLDfile, df3)

# ORCA.savefig(pl, "images/resolution_asynchrone.pdf"; format = "pdf")