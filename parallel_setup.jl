using Distributed

#addprocs(3)
@everywhere include("PowerNetwork.jl")
@everywhere include("CommunicationDelays.jl")
@everywhere include("Planification.jl")
@everywhere include("market_sim.jl")
@everywhere using DataFrames

include("write_results.jl")
using JLD

# # N_prodcons = [3,6]
# N_prodcons = [30,80]
# # N_prodcons = [10,21]
# type = "full_P2P"

# N_alpha = 1
# N_beta = 1
# N_sigma = 1
# N_delta = 1
# N_tirages = 1
# N_gamma = 1

# A = (N_alpha ≠ 1) ? range(0.0, 30.0, length=N_alpha) : (6.0:6.0)
# B = (N_beta ≠ 1) ? range(0.0, 10.0, length=N_beta) : (0.0:0.0)
# S = (N_sigma ≠ 1) ? range(0.0, 1.0, length=N_sigma) : (0.0:0.0)
# D = (N_delta ≠ 1) ? range(0.0, 1.0, length=N_delta+1)[2:end] : (1.0:1.0)
# # D = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
# # D = [0.5, 1]
# # D = [0.2, 0.7, 0.8]
# # D = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09,
# # 		0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19,
# # 		0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29,
# # 		0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39,
# # 		0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49,
# # 		0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59,
# # 		0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69,
# # 		0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79,
# # 		0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89,]
# T = (1:N_tirages)
# # T = (2:N_tirages+1)
# # G = (N_gamma ≠ 1) ? 10 .^(range(-12,0,length=N_gamma)) : (1e-9:1e-9)
# # G = [ 0.0001, 0.001, 
# # 	0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09,
# # 	0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
# # 	2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0,]
# G = [1.0]

# # par = PowerNetwork.power_network("n9_p3_c6_0x0004.csv", type)
# par = PowerNetwork.power_network("n110_p30_c80_0x000f.csv", type)
# rho = par.preferred_rho	
# ΔT = 8000.0
# sendType = "sendback"
# iterMatching = "yesmatching"
# priceUpdate = "new"
# tradeUpdate = "new"
# δt = 10
# Tsimu = 3000
# # OSQPeps = [1e-3, 1e-5]
# OSQPeps=1e-5

# filename = string(par.filename[1:end-4],".jld")
# # JLDfile = WR.JLDFile(filename, Dict(	
# # 										"alpha"	=> collect(A),
# # 										"beta" => collect(B),
# # 										"sigma" => collect(S), 
# # 										"delta" => collect(D),
# # 										"tirages" => collect(T),
# # 										"gamma" => collect(G),
# # 										"penalty_factor" => [rho],
# # 										"DeltaT" => [ΔT],
# # 										"sendType" => [sendType],
# # 										"iterMatching" => [iterMatching], 
# # 										"priceUpdate" => [priceUpdate], 
# # 										"tradeUpdate" => [tradeUpdate], 
# # 										"sampling_period"=> [δt],
# # 										"Tsimu" => [Tsimu],
# # 										"OSQPeps" => [OSQPeps]
# # 									);
# # 					)
# JLDfile = WR.JLDFile("n110_p30_c80_0x000f_alphabeta.jld",
# 		"D:/Julia/n110_p30_c80_0x000f_alphabeta.jld", 
# 		"n110_p30_c80_0x000f.csv")
# Ndone = JLDfile.Ndone
# DF = JLDfile.df_params[Ndone+1:end, :]
# # N_points = size(DF, 1)
# # N_points = 1
# # Df = JLDfile.df_params
# # DF = Df[Df.NID .== 26,:]
# f = jldopen(JLDfile.filepath, "r")
# Df = read(f, "param_dataframe")
# DF = Df[(Df.NID.==426).|(Df.NID.==431).| (Df.NID.==436).|
# 			(Df.NID.==427).|(Df.NID.==432).| (Df.NID.==437).|
# 			(Df.NID.==428).|(Df.NID.==433).| (Df.NID.==438).|
# 			(Df.NID.==429).|(Df.NID.==434).| (Df.NID.==439).|
# 			(Df.NID.==430).|(Df.NID.==435).| (Df.NID.==440),:] 

# N_points = size(DF,1)

# close(f)


############################################################
# n31_p10_c21_0xeeee
############################################################
# par = PowerNetwork.power_network("n31_p10_c21_0xeeee.csv", type)
# rho = par.preferred_rho	
# ΔT = 8000.0
# sendType = "sendback"
# iterMatching = "yesmatching"
# priceUpdate = "new"
# tradeUpdate = "new"
# δt = 5
# Tsimu = 1500
# # OSQPeps = [1e-3, 1e-5]
# OSQPeps=1e-5

# filename = string(par.filename[1:end-4],".jld")
# JLDfile = WR.JLDFile(filename, Dict(	
# 										"alpha"	=> collect(A),
# 										"beta" => collect(B),
# 										"sigma" => collect(S), 
# 										"delta" => collect(D),
# 										"tirages" => collect(T),
# 										"gamma" => collect(G),
# 										"penalty_factor" => [rho],
# 										"DeltaT" => [ΔT],
# 										"sendType" => [sendType],
# 										"iterMatching" => [iterMatching], 
# 										"priceUpdate" => [priceUpdate], 
# 										"tradeUpdate" => [tradeUpdate], 
# 										"sampling_period"=> [δt],
# 										"Tsimu" => [Tsimu],
# 										"OSQPeps" => [OSQPeps]
# 									);
# 					)
# Ndone = JLDfile.Ndone
# DF = JLDfile.df_params[Ndone+1:end, :]
# N_points = size(DF, 1)
# 
############################################################
# n9_p3_c6_0xcccd
############################################################
N_prodcons = [3,6]
type = "full_P2P"
par = PowerNetwork.power_network("n9_p3_c6_0xcccd.csv", type)
mean_dist = sum(par.comm_graph.weights)/(3*6*2)
mean_delay = 0.5:0.5
# rho = par.preferred_rho	
rho = 2
ΔT = 8000.0
sendType = "sendback"
iterMatching = "yesmatching"
priceUpdate = "new"
tradeUpdate = "new"
δt = 0.05
Tsimu = 1500
OSQPeps=1e-10

S = 0.01:0.01
D = [0.1, 0.4, 0.6, 0.8, 1]
T = 1:1
G = 0.1:0.1

alphas = [0,0.5,1]

planStruct = Planification.planStruct(par)

filename = string(par.filename[1:end-4],".jld")
JLDfile = WR.JLDFile(filename, Dict(	
										"alpha"	=> collect(alphas),
										"mean_delay" => collect(mean_delay),
										"sigma" => collect(S), 
										"delta" => collect(D),
										"tirages" => collect(T),
										"gamma" => collect(G),
										"penalty_factor" => [rho],
										"DeltaT" => [ΔT],
										"sendType" => [sendType],
										"iterMatching" => [iterMatching], 
										"priceUpdate" => [priceUpdate], 
										"tradeUpdate" => [tradeUpdate], 
										"sampling_period"=> [δt],
										"Tsimu" => [Tsimu],
										"OSQPeps" => [OSQPeps]
									);
					)
Ndone = JLDfile.Ndone
DF = JLDfile.df_params[Ndone+1:end, :]
N_points = size(DF, 1)