module WR
# WR for write results

	using HDF5
	using JLD
	using PlotlyJS
	using DataFrames
	using DataStructures
	using Statistics
    using LightGraphs, SimpleWeightedGraphs
	using ..PowerNetwork
	using ..MarketSimulation

	import Base.show

    ################################################################################
    # Structure Julia Data File
    ################################################################################

    struct NotImplementedYet <: Exception end

	mutable struct JLDFile
		filename::String
		filepath::String
		testcasefile::String
		df_params::DataFrame
		params::Dict{String, <:Array{T,1} where T}
		Ndone::Int64

		function JLDFile(filename::String, 
			params_dict_input::Dict{String, <:Array{<:Any,1}} = Dict{String, Array{Float64,1}}();
			add_param_values::Bool = true,
			new_param_values::Array{<:Number,1} = Array{Float64,1}() )

			@assert endswith(filename, ".jld") "'filename' must be an JLD file."
			filepath = string("results/",filename)
			testcasefile = string(filename[1:end-4], ".csv")

			if isfile(filepath)
				# Read already existing values
				jldopen(filepath, "r") do f
					Df = read(f,"param_dataframe")
					params_dict = read(f, "param_dict")
					Ndone = read(f, "Ndone")
				end

				# If the file already exists, ask if there is a new value to add.
				println(filepath, " already exists.")
				if add_param_values
					ans = ""
					ans2 = ""
					while ans ∉ ["y", "n"]
						print("    Do you want to add a parameter value ? ('y'/'n'): ")
						ans = readline()
					end
					
					if ans == "y"
						# If there is a new value, ask which parameter to modify, or if there is a new parameter to add
						params_dict = params_dict_input

						# Print the already existing parameters and their values :					
						println("    Choose an existing parameter or create a new one :")
						display(params_dict)
						names_params = push!(string.(keys(params_dict)), "new")
						while ans2 ∉ names_params
							print("    Type the parameter name or 'new' : ")
							ans2 = readline()
						end
						new_param = ans2

						if new_param == "new"
							# Ask which was the parameter value of the already existing data.
							print("    Which was the parameter value of the already existing data (number only) ? : ")
							ans3 = readline()
							new_param_oldvalue = parse(Float64, ans3)
							throw(NotImplementedYet())
							## TO COMPLETE
						elseif new_param_values ≠ Array{Float64,1}()
							params_dict, Df = addValueParam(new_param, new_param_values, params_dict, Df)

							# Modify JLD file
							jldopen(filepath, "r+") do f 
								delete!(f, "param_dataframe")
								delete!(f, "param_dict")

								f["param_dataframe"] = Df
								f["param_dict"] = params_dict
							end
						else 
							params_dict, Df = addValueParam(params_dict, Df)

							# Modify JLD file
							jldopen(filepath, "r+") do f 
								delete!(f, "param_dataframe")
								delete!(f, "param_dict")

								f["param_dataframe"] = Df
								f["param_dict"] = params_dict
							end
						end
					end
				end
			else
				Df = initDfparams(params_dict_input)
				params_dict = params_dict_input
				Ndone = 0
			end
			new(filename, filepath, testcasefile, Df, params_dict, Ndone)
		end

		function JLDFile(filename::String, filepath::String, testcasefile::String)
			# For reading only
			@assert endswith(filepath, filename) "Filepath must end with filename."
			@assert isfile(filepath) "Filepath not valid."

			fid = jldopen(filepath, "r")
				Df = read(fid,"param_dataframe")
				params_dict = read(fid, "param_dict")
				Ndone = read(fid, "Ndone")
			close(fid)

			new(filename, filepath, testcasefile, Df, params_dict, Ndone)
		end

		function JLDFile(filename::String, df::DataFrame)
			filepath = string("results/",filename)
			testcasefile = string(filename[1:end-4], ".csv")
			params = Dict{String, Array{Int64,1}}
			Ndone = 0
			new(filename, filepath, testcasefile, df, params, Ndone)
		end
	end

	function show(io::IO, file::JLDFile)
		println("JLDFile ", file.filename, " :")
		for (k,v) in file.params
			println("	", k, " : ", length(v))
		end
	end

	function initDfparams(params_dict::Dict{String, <:Array{T,1} where T})
		# Create a dataframe that lists all points from the current study.
		# The study is meshing every parameter value with every other parameter value. 
		# Each point is given a number ID that can be retrieved using the findNID function.
		Df = DataFrame()
		for (k,v) in params_dict
			df = DataFrame(Symbol(k)=>v)
			if size(Df,2) == 0
				Df = df
			else
				Df = join(df, Df, kind=:cross)
			end
		end

		Npoints = size(Df, 1)
		Df.NID = range(1,stop=Npoints)

		Df
	end

	function addValueParam(new_param::String, new_param_values::Array{<:Number,1}, 
		params_dict_input::Dict{String, <:Array{T,1} where T}, params_df_input::DataFrame)
		# Adds value to already existing parameter.

		params_dict_output = copy(params_dict_input)
		new_params_dict = copy(params_dict_input)	# Used to update the dataframe

		# Adds value to the dictionary
		params_dict_output[new_param] = vcat(params_dict_input[new_param], new_param_values)
		new_params_dict[new_param] = new_param_values

		# Adds values to the dataframe
		new_df = initDfparams(new_params_dict)

		Npoints_old = size(params_df_input, 1)
		Npoints_new = size(new_df, 1)

		new_df.NID = range(Npoints_old+1, stop = Npoints_old+Npoints_new)
		params_df_output = vcat(params_df_input, new_df)

		# Returns the updated dictionary and dataframe
		return params_dict_output, params_df_output
	end

	function addValueParam(params_dict_input::Dict{String, <:Array{T,1} where T}, 
		params_df_input::DataFrame )
		# Adds value to already existing parameter.

		params_dict_output = copy(params_dict_input)
		new_params_dict = copy(params_dict_input)	# Used to update the dataframe

		# Adds values to the dataframe
		new_df = initDfparams(new_params_dict)

		Npoints_old = size(params_df_input, 1)
		Npoints_new = size(new_df, 1)

		new_df.NID = range(Npoints_old+1, stop = Npoints_old+Npoints_new)
		params_df_output = vcat(params_df_input, new_df)

		# Returns the updated dictionary and dataframe
		return params_dict_output, params_df_output
	end

    ################################################################################
    # File creation and modification
    ################################################################################

	function createfile(file::JLDFile)
		@assert !isfile(file.filepath) "File already exists !"

		jldopen(file.filepath, "w", compress=true) do f 
			f["param_dataframe"] = file.df_params
			f["param_dict"] = file.params
			f["Ndone"] = 0
		end

		println("	", file.filename, " has been successfully created.")
	end

	function addpoint(file::JLDFile, NID::Int, mem::MarketSimulation.memoryArrays)
		if !isfile(file.filepath)
			createfile(file)
		end

		jldopen(file.filepath, "r+", compress=true) do f
			social_welfare = dropdims(sum(abs.(mem.trade_array).*mem.prices_array, dims=(1,2)), dims=(1,2))
			f[string(NID, "/prices")] = mem.prices_array
			f[string(NID, "/power")] = mem.power_array
			f[string(NID, "/trades")] = mem.trade_array
			f[string(NID, "/length_mailbox")] = mem.length_mailbox_array
			f[string(NID, "/primal_res")] = mem.primal_res_array
			f[string(NID, "/dual_res")] = mem.dual_res_array
			f[string(NID, "/message")] = mem.message_array
			f[string(NID, "/computation_time")] = mem.computation_time
			f[string(NID, "/social_welfare")] = social_welfare

			# Updating counter Ndone
			Ndone = read(f,"Ndone")
			if NID > Ndone
				delete!(f, "Ndone")
				f["Ndone"] = NID
				file.Ndone = NID
			end
		end
	end

	function addpoint_atconvergence(file::JLDFile, NID::Int, 
		mem::MarketSimulation.memoryArrays, Eprim_rel2::Number)

		### Only save results at convergence!

		if !isfile(file.filepath)
			createfile(file)
		end

		# Eprim_abs2
		par = PowerNetwork.power_network(file.testcasefile, "full_P2P")
		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
		Eprim_abs2 = Eprim_rel2*optimal_power_exchange

		conv_iter = convergenceIteration(mem.primal_res_array, Eprim_abs2)

		jldopen(file.filepath, "r+", compress=true) do f
			social_welfare = dropdims(sum(abs.(mem.trade_array).*mem.prices_array, dims=(1,2)), dims=(1,2))
			f[string(NID, "/prices")] = resAtConvergence(mem.prices_array, conv_iter)
			f[string(NID, "/power")] = resAtConvergence(mem.power_array, conv_iter)
			f[string(NID, "/trades")] = resAtConvergence(mem.trade_array, conv_iter)
			f[string(NID, "/length_mailbox")] = resAtConvergence(mem.length_mailbox_array, conv_iter)
			f[string(NID, "/primal_res")] = resAtConvergence(mem.primal_res_array, conv_iter)
			f[string(NID, "/dual_res")] = resAtConvergence(mem.dual_res_array, conv_iter)
			f[string(NID, "/message")] = resAtConvergence(mem.message_array, conv_iter)
			f[string(NID, "/computation_time")] = resAtConvergence(mem.computation_time, conv_iter)
			f[string(NID, "/social_welfare")] = resAtConvergence(social_welfare, conv_iter)
			f[string(NID, "/conv_iter")] = conv_iter

			# Updating counter Ndone
			Ndone = read(f,"Ndone")
			if NID > Ndone
				delete!(f, "Ndone")
				f["Ndone"] = NID
				file.Ndone = NID
			end
		end
	end

	################################################################################
    # Find NIDs of given parameter values
    ################################################################################

	function findNID(file::JLDFile, point::Dict{String, <:Number})
		df = file.df_params
		for (k,v) in point
			df = df[ df[:,Symbol(k)].== v ,:]
		end
		NID = df[:,:NID]

		return NID, df
	end

    ################################################################################
    # Read raw results
    ################################################################################
    # Result dictionary :
    # Dict( row_index => 
    # 					Dict( arg1 => res_arg1,
    # 							arg2 => res_arg2,...), ...
    # 				)
    # (must be associated with the given df_NID)

    function readRes(file::JLDFile, df_NID::DataFrame, args::Array{String,1})

    	dict_out = Dict{Int64, Any}()
    	jldopen(file.filepath, "r") do f 
    		for idrow in 1:size(df_NID, 1)
    			nid = df_NID.NID[idrow]
    			if exists(f, string(nid))
	    			dict_in = Dict{String, Any}()
	    			for arg in args
	    				# dict_in[arg] = read(f[string(df_NID[idrow, :NID])], arg)
	    				dict_in[arg] = read(f[string(nid)], arg)
	    			end
	    			dict_out[nid] = dict_in
	    		end
    		end
    	end

    	dict_out
    end

    function readRes(file::JLDFile, df_NID::DataFrameRow, args::Array{String,1})
    	df = DataFrame(df_NID)
    	readRes(file, df, args)
    end

    ### Read results and map the function f on the values of ’farg’.
    function readRes(file::JLDFile, df_NID::DataFrame, args::Array{String,1}, 
    	f::Function, farg::String, result_name::String = "function_result")

    	@assert farg ∈ args "Function argument 'farg' must be part of 'args'. "

    	resDict = readRes(file, df_NID, args)
    	for (k,v) in resDict
    		v[result_name] = f(v[farg])
    	end

    	resDict
    end

    ################################################################################
    # Compute alpha beta gradient
    ################################################################################

    function alpha_beta_gradient!(Df::DataFrame)
    	### Compute the gradient of the convergence time according to alpha and
    	### beta parameters, for several values of delta and sigma = 0

    	function gradient_sort(x::Array{<:Number,1}, z::Array{<:Number,1})
    		# Sorts the array x and compute the gradient of z as a function of x.
    		@assert length(x) == length(z) "Vectors must be the same length."

    		ind = sortperm(x)

    		x_perm = x[ind]
    		z_perm = z[ind]

    		grad = [ (z_perm[i+1] - z_perm[i])/(x_perm[i+1] - x_perm[i]) for i in 1:length(x_perm)-1]
    		push!(grad, grad[end])

    		return ind, grad
    	end

    	function gradient(x::Array{<:Number,1}, z::Array{<:Number,1})
    		ind, grad = gradient_sort(x,z)
    		grad_unsort = [grad[i] for i in ind]
    	end

    	@assert (length(unique(Df.sigma)) ==1) & 
    		(unique(Df.sigma)[1] == 0) "There must be only one sigma equal to zero."

    	Df.convtime_gradalpha = zeros(size(Df,1))
    	Df.convtime_gradbeta = zeros(size(Df,1))

    	for df_delta in groupby(Df, :delta)
    		# alpha gradient
    		for df_beta in groupby(df_delta, :beta)
    			NID = Array(df_beta.NID)
    			alpha = Array(df_beta.alpha)
    			conv_time = Array(df_beta.conv_time)
    			grad_alpha = gradient(alpha, conv_time)
    			for i in 1:size(df_beta,1)
    				nid = NID[i]
    				ind = findall(Df.NID .== nid)[1]
    				Df[ind,:convtime_gradalpha] = grad_alpha[i]
    			end
    		end

    		# beta gradient
    		for df_alpha in groupby(df_delta, :alpha)
    			NID = Array(df_alpha.NID)
    			beta = Array(df_alpha.beta)
    			conv_time = Array(df_alpha.conv_time)
    			grad_beta = gradient(beta, conv_time)
    			for i in 1:size(df_alpha,1)
    				nid = NID[i]
    				ind = findall(Df.NID .== nid)[1]
    				Df[ind,:convtime_gradbeta] = grad_beta[i]
    			end
    		end
    	end
    	Df
    end

    ################################################################################
    # Residual/time
    ################################################################################

    function readResidual(file::JLDFile, df_NID::DataFrame)

    	dictRes = readRes(file, df_NID, ["primal_res"])

    	data = GenericTrace[]
    	for df in groupby(df_NID, :delta)
    		δt = df.sampling_period[1]

    		NIDs = df.NID
    		res = [dropdims(sum(dictRes[NID]["primal_res"], dims=1), 
    			dims=1)  for NID in NIDs]
    		res_array = hcat(res...)

    		mean_value = dropdims(mean(res_array, dims=2), dims=2)
    		stdv = dropdims(std(res_array[2:end,:], mean=mean_value[2:end], 
    			dims=2), dims=2)
    		std_value = vcat(0, stdv)
    		log10.(mean_value + std_value)
    		# display(std_value)

    		trace = scatter(
    						x0 = 0,
    						dx = δt,
    						y = log10.(mean_value), 
    						error_y = attr(
    							type = "data",
    							array = std_value,
    							visible = false,
    							),
    						name = string("δ=", df.delta[1])
    						)
    		push!(data,trace)
    	end

    	layout = Layout(
						# title_text = ,
						height = 310,
						width = 500,
						margin = attr(
										l = 40,
										r = 0,
										b = 40,
										t = 0,
							),
						xaxis = attr(
										title_text = "Temps (s)",
										title_size = 14,
										title_font_family ="Times New Roman", 
										range = [0,610],
										tickfont_family = "Times New Roman",
										tickfont_size = 16,
							),
						yaxis = attr(
										title_text = "Résidu primal (échelle log)",
										title_size = 14,
										title_font_family = "Times New Roman",
										# range = Ponts[pont]["range"],
										tickfont_family = "Times New Roman",
										tickfont_size = 16,
							),
		)

    	plot(data, layout)
    end

    ################################################################################
    # Find convergence and read result at convergence
    ################################################################################

    struct NotAnArray <: Exception end

    function convergenceIteration(prim_res::Array{Float64,2}, Eprim_abs2::Number)
    	# Find convergence iteration given the evolution of the primal residual
    	# and the absolute stopping criterion.

    	prim_res_sum = dropdims(sum(prim_res, dims=1), dims=1)
    	rmax, kmin = findmax(prim_res_sum)
		test = prim_res_sum .- Eprim_abs2

		if kmin == length(test)
			conv_iter = kmin
		else
			try
				conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
				# conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
			catch
				conv_iter = length(test)
			end
		end
		return conv_iter
    end

    function resAtConvergence(array::Union{Array{<:Number}, <:Number}, conv_iter::Int64)
    	if typeof(array)<: Array{<:Number,3}
			out = array[:,:,conv_iter]
		elseif typeof(array)<: Array{<:Number,2}
			out = array[:,conv_iter]
		elseif typeof(array)<: Array{<:Number,1}
			out = array[conv_iter]
		elseif typeof(array)<:Number
			out = array
		else
			throw(NotAnArray())
		end
		return out
    end

    function readAtConvergence(file::JLDFile, df_NID::DataFrame, args::Array{String,1}, 
    	Eprim_abs2::Number=1e-3)

    	new_args = unique(vcat(args, "primal_res"))

    	resDictIn = readRes(file, df_NID, new_args, 
    		x->convergenceIteration(x, Eprim_abs2), "primal_res", "conv_iter")

    	resDictOut = Dict{Int64, Dict{String, Any}}()
    	for (k,v) in resDictIn
    		conv_iter = v["conv_iter"]
    		d = Dict{String, Any}()
    		for (arg, value) in v
    			d[arg] = resAtConvergence(value, conv_iter)
    		end
    		resDictOut[k] = d
    	end		

    	resDictOut
    end

    function flattenResults(file::JLDFile, df_NID::DataFrame, 
    	dict_in::Dict{Int64, <:Dict{String, <:Any}}, pn::PowerNetwork.power_network)
    	# Create dataframe with scalar results for each point in df_NID.

    	function flattenResults(array::Union{Array{<:Number}, <:Number}, 
    		arg::String, pn::PowerNetwork.power_network)

    		if arg == "trades"
    			# trade residual sum(|tij+tji|)
    			output = sum(array)
    			outputname = "trade_residual"
    		elseif arg == "prices"
    			# prices mean for active trades
    			N_notnull = sum(array.≠0)
    			output = sum(array)/N_notnull
    			outputname = "mean_price"
    		elseif arg == "primal_res"
    			# sum of local residuals
    			output = sum(array)
    			outputname = "primal_res"
    		elseif arg == "dual_res"
    			# sum of local residuals
    			output = sum(array)
    			outputname = "dual_res"
    		elseif arg == "length_mailbox"
    			# mean of local length_mailbox
    			output = sum(array)/length(array)
    			outputname = "length_mailbox"
    		elseif arg == "social_welfare"
    			# already a scalar
    			output = array
    			outputname = "social_welfare"
    		elseif arg == "message"
    			# sum of all messages
    			output = sum(array)
    			outputname = "sum_messages"
    		elseif arg == "power"
    			# total power produced
    			output = sum(2*pn.gencost_a.*array.^2 + pn.gencost_b.*array)
    			outputname = "objective_function"
    		elseif arg == "computation_time"
    			output = array
    			outputname = "simulation_time"
    		elseif arg == "conv_iter"
    			output = array
    			outputname = "conv_iter"
    		elseif arg == "compare_trades"
    			output = array
    			outputname = "compare_trades"
    		else
    			print(arg)
    		end

    		return output, outputname
    	end

    	inputkeys = collect(keys(dict_in[collect(keys(dict_in))[1]]))

    	flatOutputNames = Dict(
    							"trades" => "trade_residual",
    							"prices" => "mean_price",
    							"primal_res" => "primal_res",
    							"dual_res" => "dual_res",
    							"length_mailbox" => "length_mailbox",
    							"social_welfare" => "social_welfare", 
    							"message" =>  "sum_messages", 
    							"power" => "objective_function",
    							"computation_time" => "simulation_time",
    							"conv_iter" => "conv_iter",
    							"compare_trades" => "compare_trades"
    							)
    	outputkeys = [flatOutputNames[inkey] for inkey in inputkeys]

    	Npoints = size(df_NID,1)
    	Df = DataFrame( Dict(outputkeys[i]=> zeros(Npoints) for i=1:length(outputkeys)) )
    	Df.NID = 1:Npoints 		# temporary values

    	k = 1
    	for (NID, dict) in dict_in
    		Df[k, :NID] = NID
    		for (arg, res_arg) in dict
    			arg2 = flatOutputNames[arg]
    			Df[k, Symbol(arg2)], name = flattenResults(res_arg, arg, pn)
    		end
    		k = k+1
    	end

    	join(df_NID, Df, on= :NID)
    end

    function flattenResultsTradewise(file::JLDFile, df_NID::DataFrame,
    	dict_in::Dict{Int64, <:Dict{String, <:Any}}, 
    	pn::PowerNetwork.power_network)

    	function flattenResultsTradewise( i::Int64, j::Int64,
    		array::Union{Array{<:Number}, <:Number}, 
    		arg::String, pn::PowerNetwork.power_network)

    		if arg == "trades"
    			# trade residual sum(|tij+tji|)
    			output = (array[i,j] + array[j,i])^2
    			output = array[i,j]
    			# println("   Warning: 'trade_res' is equal to the actual trade.")
    			outputname = "trade_res"
    		elseif arg == "prices"
    			# prices mean for active trades
    			output = array[i,j]
    			outputname = "price"
    		elseif arg == "message"
    			# sum of all messages
    			output = array[i,j]
    			outputname = "messages"
    		elseif arg == "conv_iter"
    			output = array
    			outputname = "conv_iter"
    		else
    			print(arg)
    		end

    		return output, outputname
    	end

    	comm_graph = pn.comm_graph
    	Ω = pn.Ω
    	NΩ = length(Ω)

    	inputkeys = collect(keys(dict_in[collect(keys(dict_in))[1]]))

    	flatOutputNames = Dict(
    							"trades" => "trade_res",
    							"prices" => "price",
    							"message" =>  "messages", 
    							"conv_iter" => "conv_iter",
    							)
    	outputkeys = [flatOutputNames[inkey] for inkey in inputkeys if (inkey ≠ "primal_res")]
    	outputkeys = vcat(outputkeys, ["i", "j", "distance"])

    	Npoints = size(df_NID,1)*ne(comm_graph)*2
    	Df = DataFrame( Dict(outputkeys[i]=> zeros(Npoints) for i=1:length(outputkeys)) )
    	Df.NID = 1:Npoints 		# temporary values

    	k = 1
    	for (NID, dict) in dict_in
			for (i,j) in Iterators.product(1:NΩ, 1:NΩ)
				!(has_edge(comm_graph, i, j)) && continue
				for (arg, res_arg) in dict
					(arg == "primal_res") && continue

	    			arg2 = flatOutputNames[arg]
	    			Df[k, Symbol(arg2)], name = flattenResultsTradewise(i, j, res_arg, arg, pn)
	    			Df[k, :i] = i
	    			Df[k, :j] = j

	    			Df[k, :distance] = comm_graph.weights[i,j]
				end
			Df[k, :NID] = NID
			k = k+1
    		end    		
    	end

    	join(df_NID, Df, on= :NID, kind=:inner)
	end

    ################################################################################
    # Dataframe to plot results
    ################################################################################

    function DfSweepConvergence(file::JLDFile, Eprim_rel2::Number) 
		df_params = file.df_params
		DfSweepConvergence(file, df_params, Eprim_rel2)
    end

    function DfSweepConvergence(file::JLDFile, df_params::DataFrame, Eprim_rel2::Number) 

    	# Eprim_abs2
		par = PowerNetwork.power_network(file.testcasefile, "full_P2P")
		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
		Eprim_abs2 = Eprim_rel2*optimal_power_exchange

		#
		resDictOut = readAtConvergence(file, df_params, 
			["primal_res", "trades", "power", "social_welfare", "prices", "message"], 
			Eprim_abs2)

		Df = flattenResults(file, df_params, resDictOut, par)
		Df.conv_time = Df.conv_iter.*Df.sampling_period

		Df
    end

    function DfCompareTrades(file::JLDFile, Eprim_rel2::Number, ref_point::Dict{String, <:Number})

    	# Compares trades, point by point between ref_point and other points
    	# the reference point changes with gamma

    	function compareTrades(Dict1::Dict{String, <:Any}, Dict2::Dict{String, <:Any})
    		T1 = Dict1["trades"]
    		T2 = Dict2["trades"]

    		sqrt(sum((T1-T2).^2))
    	end

    	function compareTrades2(Dict1::Dict{String, <:Any}, Dict2::Dict{String, <:Any})
    		T1 = Dict1["trades"]
    		T2 = Dict2["trades"]

    		ans = sum(abs.(T1[1:30,31:110]-T2[1:30,31:110]))
    		# println(ans)

    		ans
    	end

    	function compareTrades(refDf::DataFrame, df_params::DataFrame, 
    		resDict::Dict{Int64, <:Dict{String, <:Any}})
    		for (k,v) in resDict
    			# refDict with same gamma value
    			gamma = df_params[df_params.NID.==k, :gamma][1]
    			refNID = refDf[refDf.gamma.==gamma, :NID][1]
    			refDict = resDict[refNID]
    			v["compare_trades"] = compareTrades2(refDict, v)
    		end
    		resDict
    	end

    	# Eprim_abs2
		par = PowerNetwork.power_network(file.testcasefile, "full_P2P")
		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
		Eprim_abs2 = Eprim_rel2*optimal_power_exchange

		# find NID of reference point
		NIDs_ref, df = findNID(file, ref_point)

		resDict = readAtConvergence(file, file.df_params, 
			["primal_res", "trades", "power"], 
			Eprim_abs2)

		compareTrades(df, file.df_params, resDict)

		Df = flattenResults(file, file.df_params, resDict, par)
		Df.conv_time = Df.conv_iter.*Df.sampling_period

		Df
	end

	function DfSweepTradewiseConvergence(file::JLDFile, Eprim_rel2::Number, df::DataFrame) 

    	# Eprim_abs2
		par = PowerNetwork.power_network(file.testcasefile, "full_P2P")
		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
		Eprim_abs2 = Eprim_rel2*optimal_power_exchange

		#
		df_params = file.df_params
		resDictOut = readAtConvergence(file, df, 
			["trades", "prices", "message"], 
			Eprim_abs2)

		#
		Df = flattenResultsTradewise(file, df, resDictOut, par)
    end

    ################################################################################
    # Dividing data to compute
    ################################################################################
	    ### Avoid out of memory error
	    ### Creates a jld file that contains the Dataframes results

    function divideAndCompute_SweepConvergence(JLDfile::JLDFile, filepath::String, 
    	Eprim_rel2::Number)

    	@assert endswith(filepath, ".jld") "Filepath must end with '.jld'."

    	DF = JLDfile.df_params

    	# Creates the jld output file if it doesn't exist
    	if !isfile(filepath)
    		fid = jldopen(filepath, "w")
    		close(fid)
    	end

    	# Grouping by delta
    	for df in groupby(DF, :delta)
    		delta = df.delta[1]
    		print("Processing delta = ", delta, "                    ")
    		sub_df = DfSweepConvergence(JLDfile, DataFrame(df), Eprim_rel2)
    		jldopen(filepath, "r+") do fid 
    			write(fid, string("delta", delta), sub_df)
    		end
    		println("... done")
    	end

    	println("Attention ! pour n110_p30_c80_0x000f_alphabeta.jld, il y a un problème de double lecture...")
    end

    ################################################################################
    # Read data already stored at convergence
    ################################################################################

    function readAtConvergence(filepath::String)
    	@assert isfile(filepath) "Incorrect filepath."
    	@assert endswith(filepath, ".jld") "File must be Julia Data type: .jld"

    	fid = jldopen(filepath, "r") 
    		Df = read(fid, "param_dataframe")
    		N = size(Df,1)
    		Df[:,:conv_time] = Df.sampling_period

    		for i in 1:N
    			Df[i,:conv_time] = Df[i,:conv_time]*read(fid, string(Df.NID[i], "/conv_iter"))
    		end
    	close(fid)
    	
    	Df
    end

    
end