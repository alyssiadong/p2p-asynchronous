##################################################
## Tests for Planification.jl

# include("PowerNetwork.jl")
# include("Planification.jl")

# pn = PowerNetwork.power_network("n9_p3_c6_0x0007.csv", 
# 	"full_P2P")
# plan = Planification.planStruct(pn)
# # Planification.checkAgent(plan, 10)


# trig = Planification.TradewisePrimalResidual(1e-3, 2, 6)
# trig2 = Planification.Timestamp(100, 10)
# Planification.addTrigger!(plan, 6, trig)
# display(plan.trigger_list)
# Planification.addTrigger!(plan, 6, trig2)
# display(plan.trigger_list)

##################################################
## Test for market_sim.jl with pause

# include("PowerNetwork.jl")
# include("Planification.jl")
# include("CommunicationDelays.jl")
# include("market_sim.jl")

# using PlotlyJS

# pn = PowerNetwork.power_network("n9_p3_c6_0x0007.csv", 
# 	"full_P2P")

# rho = pn.preferred_rho	
# gamma = 1e-3
# ΔT = 8000.0
# sendType = "sendback"
# iterMatching = "yesmatching"
# priceUpdate = "new"
# tradeUpdate = "new"
# δt = 1
# Tsimu = 3000
# OSQPeps=1e-5
# Ntrig = fill(1,9)
# Tmax = fill(8000.0,9)

# α = 5.0
# β = 1.0
# γ = 0

# aPar = MarketSimulation.algoParams( 
# 		rho, gamma, OSQPeps, "OSQP", Ntrig, Tmax,
# 		sendType, iterMatching, priceUpdate, tradeUpdate)
# nPar = CommunicationDelays.commParams(α, β, γ)
# plan = Planification.planStruct(pn)
# Planification.addTrigger!(plan, 7, Planification.Timestamp(200, 200))
# Planification.addTrigger!(plan, 4, Planification.Timestamp(200, 200))
# Planification.addTrigger!(plan, 5, Planification.Timestamp(200, 200))
# # Planification.addTrigger!(plan, 7, Planification.Timestamp(200, 200))
# # Planification.addTrigger!(plan, 3, Planification.TradewiseIteration(40, 9, 200))


# mem = MarketSimulation.run_simulation(Tsimu, δt, pn, nPar, aPar, plan)

# PlotlyJS.plot(mem.power_array')

# # messages = dropdims(sum(mem.message_array, dims=2),dims =2)
# # PlotlyJS.plot(messages')

# # sum_power = dropdims(sum(mem.power_array, dims=(1)),dims =(1))
# # PlotlyJS.plot(sum_power)

# # res = mem.trade_array + permutedims(mem.trade_array,(2,1,3))
# # PlotlyJS.plot(res[3,:,:]')


# ##################################################
## Tests for Planification.jl: adding warm start

# include("PowerNetwork.jl")
include("Planification.jl")
# include("CommunicationDelays.jl")
include("market_sim.jl")

using PlotlyJS

pn = PowerNetwork.power_network("n9_p3_c6_0xcccc.csv", 
	"full_P2P")
plan = Planification.planStruct(pn)
# Planification.addTrigger!(plan, 7, Planification.Timestamp(2000, 1300))

# Running the market for the first time without warm start
	rho = pn.preferred_rho	
	gamma = 1e-1
	ΔT = 8000.0
	sendType = "sendback"
	iterMatching = "yesmatching"
	priceUpdate = "new"
	tradeUpdate = "new"
	δt = 1
	Tsimu = 3000
	OSQPeps=1e-5
	Ntrig = fill(1,9)
	Tmax = fill(8000.0,9)

	α = 5.0
	β = 1.0
	γ = 0

	aPar = MarketSimulation.algoParams( 
			rho, gamma, OSQPeps, "OSQP", Ntrig, Tmax,
			sendType, iterMatching, priceUpdate, tradeUpdate)
	nPar = CommunicationDelays.commParams(α, β, γ)
	mem = MarketSimulation.run_simulation(Tsimu, δt, pn, nPar, aPar, plan)

	mem.trade

# pn = PowerNetwork.power_network("n9_p3_c6_0x0007.csv", 
# 	"full_P2P")
# plan = Planification.planStruct(pn)
# T = mem.trade
# Λ = mem.prices
# # T[7,:] .= 0
# # T[:,7] .= 0
# Λ[7,:] .= 1
# Λ[:,7] .= 1
# Planification.setWarmStart!(plan, "T", T)
# Planification.setWarmStart!(plan, "Λ", Λ)
# mem = MarketSimulation.run_simulation(Tsimu, δt, pn, nPar, aPar, plan)

## Plot results
PlotlyJS.plot(mem.power_array')

### Gamma sweep
# JLDfile = WR.JLDFile("n110_p30_c80_0x000f_gammasweep.jld",
#   "results/n110_p30_c80_0x000f_gammasweep.jld", 
#   "n110_p30_c80_0x000f.csv")

# refDict = Dict("delta" => 1.0, "OSQPeps" => 1e-5)
# # Df = WR.DfCompareTrades(JLDfile, 1e-9, refDict)
# sort!(Df, :delta, rev=false)
# Df.loggamma = log10.(Df.gamma)

# # Reference value: sum(|tij,δ=1,γ|)
# Eprim_rel2 = 1e-9
# par = PowerNetwork.power_network("n110_p30_c80_0x000f.csv", "full_P2P")
# pmin = par.Pmin
# pmax = par.Pmax
# optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))        # Needed to compute absolute Epsilon_prim
# Eprim_abs2 = Eprim_rel2*optimal_power_exchange

# df = JLDfile.df_params
# df = df[ (df.delta.==1),:]
# dic = WR.readAtConvergence(JLDfile, df, ["trades"], Eprim_abs2)

# Df.tradewise_distance_normalized = zeros(size(Df,1))
# for g in unique(df.gamma)
#     nid = df[df.gamma .== g,:NID][1]
#     t0 = dic[nid]["trades"]
#     sum0 = sum(abs.(t0[1:30,31:110]))
#     # println(sum0)

#     Df[Df.gamma .== g, :tradewise_distance_normalized] = Df[Df.gamma .== g, :compare_trades] ./sum0
# end

# m_color =  Dict(
#         1.0 => "#1f77b4",  # muted blue
#         0.8 => "#ff7f0e",  # safety orange
#         0.7 => "#2ca02c",  # cooked asparagus green
#         0.5 => "#d62728",  # brick red
#         0.2 => "#9467bd",  # muted purple
#         # =>'#8c564b',  # chestnut brown
#         # =>'#e377c2',  # raspberry yogurt pink
#         # =>'#7f7f7f',  # middle gray
#         # =>'#bcbd22',  # curry yellow-green
#         # =>'#17becf'   # blue-teal
#     )
# m_symbol = Dict(
#         1.0 => "circle", 
#         0.8 => "triangle-down", 
#         0.7 => "triangle-up", 
#         0.5 => "diamond",
#         0.2 => "square",
#         )

# data = GenericTrace[]
# for df in groupby(Df, [:delta])
#   # if df.delta[1] == 1.0
#       trace = scatter(x=df.gamma, y=df.objective_function, 
#           # mode = "lines+markers", 
#           mode = "markers", 
#           # line_width = 2,
#           # marker_size = 5, 
#           # name = string("OF_delta =", df.delta[1]),
#           marker = attr(
#                     symbol = m_symbol[df.delta[1]],
#                     color = m_color[df.delta[1]],
#             ),
#           showlegend = false,
#           yaxis = "y",
#           xaxis = "x")
#       push!(data, trace)
#   # end
#   trace = scatter(x=df.gamma, y=df.tradewise_distance_normalized, 
#       # mode = "lines+markers", 
#       mode = "markers", 
#       # line_width = 2,
#       # marker_size = 5, 
#       marker = attr(
#                     symbol = m_symbol[df.delta[1]],
#                     color = m_color[df.delta[1]],
#             ),
#       name = string("δ = ", df.delta[1]),
#       yaxis = "y2",
#       xaxis = "x")
#   push!(data, trace)
# end

# layout = Layout(
#       height = 300,
#       width = 328,
#       margin = attr(
#                       l = 0,
#                       r = 5,
#                       t = 5,
#                       b = 20,
#                       ),
#       # showlegend = false,
#       legend = attr(
#                       x = 0.7,
#                       y = 0.1,
#                       font_size = 10,
#                       font_family = "PT Sans Narrow",
#           ),
#       xaxis = attr(
#                       constraintowards = "bottom",
#                       anchor = "y2",
#                       type = "log",
#                       range = [-4,1],
#                       title = attr(
#                                       text = "γ",
#                                       font_size = 10,
#                                       font_color = "#444444",
#                                       font_family = "PT Sans Narrow",
#                           ), 
#                       tickfont_size = 10,
#                       tickfont_family = "PT Sans Narrow",
#                       zeroline=true,
#                       ticks = "inside",
#                       dtick = 1,
#                       showline = true,
#                       linecolor= "black",
#                       linewidth= 0.5,
#                       mirror= "allticks",
#                       ),
#       yaxis2 =attr(   
#                       domain = [0,0.6],
#                       # range = [sw_min, sw_max],
#                       # side = "right",
#                       # title = attr(
#                       #               standoff = 80,
#                       #               text = "Trades comparison", 
#                       #               font_size = 17,
#                       #               font_color = "#ff7f0e"
#                       #   ), 
#                       # overlaying="y",
#                       # color = "#ff7f0e",
#                       tickfont_size = 10,
#                       tickfont_family = "PT Sans Narrow",
#                       # tickfont=attr(
#                       #             color="#ff7f0e"
#                       #         ),
#                       automargin = true,
#                       zeroline=true,
#                       ticks = "inside",
#                       showline = true,
#                       linecolor= "black",
#                       linewidth= 0.5,
#                       mirror= "allticks",
#                       ),
#       yaxis = attr(   
#                       domain = [0.65,1],
#                       # title = attr(
#                       #               standoff = 80,
#                       #               text = "Objective function", 
#                       #               font_size = 17,
#                       #               font_color = "#1f77b4"
#                       #   ), 
#                       # color = "#1f77b4",
#                       tickfont_size = 10,
#                       tickfont_family = "PT Sans Narrow",
#                       # tickfont = attr(
#                       #             color="#1f77b4"
#                       #         ),
#                       automargin = true,
#                       zeroline=true,
#                       dtick = 40000,
#                       ticks = "inside",
#                       showline = true,
#                       linecolor= "black",
#                       linewidth= 0.5,
#                       mirror= "allticks",
#                       ),
#       annotations = [
#                       attr(
#                               visible = true,
#                               text = "Global cost value C<sub>δ</sub>",
#                               font = attr( 
#                                   size = 11, 
#                                   # color = "black", 
#                                   family = "PT Sans Narrow"),
#                               showarrow = false,
#                               yref = "y",
#                               x = -1.5,
#                               y = -72500,
#                           ),
#                       attr(
#                               visible = true,
#                               text = "Trades comparison v<sub>δ</sub>",
#                               font = attr( 
#                                   size = 11, 
#                                   # color = "black", 
#                                   family = "PT Sans Narrow"),
#                               showarrow = false,
#                               yref = "y2",
#                               x = -1.5,
#                               y = 0.23,
#                           ),
#                   ],
#       )

# plot(data, layout)