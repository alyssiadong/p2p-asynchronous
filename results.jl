module Res

	using HDF5
	using LinearAlgebra
	using DataFrames, Statistics
	using PlotlyJS
	using ..PowerNetwork

	import Base.show
    export shows

    ################################################################################
    # Structures
    ################################################################################

    ### Market structure 
    struct market
    	name::String
    	parPath::String
    	resPath::Array{String}		# List of all the result files present in the ’results’ folder.

    	Nprod::Int64
    	Ncons::Int64
    	Nid::String

    	function market(name::String)
    		# Nid
    		Nid = name[end-5:end]
    		@assert startswith(Nid, "0x") "Uncorrect name :\n 	must end with hexadecimal format : ’0x2e56’"

    		# Nprod & Ncons
    		# Regular expression match
    		reg = r"_c\d+_"
    		m = match(reg, name)
    		Ncons = parse(Int, m.match[3:end-1])
    		reg = r"_p\d+_"
    		m = match(reg, name)
    		Nprod = parse(Int, m.match[3:end-1])    		

    		# Testcase file existence
	        parPath = string("testcase/", name, ".csv")
	        @assert isfile(parPath) "Testcase parameter file does not exist."

	        # Results files
	        files = readdir("results")
	       	resPath = files[map(x->startswith(x,name),files)]

	        new(name, parPath, resPath, Nprod, Ncons, Nid)
    	end

    	function market(Nprod::Int, Ncons::Int, Nid::String)
			# Nid
    		@assert startswith(Nid, "0x") && (length(Nid) == 6) "Uncorrect Nid :\n 	hexadecimal format : ’0x2e56’"

    		# Nprod+Ncons+name
    		NΩ = Nprod + Ncons
    		name = string("n", NΩ, "_p", Nprod, "_c", Ncons, "_", Nid)

	        # Testcase file existence
	        parPath = string("testcase/", name, ".csv")
	        @assert isfile(parPath) "Testcase parameter file does not exist."

	        # Results files
	        files = readdir("results")
	       	resPath = files[map(x->startswith(x,name),files)]

	        new(name, parPath, resPath, Nprod, Ncons, Nid)
    	end
    end

    function show(io::IO, mar::market)
    	println("Market ", mar.name)
    	if length(mar.resPath) == 0
    		println("No result file.")
    	else
    		println("Results files :")
	    	for path in mar.resPath
	    		println("	", path)
	    	end
	    end
    end

    ### Results structure
    struct res 
    	mar::market
    	resPath::String
    	trueResPath::String

    	A::Array{Float64}	# alpha array
    	B::Array{Float64}	# beta array	
    	D::Array{Float64}	# delta array
    	S::Array{Float64}	# sigma array
    	a::Int64			# length(A)
    	b::Int64			# length(B)
    	d::Int64			# length(D)
    	s::Int64			# length(S)

    	ρ::Number 			# ADMM penalty factor

    	function res(mar::market)
    		# Select which result file to work on
    		display(mar)
    		if length(mar.resPath) == 0
    			println(" -->","No result file to select.")
    			resPath = ""
    		elseif length(mar.resPath) == 1
    			println(" -->",mar.resPath[1], " selected.")
    			resPath = mar.resPath[1]
    			trueResPath = string("results/", resPath)
    		else 
    			# Asks user which result file to select.
    			print("Select result file (number): ")
    			n = readline()
    			n = parse(Int, n)
    			println(" -->", mar.resPath[n], " selected.")
    			resPath = mar.resPath[n]
    			trueResPath = string("results/", resPath)
    		end

    		# Read file
    		fid = h5open(trueResPath, "r")
    		g = fid["algoParams"]
    		A = read(g, "A")
    		B = read(g, "B")
    		D = read(g, "D")
    		S = read(g, "S")
    		ρ = read(g, "penalty_factor")
    		close(fid)

    		a = length(A)
    		b = length(B)
    		d = length(D)
    		s = length(S)

    		new(mar, resPath, trueResPath, A, B, D, S, a, b, d, s, ρ)
    	end

    	function res(mar::market, n::Int)
    		@assert 0<n≤length(mar.resPath) "Index number n out of bounds."

    		# Select which result file to work on
    		if length(mar.resPath) == 0
    			println(" -->","No result file to select.")
    			resPath = ""
    		elseif length(mar.resPath) == 1
    			println(" -->",mar.resPath[1], " selected.")
    			resPath = mar.resPath[1]
    			trueResPath = string("results/", resPath)
    		else 
    			println(" -->", mar.resPath[n], " selected.")
    			resPath = mar.resPath[n]
    			trueResPath = string("results/", resPath)
    		end

    		# Read file
    		fid = h5open(trueResPath, "r")
    		g = fid["algoParams"]
    		A = read(g, "A")
    		B = read(g, "B")
    		D = read(g, "D")
    		S = read(g, "S")
    		ρ = read(g, "penalty_factor")
    		close(fid)

    		a = length(A)
    		b = length(B)
    		d = length(D)
    		s = length(S)

    		new(mar, resPath, trueResPath, A, B, D, S, a, b, d, s, ρ)
    	end

    	function res(mar::market, resPath::String)

    		@assert ((startswith(resPath, "D:/") && isfile(resPath)) || isfile(string("results/", resPath))) "Result file doesn’t exist."

    		# Read file
    		if startswith(resPath, "D:/")
    			trueResPath = resPath
    		else
    			trueResPath = string("results/", resPath)
    		end
    		fid = h5open(trueResPath, "r")
    		g = fid["algoParams"]
    		A = read(g, "A")
    		B = read(g, "B")
    		D = read(g, "D")
    		S = read(g, "S")
    		ρ = read(g, "penalty_factor")
    		close(fid)

    		a = length(A)
    		b = length(B)
    		d = length(D)
    		s = length(S)

    		new(mar, resPath, trueResPath, A, B, D, S, a, b, d, s, ρ)
    	end
    end

    function show(io::IO, result::res)
    	println("Result structure : ", result.resPath)
    	println("	a 	", result.a)
    	println("	b 	", result.b)
    	println("	d 	", result.d)
    	println("	s 	", result.s)
    end

    _unpack(result::res) = (result.A, result.B, result.D, result.S, 
    	result.a, result.b, result.d, result.s)

    ### Optimal result structure
    struct opt_res
    	trades::Array{Number}
    	prices::Array{Number}
    	SW::Number
    	Eprim_rel2::Number

    	function opt_res(mar::market, Eprim_rel2::Number)
    		par = PowerNetwork.power_network(string(mar.name,".csv"), "full_P2P")
    		trades, prices, kmax = PowerNetwork.optimal_res_admm(par, par.preferred_rho, Eprim_rel2)
    		SW = sum(abs.(trades).*prices)

    		new(trades, prices, SW, Eprim_rel2)
    	end
    	function opt_res()
    		trades = zeros(1,1)
    		prices = zeros(1,1)
    		SW = 0.0
    		Eprim_rel2 = 1e-12
    		new(trades, prices, SW, Eprim_rel2)
    	end
    end
    optResult0 = opt_res()

    ################################################################################
	# Low level functions
	# At given a, b, d, s point.
	################################################################################

    ### Read result ’type’ for one point (a,b,d,s,t)
    function readRes(result::res, type::Union{String, Array{String,1}}, a::Int, b::Int, d::Int, s::Int, t::Int)
    	@assert a in range(1, stop=result.a) "Index a out of bounds."
    	@assert b in range(1, stop=result.b) "Index b out of bounds."
    	@assert d in range(1, stop=result.d) "Index d out of bounds."
    	@assert s in range(1, stop=result.s) "Index s out of bounds."

    	fid = h5open(result.trueResPath, "r")
    	g = fid[string(d)]

    	name = string("alpha", a, "_beta", b, "_sigma", s, "_tirage", t)
    	@assert exists(g, name) "Index t out of bounds."

    	if typeof(type) == String
	    	@assert exists(g[name], type) "Incorrect 'type'."
	    	out = read(g[name], type)
	    else
	    	out = Array{Any,1}()
	    	for it in type
		    	@assert exists(g[name], it) string("Incorrect 'type' ", it," .")
		    	push!(out, read(g[name], it))
		    end
		end
    	close(fid)
    	out
    end

    ### Read result ’type’ for one point (a,b,d,s,t) of an already opened HDF5 file.
    ### If ’type’ is an array of strings, the function returns an array of all results for t in ’type’.
    ### Assertions inhibited for speed.
    function readRes(fid::HDF5File, type::Union{String, Array{String,1}}, a::Int, b::Int, d::Int, s::Int, t::Int)
    	g = fid[string(d)]
    	name = string("alpha", a, "_beta", b, "_sigma", s, "_tirage", t)
    	# @assert exists(g, name) "Index t out of bounds."
    	# @assert exists(g[name], type) "Incorrect 'type'."
    	if typeof(type) == String
    		out = read(g[name], type)
    	else
    		out = Array{Any,1}()
    		for it in type 
    		    res = read(g[name], it)
    		    push!(out,res)
    		end
    	end
    	out
    end

    ### Read result ’type’ for one point (a,b,d,s,t) of an already opened HDF5 file.
    ### If ’type’ is an array of strings, the function returns an array of all results for t in ’type’.
    ### Assertions inhibited for speed.
    ### ’name’ must be of type "alphaA_betaB_sigmaS_tirageT"
    function readRes(fid::Union{HDF5File, HDF5Group}, type::Union{String, Array{String,1}}, name::String)
    	if typeof(type) == String
    		out = read(fid[name], type)
    	else
    		out = Array{Any,1}()
    		for it in type 
    		    res = read(fid[name], it)
    		    push!(out,res)
    		end
    	end
    	out
    end

    ### Read results of an integer value for each draw
    function readRes(result::res, type::String, a::Int, b::Int, d::Int, s::Int)
    	@assert a in range(1, stop=result.a) "Index a out of bounds."
    	@assert b in range(1, stop=result.b) "Index b out of bounds."
    	@assert d in range(1, stop=result.d) "Index d out of bounds."
    	@assert s in range(1, stop=result.s) "Index s out of bounds."

    	fid = h5open(result.trueResPath, "r")
    	g = fid[string(d)]
    	tirages = names(g)
    	tirages = tirages[map(x->startswith(x,string("alpha", a, "_beta", b, "_sigma", s)),tirages)]

    	N = length(tirages)
    	Df = DataFrame(:tirage => Int64[], :output => Number[])
    	for (n,t) in enumerate(tirages)
    		out = read(g[t], type)
    		push!(Df, [n, out])
    	end

    	close(fid)    	
    	Df
    end

    ### Read results for each draw and map the function f on the values of ’type’.
    ### Function f must return a number.
    # function readRes(result::res, type::String, a::Int, b::Int, d::Int, s::Int, f::Function)
    	# @assert a in range(1, stop=result.a) "Index a out of bounds."
    	# @assert b in range(1, stop=result.b) "Index b out of bounds."
    	# @assert d in range(1, stop=result.d) "Index d out of bounds."
    	# @assert s in range(1, stop=result.s) "Index s out of bounds."

    	# fid = h5open(result.trueResPath, "r")
    	# g = fid[string(d)]
    	# tirages = names(g)
    	# tirages = tirages[map(x->startswith(x,string("alpha", a, "_beta", b, "_sigma", s)),tirages)]

    	# N = length(tirages)
    	# Df = DataFrame(:a => Int64[], :b => Int64[], :d => Int64[], :s => Int64[],
    	# 	:tirage => Int64[], :output => Number[])
    	# for (n,t) in enumerate(tirages)
    	# 	out = read(g[t], type)
    	# 	push!(Df, [a,b,d,s,n, f(out)])
    	# end

    	# close(fid)
    	# Df
    # end

    ### Read results for each draw and map the function f on the values of ’type’.
    ### If ’type’ is an array of strings, the function returns an array of all results for t in ’type’.
    ### Function f must return a number.
    ### Assertions inhibited for speed.
    function readRes(result::res, type::Union{String, Array{String,1}}, a::Int, b::Int, d::Int, s::Int, f::Function)
    	fid = h5open(result.trueResPath, "r")
    	g = fid[string(d)]
    	tirages = names(g)
    	tirages = tirages[map(x->startswith(x,string("alpha", a, "_beta", b, "_sigma", s)),tirages)]

    	N = length(tirages)
    	Df = DataFrame(:a => Int64[], :b => Int64[], :d => Int64[], :s => Int64[],
    		:tirage => Int64[], :output => Number[])
    	for (n,t) in enumerate(tirages)
    		out = readRes(g, type, t)
    		push!(Df, [a,b,d,s,n, f(out)])
    	end
    	close(fid)
    	Df
    end

    # Convergence iter for 1 sample a,b,d,s,t
    function readResAtConvergence(result::res, type::String, 
    	a::Int, b::Int, d::Int, s::Int, t::Int, Eprim_abs2::Number)

    	# Primal residual retrieval
    	prim_res = readRes(result, "primal_res", a, b, d, s, t)
    	prim_res_sum = dropdims(sum(prim_res, dims=1), dims=1)
    	rmax, kmin = findmax(prim_res_sum)
		test = prim_res_sum .- Eprim_abs2

		# Convergence iteration
		if kmin == length(test)
			conv_iter = kmin
		else
			try
				conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
				# conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
			catch
				conv_iter = length(test)
			end
		end

		# Result read at convergence
		out_array = readRes(result, type, a, b, d, s, t)
		if typeof(out_array)<: Array{<:Number,3}
			out = out_array[:,:,conv_iter]
		elseif typeof(out_array)<: Array{<:Number,2}
			out = out_array[:,conv_iter]
		elseif typeof(out_array)<: Array{<:Number,1}
			out = out_array[conv_iter]
		else
			throw(NotAnArray())
		end

		return (conv_iter, out)
    end
    struct NotAnArray <: Exception end

    # Convergence iter for 1 sample, function used in readRes(...,f::Function)
    function convergenceIteration(prim_res::Array{Float64,2}, Eprim_abs2::Number)
    	prim_res_sum = dropdims(sum(prim_res, dims=1), dims=1)
    	rmax, kmin = findmax(prim_res_sum)
		test = prim_res_sum .- Eprim_abs2

		if kmin == length(test)
			conv_iter = kmin
		else
			try
				conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
				# conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
			catch
				conv_iter = length(test)
			end
		end
		return conv_iter
    end

    ### Convergence iteration for every sample of a,b,d,s
    ### Returns a DataFrame that contains the columns : 
    ###		a,b,d,s,tirage(t),convIter, convTime, alpha, beta, delta, sigma
    function convergenceIteration(result::res, a::Int, b::Int, d::Int, s::Int, Eprim_abs2::Number)
    	Df = readRes(result, "primal_res", a, b, d, s, x->convergenceIteration(x, Eprim_abs2))
    	rename!(Df, Dict(:output => :convIter))

    	# Convergence time
    	δt = readRes(result, "delta_t_sampling", a, b, d, s, 1)
    	Df.convTime = Df.convIter.*δt

    	# Alpha, beta, delta, sigma
    	A,B,D,S,a0,b0,d0,s0 = _unpack(result)
    	N = size(Df,1)
    	Df.alpha = fill(A[a], N)
    	Df.beta = fill(B[b], N)
    	Df.delta = fill(D[d], N)
    	Df.sigma = fill(S[s], N)

    	Df
    end

    ### Applies function f to ’type’ for each draw at time of convergence.
    ### Function f must return a number.
    function applyFunctionAtConvergence(result::res, type::Union{String, Array{String,1}}, a::Int, b::Int, d::Int, s::Int, 
    	Eprim_abs2::Number, f::Function)

    	fid = h5open(result.trueResPath, "r")

    	Df = convergenceIteration(result, a, b, d, s, Eprim_abs2)
    	Df.output = zeros(size(Df,1))
    	for (i,t) ∈ enumerate(Df.tirage)
    		array = readRes(fid, type, a, b, d, s, t) 
    		Df.output[i] = f(array, Df.convIter[i])
    	end
    	close(fid)
    	Df
    end

    # Same but adds another column to Df.
    function applyFunctionAtConvergence!(Df::DataFrame, result::res, type::String, a::Int, b::Int, d::Int, s::Int, 
    	f::Function)

    	Df.output = zeros(size(Df,1))
    	for (i,t) ∈ enumerate(Df.tirage)
    		array = readRes(result, type, a, b, d, s, t) 
    		Df.output[i] = f(array, Df.convIter[i])
    	end
    end

    ### 
    function sw(A::Array{Float64,1}, conv_iter::Int64)
		A[conv_iter]
	end

	function sum_messages(A::Array{Int64, 3}, conv_iter::Int64)
		sum(A[:,:,conv_iter])
	end

	function V(A::Array{Any,1}, conv_iter::Int64, optRes::opt_res, rho::Number)
		trades0, prices0 = optRes.trades, optRes.prices
		trades, prices = A

		trades = trades[:,:,conv_iter]
		prices = prices[:,:,conv_iter]

		V0 = rho*norm(trades0-trades0')^2 + 1/rho*norm(prices0)^2
		
		out = rho*norm((trades - trades')/2 - (trades0 - trades0')/2,2)^2 + 
			1/rho*norm(prices-prices0,2)
		out/V0
	end

	function V(A::Array{Any,1}, optRes::opt_res, rho::Number)
		trades0, prices0 = optRes.trades, optRes.prices
		trades, prices = A
		V0 = rho*norm(trades0-trades0')^2 + 1/rho*norm(prices0)^2

		out = dropdims(rho*sum(((trades - permutedims(trades, (2,1,3)))/2 .- (trades0 - trades0')/2).^2,dims = (1,2)) + 
			1/rho*sum((prices .- prices0).^2,dims=(1,2)), dims=(1,2)) 
		out./V0
	end

	################################################################################
	# High level functions
	# At given a,b,s point.
	################################################################################

	# Convergence iteration of point (a,b,s) for every draw and every δ.
	function convIterDeltaSweep(result::res, Eprim_abs2::Number)
		A,B,D,S,Na,Nb,Nd,Ns = _unpack(result)
		@assert (Na == 1) && (Nb == 1)  "Result structure must represent only one point (a,b)."

		a = 1
		b = 1
		Df = DataFrame(:a => Int64[], :b => Int64[], :d => Int64[], :s => Int64[], 
			:tirage => Int64[], :convIter => Int64[], :convTime => Float64[],
			:alpha => Float64[], :beta => Float64[], :delta => Float64[], :sigma => Float64[])
		for (d,s) in Iterators.product(1:Nd,1:Ns)
			df = convergenceIteration(result, a,b,d,s,Eprim_abs2)
			Df = vcat(df, Df)
		end
		Df
	end

	# Social welfare
	function convIterDeltaSweepSW(result::res, Eprim_abs2::Number)
		A,B,D,S,Na,Nb,Nd,Ns = _unpack(result)
		@assert (Na == 1) && (Nb == 1)  "Result structure must represent only one point (a,b)."

		a = 1
		b = 1
		Df = DataFrame(:a => Int64[], :b => Int64[], :d => Int64[], :s => Int64[], 
			:tirage => Int64[], :convIter => Int64[], :convTime => Float64[], :output=>Float64[],
			:alpha => Float64[], :beta => Float64[], :delta => Float64[], :sigma => Float64[])
		for (d,s) in Iterators.product(1:Nd,1:Ns)
			df = applyFunctionAtConvergence(result, "social_welfare",a,b,d,s,Eprim_abs2, sw)
			Df = vcat(df, Df)
		end
		rename!(Df, Dict(:output => :social_welfare))
		Df
	end

	# Number of exchanged messages
	function convIterDeltaSweepMess(result::res, Eprim_abs2::Number)
		A,B,D,S,Na,Nb,Nd,Ns = _unpack(result)
		@assert (Na == 1) && (Nb == 1)  "Result structure must represent only one point (a,b)."

		a = 1
		b = 1
		Df = DataFrame(:a => Int64[], :b => Int64[], :d => Int64[], :s => Int64[], 
			:tirage => Int64[], :convIter => Int64[], :convTime => Float64[], :output=>Float64[],
			:alpha => Float64[], :beta => Float64[], :delta => Float64[], :sigma => Float64[])
		for (d,s) in Iterators.product(1:Nd,1:Ns)
			df = applyFunctionAtConvergence(result, "message",a,b,d,s,Eprim_abs2, sum_messages)
			Df = vcat(df, Df)
		end
		rename!(Df, Dict(:output => :number_messages))
		Df
	end

	# SW + Number of exchanged messages
	function convIterDeltaSweepSWMess(result::res, Eprim_abs2::Number)
		A,B,D,S,Na,Nb,Nd,Ns = _unpack(result)
		@assert (Na == 1) && (Nb == 1)  "Result structure must represent only one point (a,b)."

		a = 1
		b = 1
		Df = DataFrame(:a => Int64[], :b => Int64[], :d => Int64[], :s => Int64[], 
			:tirage => Int64[], :convIter => Int64[], :convTime => Float64[], 
			:number_messages => Float64[], :output=>Float64[],
			:alpha => Float64[], :beta => Float64[], :delta => Float64[], :sigma => Float64[])
		for (d,s) in Iterators.product(1:Nd,1:Ns)
			df = applyFunctionAtConvergence(result, "message",a,b,d,s,Eprim_abs2, sum_messages)
			rename!(df, Dict(:output=>:number_messages))
			applyFunctionAtConvergence!(df, result, "social_welfare",a,b,d,s, sw)
			Df = vcat(df, Df)
		end
		rename!(Df, Dict(:output => :social_welfare))
		Df
	end

	################################################################################
	# Data retrieving functions
	################################################################################

	# For a res structure only (results inside a single HDF5 file)
	# convIter, convTime and SW
	function DfPlotDeltaSweepSW(result::res, Eprim_rel2::Number)
		# Eprim_abs2
		par = PowerNetwork.power_network(string(result.mar.name,".csv"), "full_P2P")
		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
		Eprim_abs2 = Eprim_rel2*optimal_power_exchange

		# Reading results
		Df = convIterDeltaSweepSW(result, Eprim_abs2)
	end

	# For a res structure only (results inside a single HDF5 file)
	# convIter, convTime and number_messages
	function DfPlotDeltaSweepMess(result::res, Eprim_rel2::Number)
		# Eprim_abs2
		par = PowerNetwork.power_network(string(result.mar.name,".csv"), "full_P2P")
		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
		Eprim_abs2 = Eprim_rel2*optimal_power_exchange

		# Reading results
		Df = convIterDeltaSweepMess(result, Eprim_abs2)
	end

	################################################################################
	# Specific data retrieving functions (for speed)
	# Results for one market may be stored in multiple HDF5 files.
	################################################################################
	# deprecated
	function DfDeltaSweepConvSW_0x001c(Eprim_rel2::Number)
		mar = market(10,21,"0x001c")

		# Determinist sigma_rel = 0.0
		r = res(mar, "n31_p10_c21_0x001c_sigma0.h5")
		Df = DfPlotDeltaSweepSW(r, Eprim_rel2)

		# Stochastic sigma_rel = 0.5
		r = res(mar, "n31_p10_c21_0x001c_sigma0_5.h5")
		Df2 = DfPlotDeltaSweepSW(r, Eprim_rel2)

		# Stochastic sigma_rel = 1.0
		r = res(mar, "n31_p10_c21_0x001c_sigma1_0.h5")
		Df3 = DfPlotDeltaSweepSW(r, Eprim_rel2)

		# Stochastic sigma_rel = 0.7
		r = res(mar, "n31_p10_c21_0x001c_sigma0_7.h5")
		Df4 = DfPlotDeltaSweepSW(r, Eprim_rel2)

		Df = vcat(Df, Df2, Df3, Df4)
	end

	# deprecated
	function DfDeltaSweepConvMess_0x001c(Eprim_rel2::Number)
		mar = market(10,21,"0x001c")

		# Determinist sigma_rel = 0.0
		r = res(mar, "n31_p10_c21_0x001c_sigma0.h5")
		Df = DfPlotDeltaSweepMess(r, Eprim_rel2)

		# Stochastic sigma_rel = 0.5
		r = res(mar, "n31_p10_c21_0x001c_sigma0_5.h5")
		Df2 = DfPlotDeltaSweepMess(r, Eprim_rel2)

		# Stochastic sigma_rel = 1.0
		r = res(mar, "n31_p10_c21_0x001c_sigma1_0.h5")
		Df3 = DfPlotDeltaSweepMess(r, Eprim_rel2)

		# Stochastic sigma_rel = 0.7
		r = res(mar, "n31_p10_c21_0x001c_sigma0_7.h5")
		Df4 = DfPlotDeltaSweepMess(r, Eprim_rel2)

		Df = vcat(Df, Df2, Df3, Df4)
	end

	# deprecated
	function DfDeltaSweepConvSW_0xeeee(Eprim_rel2::Number)
		mar = market(10,21,"0xeeee")

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_7.h5")
		Df = DfPlotDeltaSweepSW(r, Eprim_rel2)

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_5.h5")
		Df2 = DfPlotDeltaSweepSW(r, Eprim_rel2)

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_2.h5")
		Df3 = DfPlotDeltaSweepSW(r, Eprim_rel2)

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_0.h5")
		Df4 = DfPlotDeltaSweepSW(r, Eprim_rel2)


		Df = vcat(Df, Df2, Df3, Df4)
	end

	# deprecated
	function DfDeltaSweepConvSW_test(Eprim_rel2::Number)
		mar = market(10,21,"0xeeee")

		r = res(mar, "n31_p10_c21_0xeeee.h5")
		Df = DfPlotDeltaSweepSW(r, Eprim_rel2)
	end

	# deprecated
	function DfDeltaSweepConvSW_test2(Eprim_rel2::Number)
		mar = market(30,80,"0x000f")

		r = res(mar, "n110_p30_c80_0x000f_test.h5")
		Df = DfPlotDeltaSweepSW(r, Eprim_rel2)
	end

	# deprecated
	function DfDeltaSweepConvMess_0xeeee(Eprim_rel2::Number)
		mar = market(10,21,"0xeeee")

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_7.h5")
		Df = DfPlotDeltaSweepMess(r, Eprim_rel2)

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_5.h5")
		Df2 = DfPlotDeltaSweepMess(r, Eprim_rel2)

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_2.h5")
		Df3 = DfPlotDeltaSweepMess(r, Eprim_rel2)

		r = res(mar, "n31_p10_c21_0xeeee_sigma0_0.h5")
		Df4 = DfPlotDeltaSweepMess(r, Eprim_rel2)

		Df = vcat(Df, Df2, Df3, Df4)
	end

	# deprecated
	function DfDeltaSweepConvSW_0x000f(Eprim_rel2::Number)
		mar = market(30,80,"0x000f")

		# r = res(mar,"D:/Julia/n110_p30_c80_0x000f_sigmaabsolute.h5")
		# Df = DfPlotDeltaSweepSW(r, Eprim_rel2)

		r = res(mar,"D:/Julia/n110_p30_c80_0x000f_sigmarelative.h5")
		Df = DfPlotDeltaSweepSW(r, Eprim_rel2)		
	end

	function DfDeltaSweep_resarray(mar::market)
		res_array = res[]
		if startswith(mar.name, "n110_p30_c80_0x000f")
		    push!(res_array, res(mar, "D:/Julia/n110_p30_c80_0x000f_sigmarelative.h5"))
		elseif startswith(mar.name, "n31_p10_c21_0xeeee")
		    push!(res_array, res(mar, "n31_p10_c21_0xeeee_sigma0_2.h5"))
		    push!(res_array, res(mar, "n31_p10_c21_0xeeee_sigma0_4.h5"))
            push!(res_array, res(mar, "n31_p10_c21_0xeeee_sigma0_6.h5"))
		    push!(res_array, res(mar, "n31_p10_c21_0xeeee_sigma0_8.h5"))
		elseif startswith(mar.name, "n31_p10_c21_0x001c")
		    push!(res_array, res(mar, "n31_p10_c21_0x001c_sigma0.h5"))
		    push!(res_array, res(mar, "n31_p10_c21_0x001c_sigma0_5.h5"))
		    push!(res_array, res(mar, "n31_p10_c21_0x001c_sigma0_7.h5"))
		    push!(res_array, res(mar, "n31_p10_c21_0x001c_sigma1_0.h5"))
		else 
		    println("	Please set the associated results files in this function.")
		end

		res_array
	end

	function DfDeltaSweepConvSW(mar::market, Eprim_rel2::Number)

		Df = DataFrame()
		res_array = DfDeltaSweep_resarray(mar)
		for res in res_array
			df = DfPlotDeltaSweepSW(res, Eprim_rel2)
			Df = vcat(Df, df)
		end

		Df
	end

	function DfDeltaSweepConvMess(mar::market, Eprim_rel2::Number)

		Df = DataFrame()
		res_array = DfDeltaSweep_resarray(mar)
		for res in res_array
			df = DfPlotDeltaSweepMess(res, Eprim_rel2)
			Df = vcat(Df, df)
		end

		Df
	end

	################################################################################
	# Plot functions
	################################################################################

	function plotDeltaSweepConvSW(Df::DataFrame; optRes::opt_res=optResult0)
		data = GenericTrace[]
		GDf = groupby(Df, :sigma)
		sw_min = +Inf
		sw_max = -Inf

		# Plot statistics for each sigma value
		for g in GDf
			### Convergence time
			df = by(g, [:delta, :sigma], Mean = :convTime=> mean, 
				STD = :convTime =>std)
			sig = string("σ=", df.sigma[1])
			trace = scatter(x=df.delta, y = df.Mean+df.STD,
								mode = "lines",
								line=attr(	width=0.5,
	                 						color="rgb(255, 255, 255)"),
								# fillcolor= RGB(68, 68, 68, 0.1),
	        					fill="tozeroy",
	        					opacity = 0,
	        					xaxis = "x",
	        					yaxis = "y",
								)
			push!(data,trace)

			trace = scatter(x=df.delta, y = df.Mean-df.STD,
								mode = "lines",
								line =attr(	width=0.5,
	                 						color="rgb(255, 188, 0)"),
								# fillcolor = RGB(68, 68, 68, 0.1),
	        					fill="tonexty",
	        					xaxis = "x",
	        					yaxis = "y",
								)
			push!(data,trace)
			
			trace = scatter(df, x=:delta, y=:Mean,
								name = sig, 
								# fill = "toself",
								xaxis = "x",
	        					yaxis = "y",
							)
			push!(data,trace)

			### Social welfare
			df = by(g, [:delta, :sigma], Mean = :social_welfare=> mean, 
				STD = :social_welfare =>std)
			trace = scatter(x=df.delta, y = df.Mean+df.STD,
								mode = "lines",
								line=attr(	width=0.5,
	                 						color="rgb(255, 255, 255)"),
								# fillcolor= RGB(68, 68, 68, 0.1),
	        					fill="tozeroy",
	        					opacity = 0,
	        					xaxis = "x",
	        					yaxis = "y2",
								)
			push!(data,trace)

			trace = scatter(x=df.delta, y = df.Mean-df.STD,
								mode = "lines",
								line =attr(	width=0.5,
	                 						color="rgb(255, 188, 0)"),
								# fillcolor = RGB(68, 68, 68, 0.1),
	        					fill="tonexty",
	        					xaxis = "x",
	        					yaxis = "y2",

								)
			push!(data,trace)
			
			trace = scatter(df, x=:delta, y=:Mean,
								name = sig, 
								# fill = "toself",
								xaxis = "x",
								yaxis = "y2",
							)
			push!(data,trace)

			sw_min = (minimum(df.Mean-3*df.STD) < sw_min) ? minimum(df.Mean-3*df.STD) : sw_min
			sw_max = (maximum(df.Mean+3*df.STD) > sw_max) ? maximum(df.Mean+3*df.STD) : sw_max
		end

		# Plot optimal social welfare value
		if (optRes.SW != 0.0)
			X = unique(Df.delta)
			Y = fill(optRes.SW, length(X))
			trace = scatter(	x=X, y=Y,
								mode = "lines",
								xaxis = "x",
								yaxis = "y2",
								line = attr(
										color = "rgb(207, 8, 61)",
										width = 4),
								name = "Optimal SW"
								)
			push!(data,trace)
		end

		layout = Layout(
				margin = attr(
								l = 60,
								r = 60,
								),
				# showlegend = false,
				xaxis =	attr(
								constraintowards = "bottom",
								anchor = "y2",
								title_text = "delta"
								),
				yaxis2 =attr(	
								domain = [0,0.45],
								range = [sw_min, sw_max],
								# side = "right",
								title_text = "Social welfare",
								# overlaying="y",
								titlefont=attr(
								            color="#ff7f0e"
								        ),
								tickfont=attr(
								            color="#ff7f0e"
								        ),
								),
				yaxis = attr(	
								domain = [0.5,1],

								title_text = "Tps conv",
								titlefont = attr(
								            color="#1f77b4"
								        ),
								tickfont = attr(
								            color="#1f77b4"
								        ),
								),
				)
		plot(data, layout)
	end

	function plotDeltaSweepConvMess(Df::DataFrame)
		data = GenericTrace[]
		GDf = groupby(Df, :sigma)

		for g in GDf
			### Convergence time
			df = by(g, [:delta, :sigma], Mean = :convTime=> mean, 
				STD = :convTime =>std)
			sig = string("σ=", df.sigma[1])
			trace = scatter(x=df.delta, y = df.Mean+df.STD,
								mode = "lines",
								line=attr(	width=0.5,
	                 						color="rgb(255, 255, 255)"),
								# fillcolor= RGB(68, 68, 68, 0.1),
	        					fill="tozeroy",
	        					opacity = 0,
	        					xaxis = "x",
	        					yaxis = "y",
								)
			push!(data,trace)

			trace = scatter(x=df.delta, y = df.Mean-df.STD,
								mode = "lines",
								line =attr(	width=0.5,
	                 						color="rgb(255, 188, 0)"),
								# fillcolor = RGB(68, 68, 68, 0.1),
	        					fill="tonexty",
	        					xaxis = "x",
	        					yaxis = "y",
								)
			push!(data,trace)
			
			trace = scatter(df, x=:delta, y=:Mean,
								name = sig, 
								# fill = "toself",
								xaxis = "x",
	        					yaxis = "y",
							)
			push!(data,trace)

			### Social welfare
			df = by(g, [:delta, :sigma], Mean = :number_messages=> mean, 
				STD = :number_messages =>std)
			trace = scatter(x=df.delta, y = df.Mean+df.STD,
								mode = "lines",
								line=attr(	width=0.5,
	                 						color="rgb(255, 255, 255)"),
								# fillcolor= RGB(68, 68, 68, 0.1),
	        					fill="tozeroy",
	        					opacity = 0,
	        					xaxis = "x",
	        					yaxis = "y2",
								)
			push!(data,trace)

			trace = scatter(x=df.delta, y = df.Mean-df.STD,
								mode = "lines",
								line =attr(	width=0.5,
	                 						color="rgb(255, 188, 0)"),
								# fillcolor = RGB(68, 68, 68, 0.1),
	        					fill="tonexty",
	        					xaxis = "x",
	        					yaxis = "y2",

								)
			push!(data,trace)
			
			trace = scatter(df, x=:delta, y=:Mean,
								name = sig, 
								# fill = "toself",
								xaxis = "x",
								yaxis = "y2",
							)
			push!(data,trace)
		end

		layout = Layout(
				margin = attr(
								l = 60,
								r = 60,
								),
				# showlegend = false,
				xaxis =	attr(
								constraintowards = "bottom",
								anchor = "y2",
								title_text = "delta"
								),
				yaxis2 =attr(	
								domain = [0,0.45],
								range = [20000, 40000],
								# side = "right",
								title_text = "Exchanged messages",
								# overlaying="y",
								titlefont=attr(
								            color="#ff7f0e"
								        ),
								tickfont=attr(
								            color="#ff7f0e"
								        ),
								),
				yaxis = attr(	
								domain = [0.5,1],
								title_text = "Tps conv",
								titlefont = attr(
								            color="#1f77b4"
								        ),
								tickfont = attr(
								            color="#1f77b4"
								        ),
								),
				)
		plot(data, layout)
	end
end