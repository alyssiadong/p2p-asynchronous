using DataFrames
using PlotlyJS
include("PowerNetwork.jl")
include("results.jl")
# mar = Res.market(30,80,"0x000f")
mar = Res.market(10,21,"0xeeee")
# r = Res.res(mar,"D:/Julia/n110_p30_c80_0x000f_sigmarelative.h5")

# mar = Res.market(10,21,"0xeeee")
# r = Res.res(mar,"n31_p10_c21_0xeeee_sigma0_2.h5")


# Df = Res.applyFunctionAtConvergence(r, "social_welfare", 1,1,2,1,0.5,sw)
# rename!(Df,Dict(:output=>:soc_wel))
# Res.applyFunctionAtConvergence!(Df, r, "trades", 1,1,2,1,0.5,f)

# Df = Res.convIterDeltaSweep(r, 0.5)
# Res.addSocialWelfare!(Df, r)

# Df = Res.convIterDeltaSweepSWMessages(r, 0.5)
# Df = Res.DfPlotDeltaSweep(r, 1e-7)
# Res.plotDeltaSweep(Df)

# Df = Res.DfDeltaSweepConvSW_0x001c(1e-7)
# Res.plotDeltaSweepConvSW(Df; optRes = Res.opt_res(r.mar.name, r.ρ, 1e-12))

# Df = Res.DfDeltaSweepConvMess_0x001c(1e-8)
# Res.plotDeltaSweepConvMess(Df)

# k,t1 = Res.readResAtConvergence(r,"trades", 1,1,3,1,1,0.01)
# k,sw1 = Res.readResAtConvergence(r,"social_welfare", 1,1,3,1,1,0.01)

# Df = Res.DfDeltaSweepConvSW_0xeeee(1e-7)
# Res.plotDeltaSweepConvSW(Df; optRes = Res.opt_res(r.mar.name, r.ρ, 1e-18))
# Res.plotDeltaSweepConvMess(Df)

# Df2 = Res.DfDeltaSweepConvSW_0x000f(1e-5)
# Res.plotDeltaSweepConvSW(Df; optRes = Res.opt_res(r.mar.name, r.ρ, 1e-12))

# soc_wel = Res.readRes(r, "social_welfare", 1,1,6,2,10)
# pr = Res.readRes(r, "primal_res", 1,1,6,2,10)

###
	# pn = PowerNetwork.power_network("n110_p30_c80_0x000f.csv", "full_P2P")
	# Eprim2_rel = 1e-9
	# R = [0.15, 0.3, 0.5, 0.8, 1.0, 2.0, 5.0, 8.0, 10.0, 
	# 	15.0, 30.0, 50.0, 75.0, 100.0]
	# Kmax = zeros(length(R))
	# for (r,ρ) ∈ enumerate(R)
	# 	println("Iteration ", r)
	# 	T, Lambda, kmax = PowerNetwork.optimal_res_admm(pn, ρ, Eprim2_rel)
	# 	Kmax[r] = kmax
	# end
	# plot(scatter(;x=R, y=Kmax))

	# R2 = R[5:end]
	# SW = zeros(length(R2))
	# for (r,ρ) ∈ enumerate(R2)
	# 	println("Iteration ", r)
	# 	T, Lambda, kmax = PowerNetwork.optimal_res_admm(pn, ρ, Eprim2_rel)
	# 	SW[r] = sum(abs.(T).*Lambda)
	# end

	# plot(scatter(x=R2, y=SW))

	# Df = DataFrame(:penalty_factor => R2, :social_welfare => SW, :conv_iter => Kmax[5:end])

	# data = GenericTrace[]
	# trace = scatter(Df, x=:penalty_factor, y=:conv_iter,
	#     name = "Convergence iteration", 
	#     yaxis = "y", 
	#     showlegend = false
	# 	)
	# push!(data, trace)
	# trace = scatter(Df, x=:penalty_factor, y=:social_welfare,
	#     name = "Social welfare", 
	#     yaxis = "y2",
	#     showlegend = false
	#     )
	# push!(data, trace)

	# layout = Layout(
	# 			yaxis = attr(
	# 				title="Conv Iter",
	# 		        titlefont=attr(
	# 		            color="#1f77b4"
	# 		        	),
	# 		        tickfont=attr(
	# 		            color="#1f77b4"
	# 		        	),
	# 				),
	# 			yaxis2 = attr(
	# 				titlefont=attr(
	#             		color="#ff7f0e"
	#         			),
	#         		tickfont=attr(
	#             		color="#ff7f0e"
	#         			),
	# 				title = "SW",
	# 				overlaying = "y",
	# 				side = "right",
	# 				anchor = "x"
	# 				)
	# 		)

	# plot(data,layout)


# pn = PowerNetwork.power_network("n110_p30_c80_0x000f.csv", "full_P2P")
# Eprim2_rel = 1e-9

# Df = PowerNetwork.penalty_factor_tuning(pn, Eprim2_rel)

# Df = Res.DfDeltaSweepConvSW_test(1e-12)	
# Res.plotDeltaSweepConvSW(Df; optRes = optRes)
mar = Res.market(30,80,"0x000f")
optRes = Res.opt_res(mar, 1e-15)
# Df = Res.DfDeltaSweepConvMess(mar, 1e-8)
# Res.plotDeltaSweepConvMess(Df)

# Df = Res.DfDeltaSweepConvSW(mar, 1e-9)
# Res.plotDeltaSweepConvSW(Df;optRes=optRes)

# mar = Res.market(10,21,"0xeeee")
# r = Res.res(mar,4)
# optRes = Res.opt_res(mar, 1e-15)
# par = PowerNetwork.power_network(string(mar.name,".csv"), "full_P2P")
# rho = par.preferred_rho

# # Res.applyFunctionAtConvergence(r, ["trades", "prices"], 1,1,1,1, 1e-5, (x,y)->Res.V(x,y,optRes, rho))
# out = Res.V(Res.readRes(r, ["trades", "prices"], 1,1,8,1,1), optRes, rho)

# plot(out)

r = Res.res(mar)
trades7 = Res.readRes(r,"trades",1,1,3,1,1)
trades8 = Res.readRes(r,"trades",1,1,6,1,1)
trades9 = Res.readRes(r,"trades",1,1,9,1,1)

trades10 = Res.readRes(r,"trades",1,1,10,1,1)
