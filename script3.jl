# include("PowerNetwork.jl")
# include("CommunicationDelays.jl")
# include("market_sim.jl")
include("write_results.jl")

using PlotlyJS
using DataFrames
using JLD

market_name = "n31_p10_c21_0xeeee"
market_name = "n3_p1_c2_0x0004"
market_name = "n110_p30_c80_0x000f"
market_name = "n9_p3_c6_0x0004"

JLDfile = WR.JLDFile(string(market_name,".jld"); add_param_values=false)



# layout = Layout(
# 		margin = attr(
# 						l = 60,
# 						r = 60,
# 						),
# 		# showlegend = false,
# 		xaxis =	attr(
# 						constraintowards = "bottom",
# 						anchor = "y2",
# 						title_text = "log(gamma)"
# 						),
# 		yaxis2 =attr(	
# 						domain = [0,0.45],
# 						# range = [sw_min, sw_max],
# 						# side = "right",
# 						title_text = "Objective function",
# 						# overlaying="y",
# 						titlefont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						tickfont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						),
# 		yaxis = attr(	
# 						domain = [0.5,1],

# 						title_text = "Social Welfare",
# 						titlefont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						tickfont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						),
# 		)

### INFLUENCE OF GAMMA ON OBJ. FUNCTION AND TRADES
# refDict = Dict("delta" => 1.0, "OSQPeps" => 1e-5)
# Df = WR.DfCompareTrades(JLDfile, 1e-9, refDict)
# Df.loggamma = log10.(Df.gamma)

# data = GenericTrace[]
# for df in groupby(Df, [:delta, :OSQPeps])
# 	trace = scatter(x=df.loggamma, y=df.objective_function, mode = "markers", 
# 		marker_size = 10, 
# 		name = string("OF_delta =", df.delta[1], ", OSQPeps =", df.OSQPeps[1]),
# 		yaxis = "y")
# 	push!(data, trace)
# 	trace = scatter(x=df.loggamma, y=df.compare_trades, mode = "markers", 
# 		marker_size = 10, 
# 		name = string("TC_delta =", df.delta[1], ", OSQPeps =", df.OSQPeps[1]),
# 		yaxis = "y2")
# 	push!(data, trace)
# end

# layout = Layout(
# 		margin = attr(
# 						l = 60,
# 						r = 60,
# 						),
# 		# showlegend = false,
# 		xaxis =	attr(
# 						constraintowards = "bottom",
# 						anchor = "y2",
# 						title_text = "log(gamma)"
# 						),
# 		yaxis2 =attr(	
# 						domain = [0,0.45],
# 						# range = [sw_min, sw_max],
# 						# side = "right",
# 						title_text = "Trades comparison",
# 						# overlaying="y",
# 						titlefont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						tickfont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						),
# 		yaxis = attr(	
# 						domain = [0.5,1],

# 						title_text = "Objective function",
# 						titlefont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						tickfont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						),
# 		)

### CONV TIME AND NUMBER OF MESSAGES
# Df = WR.DfSweepConvergence(JLDfile, 1e-9)
# Df = Df[Df.gamma .>= 0.1,:]

# data = GenericTrace[]
# for df in groupby(Df, [:sigma, :OSQPeps])
# 	trace = scatter(x=df.delta, y=df.conv_time, mode = "markers", 
# 		marker_size = 10, 
# 		name = string("sigma =", df.sigma[1], ", OSQPeps =", df.OSQPeps[1]),
# 		yaxis = "y")
# 	push!(data, trace)
# 	trace = scatter(x=df.delta, y=df.sum_messages, mode = "markers", 
# 		marker_size = 10, 
# 		name = string("sigma =", df.sigma[1], ", OSQPeps =", df.OSQPeps[1]),
# 		yaxis = "y2")
# 	push!(data, trace)
# end

# layout = Layout(
# 		margin = attr(
# 						l = 60,
# 						r = 60,
# 						),
# 		# showlegend = false,
# 		xaxis =	attr(
# 						constraintowards = "bottom",
# 						anchor = "y2",
# 						title_text = "delta"
# 						),
# 		yaxis2 =attr(	
# 						domain = [0,0.45],
# 						# range = [sw_min, sw_max],
# 						# side = "right",
# 						title_text = "Number of messages",
# 						# overlaying="y",
# 						titlefont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						tickfont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						),
# 		yaxis = attr(	
# 						domain = [0.5,1],

# 						title_text = "Convergence time",
# 						titlefont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						tickfont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						),
# 		)

# Df = WR.DfSweepConvergence(JLDfile, 1e-9)
# Df = Df[Df.gamma .== 3,:]

# data = GenericTrace[]
# for df in groupby(Df, [:tirages])
# 	trace = scatter(x=df.delta, y=df.conv_time, mode = "markers", 
# 		marker_size = 10, 
# 		name = string("gamma =", df.gamma[1], ", OSQPeps =", df.OSQPeps[1], ", sigma =", df.sigma[1]),
# 		yaxis = "y")
# 	push!(data, trace)
# 	trace = scatter(x=df.delta, y=df.sum_messages, mode = "markers", 
# 		marker_size = 10, 
# 		name = string("gamma =", df.gamma[1], ", OSQPeps =", df.OSQPeps[1], ", sigma =", df.sigma[1]),
# 		yaxis = "y2")
# 	push!(data, trace)
# end

# layout = Layout(
# 		margin = attr(
# 						l = 60,
# 						r = 60,
# 						),
# 		# showlegend = false,
# 		xaxis =	attr(
# 						constraintowards = "bottom",
# 						anchor = "y2",
# 						title_text = "delta"
# 						),
# 		yaxis2 =attr(	
# 						domain = [0,0.45],
# 						# range = [sw_min, sw_max],
# 						# side = "right",
# 						title_text = "Number of messages",
# 						# overlaying="y",
# 						titlefont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						tickfont=attr(
# 						            color="#ff7f0e"
# 						        ),
# 						),
# 		yaxis = attr(	
# 						domain = [0.5,1],

# 						title_text = "Convergence time",
# 						titlefont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						tickfont = attr(
# 						            color="#1f77b4"
# 						        ),
# 						),
# 		)

# plot(data, layout)

## trade residual at convergence for each trade in respect to the distance

df = JLDfile.df_params
df = df[(df.gamma.==3).&(df.delta.==0.1),:]

Df = WR.DfSweepTradewiseConvergence(JLDfile, 1e-6, df)

data = GenericTrace[]
for gf in groupby(Df, :tirages)
	trace = scatter( x=gf.distance, y=log10.(gf.trade_res), 
		mode = "markers",
		name = string(gf.tirages[1]),
		)
	push!(data, trace)
end
plot(data)



#################

# include("PowerNetwork.jl")

# pn = PowerNetwork.power_network("n31_p10_c21_0x001c.csv", 
# 	"full_P2P")
# ρ = pn.preferred_rho
# γ = 0.5

# T, Lambda, kmax = PowerNetwork.optimal_res_admm(pn, 
# 	ρ, 1e-12, γ)

# P = dropdims(sum(T, dims=2), dims=2)

# local_objective = 2*pn.gencost_a.*P.^2 + pn.gencost_b.*P
# global_objective = sum(local_objective)

# P
