function save_results(rc::RemoteChannel, N_points::Int, JLDfile::WR.JLDFile)

	n = N_points
	while n>0
		mem, df = take!(rc)
		NID = df.NID
		WR.addpoint_atconvergence(JLDfile, NID, mem, 1e-12)
		println(n, " saved.")
		n = n-1
	end
end
 
@everywhere function launch_computation( df::DataFrameRow, rc::RemoteChannel, 
	par::PowerNetwork.power_network, N_prodcons::Array{Int})

	α = df.alpha
	β = df.beta
	σ = df.sigma
	delta = df.delta
	gamma = df.gamma
	rho = df.penalty_factor
	sendType = df.sendType
	iterMatching = df.iterMatching
	priceUpdate = df.priceUpdate
	tradeUpdate = df.tradeUpdate
	δt = df.sampling_period
	Tsimu = df.Tsimu
	OSQPeps = df.OSQPeps

	Ntrig = vcat(fill(ceil(Int,N_prodcons[2]*delta), N_prodcons[1]), 
		fill(ceil(Int,N_prodcons[1]*delta), N_prodcons[2]))

	Tmax = fill(df.DeltaT, sum(N_prodcons))

	aPar = MarketSimulation.algoParams(
		rho, gamma, OSQPeps, "OSQP", Ntrig, Tmax, sendType, 
		iterMatching, priceUpdate, tradeUpdate)
	nPar = CommunicationDelays.commParams(α, β, σ)

	mem = MarketSimulation.run_simulation(Tsimu, δt, par, nPar, aPar)

	put!(rc, (mem, df))
end

rc = RemoteChannel(()->
	Channel{Tuple{MarketSimulation.memoryArrays, DataFrameRow}}(N_points))

@async save_results(rc, N_points, JLDfile)

@sync @distributed for i in range(1,stop=size(DF,1))
	launch_computation(DF[i,:], rc, par, N_prodcons) 
end
