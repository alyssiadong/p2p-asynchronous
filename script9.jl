## Analyzing data from the RPi Asynchronous opENS system

using CSV, DataFrames
using PlotlyJS
using ORCA
using Statistics

# filepath = "chronograf_results/2020-03-25-11-34 Chronograf Data.csv"
# data = CSV.read(filepath)
# data = data[completecases(data),:]

# rename!(data, Dict(
# 				"mqtt_consumer.dual_res_ij" => "dual_res_ij",
# 				"mqtt_consumer.primal_res_ij" => "primal_res_ij",
# 				"mqtt_consumer.from" => "from_i",
# 				"mqtt_consumer.to" => "to_j",
# 				"mqtt_consumer.ki" => "k_i",
# 				"mqtt_consumer.k_ij" => "k_ij",
# 				"mqtt_consumer.price_ij" => "price_ij",
# 				"mqtt_consumer.timestamp_send" => "timestamp_send",
# 				"mqtt_consumer.trade_ij" => "trade_ij",
# 				"mqtt_consumer.dt_comm" => "dt_comm",
# 				"mqtt_consumer.dt_stall" => "dt_stall",
# 				"mqtt_consumer.dt_comp" => "dt_comp",
# 				))

# i = 1
# j = 6

# # vij
# da = data[(data.from_i .== i) .& (data.to_j .== j),:]
# # vji
# db = data[(data.from_i .== j) .& (data.to_j .== i),:]

# # Communication delays correction
# ma = median(da.dt_comm)
# mb = median(db.dt_comm)
# dt = (mb-ma)/2
# da.dt_comm_corr = da.dt_comm .+ dt
# db.dt_comm_corr = db.dt_comm .- dt

# trace1 = histogram(x=da.dt_iteration, 
# 					opacity=0.75, 
# 					# xbins=Dict(:start => -0.1, :end =>0.1, :size => 0.001),
# 					name = string("delais_", da.from_i[1], da.to_j[1])
# 					)
# trace2 = histogram(x=db.dt_iteration, 
# 					opacity=0.75, 
# 					# xbins=Dict(:start => -0.1, :end => 0.1, :size => 0.001),
# 					name = string("delais_", db.from_i[1], db.to_j[1])
# 					)

# data = [trace1, trace2]
# layout = Layout(barmode="overlay")
# plot(data, layout)

# trace3 = box(y=d03.dt_comm)
# plot(trace3)
# 

## Testing RPiAsync.jl
include("RPiAsync.jl")
# RPiAsync.plot_delay_histo()
# RPiAsync.test03()

# filepath = "chronograf_results/alpha0 - delta1 - messages.csv"
# Df = RPiAsync.create_dataframe(filepath)
# RPiAsync.fix_comm_delays!(Df)
# RPiAsync.analyze_delays(Df)

# filepath = "chronograf_results/2020-04-08-17-32 Chronograf Data- delta0_4 - monitor.csv"

# Df = RPiAsync.create_dataframe_monitor(filepath)
# gdf = groupby(Df, :i, sort = true)


# Df.global_primal_res = zeros(size(Df,1))
# for id in 1:size(Df,1)
# 	t0 = Df.datetime[id]

# 	# Find pointers for each subgroup
# 	# pointers = zeros(Int, length(gdf))
# 	# for idf in 1:length(gdf)
# 	# 	pointers[idf] = findlast(x->x==true, gdf[idf].datetime .<= t0)
# 	# end
# 	pointers = [findlast(x->x==true, gdf[idf].datetime .<= t0) for idf in 1:length(gdf)]
# 	if all(pointers .!= nothing)
# 		Df.global_primal_res[id] = sum([gdf[idf].local_primal_res[pointers[idf]] for idf in 1:length(gdf)])
# 	end
# end
# 
# RPiAsync.test03();
# DF = RPiAsync.DF_primal_res(" - p0_05")
# DF = RPiAsync.DF_primal_res_loss()
# RPiAsync.plot_primal_res_loss(DF)

# RPiAsync.plot_async_gain(DF, 1e-8)
# RPiAsync.plot_async_gain2(DF, 1e-8)
# savefig(RPiAsync.plot_async_gain(DF, 1e-8),"images_SGE/conv_loss2.pdf", format = "pdf")
# savefig(RPiAsync.plot_async_gain2(DF, 1e-8),"images_SGE/conv_loss.pdf", format = "pdf")

# DF = RPiAsync.DF_primal_res("")
# RPiAsync.plot_primal_res(DF)
# for alpha in [0, 2, 4]
# 	savefig( RPiAsync.plot_primal_res(DF, alpha), string("images_SGE/conv_alpha",alpha,".pdf"), format = "pdf")
# end
# RPiAsync.plot_primal_res(2)
# RPiAsync.plot_delay_histo()
# savefig(RPiAsync.plot_delay_histo(), "images_SGE/histo_delais.pdf", format = "pdf")

# RPiAsync.plot_delay_histo(4,2,8)

# savefig(RPiAsync.plot_primal_res(DF), "images_SGE/convergence.pdf", format = "pdf")
# RPiAsync.plot_primal_res(DF)
# 
# RPiAsync.plot_local_primal_res(alpha=0.5, delta=0.4,ntrig_texts=[" - p0_05", ""])
# RPiAsync.plot_local_primal_res(alpha=0.5, delta=0.1,ntrig_texts=[""])
# RPiAsync.analyze_delays("chronograf_results/cccd - alpha1_0 - delta0_1 - ntrig2_6 - messages.csv")
# RPiAsync.analyze_delays("chronograf_results/cccd - alpha1_0 - delta0_1 - messages.csv")
# 
# Df = RPiAsync.DF_primal_res_pause()
# RPiAsync.plot_primal_res_pause(Df)
# savefig(RPiAsync.plot_primal_res_pause(Df), "images_SGE/convergence_pause.pdf", format = "pdf")

# DF = RPiAsync.DF_primal_res("")
# RPiAsync.plot_primal_res(DF)
# savefig(RPiAsync.plot_primal_res(DF), "images_SGE/convergence.pdf", format = "pdf")

# DF2 = RPiAsync.DF_primal_res_loss()
# RPiAsync.plot_async_gain2(DF2, 1e-6)
savefig(RPiAsync.plot_async_gain2(DF2, 1e-6), "images_SGE/conv_loss2.pdf", format = "pdf")

# DF = RPiAsync.DF_primal_res("")
# DF_mes = RPiAsync.DF_messages()
# RPiAsync.plot_message(DF_mes, DF)
# savefig(RPiAsync.plot_message(DF_mes, DF), "images_SGE/conv_mess.pdf", format = "pdf")
# RPiAsync.plot_message2(DF_mes, DF)
# savefig(RPiAsync.plot_message2(DF_mes, DF), "images_SGE/conv_mess2.pdf", format = "pdf")