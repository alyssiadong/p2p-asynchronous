module Planification
### Lists all planified errors and pause of agents.
### Warm start parameters.
### Error management strategies?

	using ..PowerNetwork
	using LightGraphs, SimpleWeightedGraphs
	using Printf

	export planStruct

	import Base.show
    export show

################################################################################
# Trigger types declaration and constructors
################################################################################
	abstract type Trigger end

### Timestamp
	struct Timestamp <: Trigger  
		abs_time::Number
		timeout::Number

		function Timestamp(abs_time::Number, timeout::Number)
			@assert timeout > 0 "Timeout must be strictly positive."
			@assert abs_time ≥ 0 "Timestamp must be positive."
			new(abs_time,timeout)
		end
	end

	function show(io::IO, trigger::Timestamp)
		println("Timestamp trigger:")
		println("              abs_time = ", trigger.abs_time)
		println("               timeout = ", trigger.timeout)
	end

### LocalIteration
	struct LocalIteration <: Trigger 
		# Agent i's local iteration k_i
		k::UInt
		timeout::Number

		function LocalIteration(k::Int, timeout::Number)
			@assert timeout > 0 "Timeout must be strictly positive."
			@assert k > 0 "Iteration must be positive."
			new(UInt(k),timeout)
		end
	end

	function show(io::IO, trigger::LocalIteration)
		println("Local iteration trigger:")
		println("                   k_i = ", trigger.k)
		println("               timeout = ", trigger.timeout)
	end

### TradewiseIteration
	struct TradewiseIteration <: Trigger 
		# Agent i's tradewise iteration k_ij
		k::UInt
		j::Int
		timeout::Number

		function TradewiseIteration(k::Int, j::Int, timeout::Number)
			@assert timeout > 0 "Timeout must be strictly positive."
			@assert k > 0 "Iteration must be positive."
			@assert j > 0 "Partners' ID must be strictly positive."
			new(UInt(k),j,timeout)
		end
	end

	function show(io::IO, trigger::TradewiseIteration)
		println("Tradewise iteration trigger:")
		println("                  k_ij = ", trigger.k)
		println("          trade with j = ", trigger.j)
		println("               timeout = ", trigger.timeout)
	end

### LocalPrimalResidual
	struct LocalPrimalResidual <: Trigger 
		# Sets off when the local primal residual rp_i < ε_trig
		ε_trig::Number
		timeout::Number

		function LocalPrimalResidual(epsilon::Number, timeout::Number)
			@assert timeout > 0 "Timeout must be strictly positive."
			@assert epsilon > 0 "Epsilon must be strictly positive."
			new(epsilon,timeout)
		end
	end

	function show(io::IO, trigger::LocalPrimalResidual)
		println("Local primal residual trigger:")
		println("                ε_trig = ", trigger.ε_trig)
		println("               timeout = ", trigger.timeout)
	end

### TradewisePrimalResidual
	struct TradewisePrimalResidual <: Trigger 
		ε_trig::Number
		j::Int
		timeout::Number

		function TradewisePrimalResidual(epsilon::Number, j::Int, timeout::Number)
			@assert timeout > 0 "Timeout must be strictly positive."
			@assert epsilon > 0 "Epsilon must be strictly positive."
			@assert j > 0 "Partners' ID must be strictly positive." 
			new(epsilon,j,timeout)
		end
	end

	function show(io::IO, trigger::TradewisePrimalResidual)
		println("Tradewise primal residual trigger:")
		println("                ε_trig = ", trigger.ε_trig)
		println("          trade with j = ", trigger.j)
		println("               timeout = ", trigger.timeout)
	end

################################################################################
# Planification structure
# For now: only pause
################################################################################

	struct planStruct
		power_network::power_network
		trigger_list::Dict{Int64, Array{<:Trigger,1}}	# Dict(i=>triggers)
		warm_start::Dict{String, Array{<:Number,2}}		# Dict("T"=>[tij^0], "Λ"=>[λij^0])

		function planStruct(pn::power_network)
			trigger_list = Dict(i => Array{Trigger,1}() for i in vertices(pn.comm_graph))
			N = nv(pn.comm_graph)
			warm_start = Dict(	"T" => zeros(N,N),
								"Λ" => zeros(N,N))
			new(pn, trigger_list, warm_start)
		end
	end

	function show(io::IO, str::planStruct)
		println("Planification struct associated to ", str.power_network.filename)
		# println("    Number of agent trigger(s): ", length(str.trigger_list))
	end

	### Add a trigger
	function addTrigger!(plan::planStruct, agent::Int, trig::Trigger)
		if typeof(trig) ∈ [TradewiseIteration, TradewisePrimalResidual]
			checkAgent(plan, agent, trig.j)
		else
			checkAgent(plan, agent)
		end

		if agent in keys(plan.trigger_list)
			trig_old = plan.trigger_list[agent]
			addTrigger(plan, agent, trig_old, trig)
		else
			plan.trigger_list[agent] = [trig]
		end
	end

	function addTrigger(plan::planStruct, agent::Int, trig_old::Array{<:Trigger,1}, trig_new::Trigger)
		if !(trig_new in trig_old)
			trig = vcat(trig_old, trig_new)
			plan.trigger_list[agent] = trig
		end
	end

	function setWarmStart!(plan::planStruct, name::String, val::Array{<:Number,2})
		@assert name ∈ ["T", "Λ"] "Incorrect name."
		plan.warm_start[name] = val
	end

	### Check that agent i is in the power network, and that agent j is a partner of agent i
	function checkAgent(plan::planStruct, agent_i::Int)
		@assert agent_i ∈ vertices(plan.power_network.comm_graph) @sprintf("Agent %d is not part of the network.", agent_j)
	end

	function checkAgent(plan::planStruct, agent_i::Int, agent_j::Int)
		@assert agent_i ∈ vertices(plan.power_network.comm_graph) @sprintf("Agent %d is not part of the network.", agent_i)
		@assert agent_j ∈ vertices(plan.power_network.comm_graph) @sprintf("Agent %d is not part of the network.", agent_j)

		@assert has_edge(plan.power_network.comm_graph, agent_i, agent_j) @sprintf("Agents %d and %d are not partners.", agent_i, agent_j)
	end
	

end