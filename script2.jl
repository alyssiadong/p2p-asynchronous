using PlotlyJS
using HDF5
using DataFrames
using Statistics

# filename = "results/n110_p30_c80_0x000f.h5"
# Delta = [0.1, 0.4, 0.8, 1.0]
	
# fid = h5open(filename, "r")

# data = GenericTrace[]

# for (i,delta) in enumerate(Delta)
# 	try
# 		g = fid[string(delta)]
# 		trades = read(g, "trades")
# 		prices = read(g, "prices")
# 		social_welfare = dropdims(sum(abs.(sum(trades.*prices, 
# 			dims=2)),dims=1), dims=(1,2))
# 		trace = scatter(;y=social_welfare, name = string(delta))

# 		push!(data,trace)
# 	catch exc
# 		close(fid)
# 		rethrow(exc)
# 	end
	
# end
# close(fid)

# plot(data)

# data = GenericTrace[]
# filename = "market_p2p.h5"
# fid = h5open(filename,"r")

# for (i,delta) in enumerate(Delta)

# 	try
# 		g = fid[string("P2P/",delta,"/alpha6_beta3")]
# 		trades = read(g, "trades")
# 		prices = read(g, "prices")
# 		social_welfare = dropdims(sum(abs.(sum(trades.*prices, 
# 			dims=2)),dims=1), dims=(1,2))
# 		SW[:,i] = social_welfare
# 		trace = scatter(;y=social_welfare, name = string(delta))

# 		push!(data,trace)
# 	catch exc
# 		close(fid)
# 		rethrow(exc)
# 	end
	
# end
# close(fid)
# plot(data)

### Temps de convergence et nombre de messages en fonction de delta
	function plot_deltasweep()
		fid = h5open("results/n110_p30_c80_0x000f_deltasweep.h5","r")
		D = read(fid["algoParams"], "D")
		δt = 20
		Df = DataFrame(:delta => Float64[], :conv_iter => Int64[], :nb_mes => Int64[])

		N_delta = 20
		for d in 1:N_delta
			if exists(fid,string(d))
				try
					g = fid[string(d,"/alpha1_beta1")]
					prim_array = read(g, "primal_res")
					messages = read(g, "message")

					prim = dropdims(sum(prim_array,dims=1),dims=1)
					conv_iter = maximum(findall(x->(x>1.0), prim))

					mes_iter = sum(messages[:,:,conv_iter])

					push!(Df, [D[d], conv_iter, mes_iter])
				catch exc
					close(fid)
					rethrow(exc)
				end
			end
		end
		close(fid)

		Df[:conv_time] = Df[:conv_iter].*δt

		data = GenericTrace[]
		trace = scatter(Df, x= :delta, y= :conv_time,)
		push!(data, trace)
		trace = scatter(Df, x= :delta, y= :nb_mes,
				yaxis = "y2",
				xaxis = "x",
				)
		push!(data, trace)

		layout = Layout(
				showlegend = false,
				xaxis=attr(		
								constraintowards = "bottom",
								anchor = "y2",
								title_text = "delta"),
				yaxis2 = attr(	
								# domain = [0,0.4],
								side = "right",
								title_text = "Messages échangés",
								overlaying="y",
								titlefont=attr(
								            color="#ff7f0e"
								        ),
								tickfont=attr(
								            color="#ff7f0e"
								        ),
								),
				yaxis = attr(	
								# domain = [0.4, 1],
								title_text = "Tps conv",
								titlefont=attr(
								            color="#1f77b4"
								        ),
								tickfont=attr(
								            color="#1f77b4"
								        ),
								),
				)

		plot(data, layout)
	end

### 
	function plot_alphabetasweep(Df::DataFrame)
		Df2 = Df[Df[:a].==15,:]

		data = GenericTrace[]
		trace = scatter(Df2, x= :beta, y= :conv_time,)
		push!(data, trace)
		trace = scatter(Df2, x= :beta, y= :nb_mes,
				yaxis = "y2",
				xaxis = "x",
				)
		push!(data, trace)

		layout = Layout(
				margin = attr(
								l = 60,
								r = 60,
						),
				showlegend = false,
				xaxis=attr(		
								constraintowards = "bottom",
								anchor = "y2",
								title_text = "beta"),
				yaxis2 = attr(	
								# domain = [0,0.4],
								side = "right",
								title_text = "Messages échangés",
								overlaying="y",
								titlefont=attr(
								            color="#ff7f0e"
								        ),
								tickfont=attr(
								            color="#ff7f0e"
								        ),
								),
				yaxis = attr(	
								# domain = [0.4, 1],
								title_text = "Tps conv",
								titlefont=attr(
								            color="#1f77b4"
								        ),
								tickfont=attr(
								            color="#1f77b4"
								        ),
								),
				)

		plot(data, layout)
	end

	function plot_heatmap(Df::DataFrame)

		data = GenericTrace[]
		trace = heatmap( 
						x=Df[:alpha], 
						y=Df[:beta], 
						z=Df[:conv_time],#./Df[:mean_delay],
						showscale = false,
						xaxis = "x",
						yaxis = "y",
						)
		push!(data,trace)
		trace = heatmap(
						x=Df[:alpha], 
						y=Df[:beta], 
						z=Df[:nb_mes],
						showscale = false,
						xaxis = "x2",
						yaxis = "y",
						)
		push!(data,trace)

		layout = Layout(
				width = 900,
				height = 450,
				margin = attr(
								l = 60,
								r = 20,
								b = 50,
								t = 30,
							), 
				showlegend = false,
				xaxis=attr(		
								domain = [0,0.45],
								title_text = "alpha",
								),
				xaxis2 = attr(	
								domain = [0.5,1],
								title_text = "alpha",
								),
				yaxis = attr(	
								title_text = "beta",
								domain = [0,1],
								anchor = "x",
								),
				)

		plot(data, layout)
	end

	function read_results_alphabetasweep()
		fid = h5open("results/n110_p30_c80_0x000f_alphabetasweep.h5","r")
		A = read(fid["algoParams"], "A")
		B = read(fid["algoParams"], "B")
		δt = 20
		Df = DataFrame(:a => Int64[], :b => Int64[], 
			:alpha => Float64[], :beta => Float64[],
			:conv_iter => Int64[], :nb_mes => Int64[], 
			:mean_delay => Float64[])

		N_alpha = length(A)
		N_beta = length(B)
		for (a,b) in Iterators.product(1:N_alpha, 1:N_beta)
			if exists(fid,string("1/alpha", a, "_beta", b))
				try
					g = fid[string("1/alpha", a, "_beta", b)]
					prim_array = read(g, "primal_res")
					messages = read(g, "message")

					pn = PowerNetwork.power_network("n110_p30_c80_0x000f.csv", 
						"full_P2P")
					comPar = CommunicationDelays.commParams(A[a],B[b])
					mean_delay = CommunicationDelays.meanDelay(comPar, pn)

					prim = dropdims(sum(prim_array,dims=1),dims=1)
					conv_iter = maximum(findall(x->(x>1.0), prim))

					mes_iter = sum(messages[:,:,conv_iter])

					push!(Df, [a, b, A[a], B[b], 
						conv_iter, mes_iter, mean_delay])
				catch exc
					close(fid)
					rethrow(exc)
				end
			end
		end
		close(fid)

		Df[:conv_time] = Df[:conv_iter].*δt
		Df
	end

###

	function read_results_deltasigmasweep()
		fid = h5open("D:/Julia/n110_p30_c80_0x000f.h5","r")
		S = read(fid["algoParams"], "S")
		D = read(fid["algoParams"], "D")

		δt = 20
		Df = DataFrame(:s => Int64[], :d => Int64[], 
			:sigma => Float64[], :delta => Float64[],  
			:conv_iter => Int64[], :nb_mes => Int64[])

		N_sigma = 15
		N_delta = 20

		for (s,d) in Iterators.product(1:N_sigma, 1:N_delta)
			if exists(fid,string(d,"/alpha1_beta1_sigma", s))
				try
					g = fid[string(d,"/alpha1_beta1_sigma", s)]
					prim_array = read(g, "primal_res")
					messages = read(g, "message")

					prim = dropdims(sum(prim_array,dims=1),dims=1)
					conv_iter = maximum(findall(x->(x>10.0), prim))

					mes_iter = sum(messages[:,:,conv_iter])

					push!(Df, [s,d, S[s], D[d], conv_iter, mes_iter])
				catch exc
					close(fid)
					rethrow(exc)
				end
			end
		end
		close(fid)

		Df[:conv_time] = Df[:conv_iter].*δt

		Df
	end

	function plot_deltasweep(Df::DataFrame, s::Int64)

		Df2 = Df[Df.s .==s,:]

		data = GenericTrace[]
		trace = scatter(Df2, x= :delta, y= :conv_time,)
		push!(data, trace)
		trace = scatter(Df2, x= :delta, y= :nb_mes,
				yaxis = "y2",
				xaxis = "x",
				)
		push!(data, trace)

		layout = Layout(
				margin = attr(
								l = 60,
								r = 60,
								),
				showlegend = false,
				xaxis=attr(
								constraintowards = "bottom",
								anchor = "y2",
								title_text = "delta"
								),
				yaxis2 = attr(	
								# domain = [0,0.4],
								side = "right",
								title_text = "Messages échangés",
								overlaying="y",
								titlefont=attr(
								            color="#ff7f0e"
								        ),
								tickfont=attr(
								            color="#ff7f0e"
								        ),
								),
				yaxis = attr(	
								# domain = [0.4, 1],
								title_text = "Tps conv",
								titlefont=attr(
								            color="#1f77b4"
								        ),
								tickfont=attr(
								            color="#1f77b4"
								        ),
								),
				)

		plot(data, layout)
	end

	function read_results_tirages()

		par = PowerNetwork.power_network("n31_p10_c21_0x001c.csv", "full_P2P")

		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))

		fid = h5open("results/n31_p10_c21_0x001c_sigma5.h5","r")
		S = read(fid["algoParams"], "S")
		D = read(fid["algoParams"], "D")

		δt = 1
		Df = DataFrame(:s => Int64[], :d => Int64[], 
			:sigma => Float64[], :delta => Float64[],  
			:conv_iter => Int64[], :nb_mes => Int64[])

		N_tirages = 150
		N_delta = 21
		s = 1
		Eprim2_rel = 1e-6

		for (d,t) in Iterators.product(1:N_delta, 1:N_tirages)
			if exists(fid,string(d,"/alpha1_beta1_sigma1_tirage", t))
				try
					g = fid[string(d,"/alpha1_beta1_sigma1_tirage", t)]
					prim_array = read(g, "primal_res")
					messages = read(g, "message")

					prim = dropdims(sum(prim_array,dims=1),dims=1)
					rmax, kmin = findmax(prim)
					test = prim .- Eprim2_rel*optimal_power_exchange
					# conv_iter = maximum(findall(x->(x>0), test))
					if kmin == length(test)
						conv_iter = kmin
					else
						try
							conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
							# conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
						catch
							conv_iter = length(test)
						end
					end

					mes_iter = sum(messages[:,:,conv_iter])

					push!(Df, [s,d, 5.0, D[d], conv_iter, mes_iter])
				catch exc
					close(fid)
					rethrow(exc)
				end
			end
		end
		close(fid)

		fid = h5open("results/n31_p10_c21_0x001c_sigma15.h5","r")
		for (d,t) in Iterators.product(1:N_delta, 1:N_tirages)
			if exists(fid,string(d,"/alpha1_beta1_sigma1_tirage", t))
				try
					g = fid[string(d,"/alpha1_beta1_sigma1_tirage", t)]
					prim_array = read(g, "primal_res")
					messages = read(g, "message")

					prim = dropdims(sum(prim_array,dims=1),dims=1)
					rmax, kmin = findmax(prim)
					test = prim .- Eprim2_rel*optimal_power_exchange
					# conv_iter = maximum(findall(x->(x>0), test))
					if kmin == length(test)
						conv_iter = kmin
					else
						try
							conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
							# conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
						catch
							conv_iter = length(test)
						end
					end

					mes_iter = sum(messages[:,:,conv_iter])

					push!(Df, [s,d, 15.0, D[d], conv_iter, mes_iter])
				catch exc
					close(fid)
					rethrow(exc)
				end
			end
		end
		close(fid)

		Df[:conv_time] = Df[:conv_iter].*δt

		Df
	end

	function plot_2(Df::DataFrame)
		df = by(Df, [:delta, :sigma], Mean = :conv_time=> mean, 
			STD = :conv_time =>std)
		data = GenericTrace[]

		df1 = df[df.sigma.==5,:]
		df2 = df[df.sigma.==15,:]
		trace = scatter(df1, x=:delta, y=:Mean,
							error_y = attr(
										array = df1.STD,
										visible = true,
										),
							name = "sigma=5"
						)
		push!(data,trace)
		trace = scatter(df2, x=:delta, y=:Mean,
							error_y = attr(
										array = df2.STD,
										visible = true,
										),
							name = "sigma=15"
						)
		push!(data,trace)
		
		plot(data)
	end

	function read_results_tirages2()

		par = PowerNetwork.power_network("n31_p10_c21_0x001c.csv", "full_P2P")

		pmin = par.Pmin
		pmax = par.Pmax
		optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))

		fid = h5open("results/n31_p10_c21_0x001c_sigma0_5.h5","r")
		S = read(fid["algoParams"], "S")
		D = read(fid["algoParams"], "D")

		δt = 1
		Df = DataFrame(:s => Int64[], :d => Int64[], 
			:sigma => Float64[], :delta => Float64[],  
			:conv_iter => Int64[], :nb_mes => Int64[],
			:soc_wel => Float64[])

		N_tirages = 150
		N_delta = 21
		s = 1
		Eprim2_rel = 1e-7

		for (d,t) in Iterators.product(1:N_delta, 1:N_tirages)
			if exists(fid,string(d,"/alpha1_beta1_sigma1_tirage", t))
				try
					g = fid[string(d,"/alpha1_beta1_sigma1_tirage", t)]
					prim_array = read(g, "primal_res")
					messages = read(g, "message")
					soc_wel = read(g, "social_welfare")

					prim = dropdims(sum(prim_array,dims=1),dims=1)
					rmax, kmin = findmax(prim)
					test = prim .- Eprim2_rel*optimal_power_exchange
					# conv_iter = maximum(findall(x->(x>0), test))
					if kmin == length(test)
						conv_iter = kmin
					else
						try
							conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
							# conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
						catch
							conv_iter = length(test)
						end
					end

					mes_iter = sum(messages[:,:,conv_iter])

					push!(Df, [s,d, 0.5, D[d], conv_iter, mes_iter, soc_wel[conv_iter]])
				catch exc
					close(fid)
					rethrow(exc)
				end
			end
		end
		close(fid)

		fid = h5open("results/n31_p10_c21_0x001c_sigma0.h5","r")
		for d in 1:N_delta
			if exists(fid,string(d,"/alpha1_beta1_sigma1_tirage", 1))
				try
					g = fid[string(d,"/alpha1_beta1_sigma1_tirage", 1)]
					prim_array = read(g, "primal_res")
					messages = read(g, "message")
					soc_wel = read(g, "social_welfare")

					prim = dropdims(sum(prim_array,dims=1),dims=1)
					rmax, kmin = findmax(prim)
					test = prim .- Eprim2_rel*optimal_power_exchange
					# conv_iter = maximum(findall(x->(x>0), test))
					if kmin == length(test)
						conv_iter = kmin
					else
						try
							conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
							# conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
						catch
							conv_iter = length(test)
						end
					end

					mes_iter = sum(messages[:,:,conv_iter])

					push!(Df, [s,d, 0, D[d], conv_iter, mes_iter, soc_wel[conv_iter]])
				catch exc
					close(fid)
					rethrow(exc)
				end
			end
		end
		close(fid)

		Df[:conv_time] = Df[:conv_iter].*δt

		Df
	end

	function plot_3(Df::DataFrame)
		df = by(Df, [:delta, :sigma], Mean = :conv_time=> mean, 
			STD = :conv_time =>std)
		data = GenericTrace[]

		df1 = df[df.sigma.==0.5,:]
		df2 = df[df.sigma.==0,:]

		trace = scatter(df1, x=:delta, y=:Mean,
							error_y = attr(
										array = df1.STD,
										visible = true,
										),
							name = "sigma=0.5"
						)
		push!(data,trace)

		trace = scatter(df2, x=:delta, y=:Mean,
							error_y = attr(
										array = df2.STD,
										visible = true,
										),
							name = "sigma=0"
						)
		push!(data,trace)
		
		plot(data)
	end

	function plot_4(Df::DataFrame)

		df = by(Df, [:delta, :sigma], Mean = :soc_wel=> mean, 
			STD = :soc_wel =>std)
		data = GenericTrace[]

		df1 = df[df.sigma.==0.5,:]
		df2 = df[df.sigma.==0,:]

		trace = scatter(df1, x=:delta, y=:Mean,
							error_y = attr(
										array = df1.STD,
										visible = true,
										),
							name = "sigma=0.5"
						)
		push!(data,trace)

		trace = scatter(df2, x=:delta, y=:Mean,
							error_y = attr(
										array = df2.STD,
										visible = true,
										),
							name = "sigma=0"
						)
		push!(data,trace)
		
		plot(data)
	end