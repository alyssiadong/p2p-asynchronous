module RPiAsync

	using CSV, DataFrames
	using PlotlyJS
	using Statistics
    using LightGraphs, SimpleWeightedGraphs
    using Dates

	struct RPiAsyncParams
		N_Ω::Int64
		Ω::Array{Int64,1}
		types::Array{String,1}
		connections::SimpleWeightedGraph{Int64}

		function RPiAsyncParams()
			N_Ω = 9
			Ω = [i for i=1:N_Ω]
			types = ["prod", "prod", "prod", 
					"cons", "cons","cons",
					"cons","cons","cons",]

			I = [
				1,1,1,1,1,1,
				2,2,2,2,2,2,
				3,3,3,3,3,3,
				]
			J = [
				4,5,6,7,8,9,
				4,5,6,7,8,9,
				4,5,6,7,8,9,
				]
			V = [						# TO DO
				0,0,0,0,0,0,
				0,0,0,0,0,0,
				0,0,0,0,0,0,
				]

			connections = SimpleWeightedGraph(I, J, V)
			new(N_Ω, Ω, types, connections)
		end
	end
	params = RPiAsyncParams()

	##############################################################
	# Creating a dataframe from a chronograf csv file
	##############################################################

	function create_dataframe(filepath::String)
		data = CSV.read(filepath)
		data = data[completecases(data),:]

		if Symbol("mqtt_consumer.timestamp_send") in names(data)
			select!(data, Not(Symbol("mqtt_consumer.timestamp_send")))
		end

		rename!(data, Dict(
						"mqtt_consumer.dual_res_ij" => "dual_res_ij",
						"mqtt_consumer.primal_res_ij" => "primal_res_ij",
						"mqtt_consumer.from" => "from_i",
						"mqtt_consumer.to" => "to_j",
						"mqtt_consumer.ki" => "k_i",
						"mqtt_consumer.k_ij" => "k_ij",
						"mqtt_consumer.price_ij" => "price_ij",
						# "mqtt_consumer.timestamp_send" => "timestamp_send",
						"mqtt_consumer.trade_ij" => "trade_ij",
						"mqtt_consumer.dt_comm" => "dt_comm",
						"mqtt_consumer.dt_stall" => "dt_stall",
						"mqtt_consumer.dt_comp" => "dt_comp",
						"mqtt_consumer.dt_iteration" => "dt_iteration",
						))
		data.datetime = DateTime.([data.date[i][1:end-3] for i in 1:size(data,1)], DateFormat("m/d/y H:M:S.s p"))
		sort!(data,:datetime)
		data
	end

	function create_dataframe_monitor(filepath::String)
		data = CSV.read(filepath)
		data = data[completecases(data),:]

		rename!(data, Dict(
						"mqtt_consumer.i" => "i",
						"mqtt_consumer.k_i" => "k_i",
						"mqtt_consumer.local_cost" => "local_cost",
						"mqtt_consumer.local_dual_res" => "local_dual_res",
						"mqtt_consumer.local_primal_res" => "local_primal_res",
						"mqtt_consumer.local_power" => "local_power",
						"mqtt_consumer.local_price" => "local_price",
						))
		# data.datetime = DateTime.(data.time, DateFormat("y-m-dTH:M:S.sZ"))
		data.datetime = DateTime.([data.date[i][1:end-3] for i in 1:size(data,1)], DateFormat("m/d/y H:M:S.s p"))
		sort!(data,:datetime)
		data
	end

	##############################################################
	# Analyzing delays from a chronograf csv file or a 
	# corresponding dataframe
	##############################################################
	
	function analyze_delays(filepath::String)
		Df = create_dataframe(filepath)
		# fix_comm_delays!(Df)
		analyze_delays(Df)
	end

	function analyze_delays(Df::DataFrame)

		types = params.types
		Df.from_type = map(x->types[x+1], Df.from_i)

		data = GenericTrace[]

		# Computation delays
		trace = histogram(
							x = Df[Df.from_type .== "cons", :dt_comp],
							opacity = 0.75,
							name = "From consumer",
							yaxis = "y",
							nbinsx = 30,
			)
		push!(data, trace)
		trace = histogram(
							x = Df[Df.from_type .== "prod", :dt_comp],
							opacity = 0.75,
							name = "From producer",
							yaxis = "y",
							nbinsx = 30,
			)
		push!(data, trace)

		# Stall Delays
		trace = histogram(
							x = Df[(Df.from_type .== "cons") .& (Df.dt_stall .< 0.2), :dt_stall],
							opacity = 0.75,
							name = "From consumer",
							yaxis = "y2",
							nbinsx = 30,
			)
		push!(data, trace)
		trace = histogram(
							x = Df[(Df.from_type .== "prod") .& (Df.dt_stall .< 0.2), :dt_stall],
							opacity = 0.75,
							name = "From producer",
							yaxis = "y2",
							nbinsx = 30,
			)
		push!(data, trace)

		# Commmunication Delays
		trace = histogram(
							x = Df[Df.from_type .== "cons", :dt_comm],
							opacity = 0.75,
							name = "From consumer",
							yaxis = "y3",
							nbinsx = 30,
			)
		push!(data, trace)
		trace = histogram(
							x = Df[Df.from_type .== "prod", :dt_comm],
							opacity = 0.75,
							name = "From producer",
							yaxis = "y3",
							nbinsx = 30,
			)
		push!(data, trace)


		layout = Layout(
						barmode="overlay",
						yaxis = attr(
										domain = [0,0.3],
							),
						yaxis2 = attr(
										domain = [0.32, 0.6],
							),
						yaxis3 = attr(
										domain = [0.62, 1],
							),
			)
		plot(data, layout)
	end

	function plot_delay_histo(alpha::Int64)
		@assert alpha ∈ [0, 2, 4] "Incorrect alpha value"

		testcases = Dict(
				"delta1.0" => string("chronograf_results/alpha", alpha," - delta1 - messages.csv"),
				"delta0.8" => string("chronograf_results/alpha", alpha," - delta0_8 - messages.csv"),
				"delta0.6" => string("chronograf_results/alpha", alpha," - delta0_6 - messages.csv"),
				"delta0.4" => string("chronograf_results/alpha", alpha," - delta0_4 - messages.csv"),
				"delta0.1" => string("chronograf_results/alpha", alpha," - delta0_1 - messages.csv"),
			)
		DF = DataFrame()
		for (name,filepath) in testcases
			Df = RPiAsync.create_dataframe(filepath)
			DF = vcat(DF, Df)
		end
		trace = histogram(
							x = DF.dt_comm,
							opacity = 0.75,
							name = "prod",
							yaxis = "y",
							histnorm = "probability",
							# nbinsx = 100,
							xbins = Dict(
											:start => 0,
											:end => 3,
											:size => 0.05
								),
			)
		layout = Layout(
							height = 100,
							width = 328,
							margin = attr(
										l = 35,
										r = 10,
										t = 10,
										b = 25,
										),			
							xaxis = attr(
								range = [0,3],
								title = attr(
										text = "Délai de communication (s)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								# zeroline=true,
								ticks = "inside",
								dtick = 1,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								range = [0,3],
								# title = attr(
								# 	text = "Délai de communication (s)",
								# 	font_size = 10,
								# 	font_color = "#444444",
								# 	font_family = "PT Sans Narrow",
								# ), 
							tickfont_size = 10,
							tickfont_family = "PT Sans Narrow",
							zeroline=false,
							ticks = "inside",
							dtick = 0.1,
							showline = true,
							linecolor= "black",
							linewidth= 0.5,
							mirror= "allticks",
										),
			)
		plot(trace, layout)
		# DF
	end

	function plot_delay_histo(alpha::Int64, i::Int64, j::Int64)
		@assert alpha ∈ [0, 2, 4] "Incorrect alpha value"
		@assert i ∈ 0:8 "Incorrect i value"
		@assert j ∈ 0:8 "Incorrect j value"

		testcases = Dict(
				"delta1.0" => string("chronograf_results/alpha", alpha," - delta1 - messages.csv"),
				"delta0.8" => string("chronograf_results/alpha", alpha," - delta0_8 - messages.csv"),
				"delta0.6" => string("chronograf_results/alpha", alpha," - delta0_6 - messages.csv"),
				"delta0.4" => string("chronograf_results/alpha", alpha," - delta0_4 - messages.csv"),
				"delta0.1" => string("chronograf_results/alpha", alpha," - delta0_1 - messages.csv"),
			)
		DF = DataFrame()
		for (name,filepath) in testcases
			Df = RPiAsync.create_dataframe(filepath)
			DF = vcat(DF, Df)
		end
		DF2 = DF[((DF.from_i .== i) .& (DF.to_j .== j)) .| ((DF.from_i .== j) .& (DF.to_j .== i)), :]
		trace = histogram(
							x = DF2.dt_comm,
							opacity = 0.75,
							name = "prod",
							yaxis = "y",
							histnorm = "probability",
							# nbinsx = 100,
							xbins = Dict(
											:start => 0,
											:end => 3,
											:size => 0.05
								),
			)
		layout = Layout(
							height = 100,
							width = 328,
							margin = attr(
										l = 35,
										r = 10,
										t = 10,
										b = 25,
										),			
							xaxis = attr(
								range = [0,3],
								title = attr(
										text = "Délai de communication (s)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								# zeroline=true,
								ticks = "inside",
								dtick = 1,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								range = [0,3],
								# title = attr(
								# 	text = "Délai de communication (s)",
								# 	font_size = 10,
								# 	font_color = "#444444",
								# 	font_family = "PT Sans Narrow",
								# ), 
							tickfont_size = 10,
							tickfont_family = "PT Sans Narrow",
							zeroline=false,
							ticks = "inside",
							dtick = 0.1,
							showline = true,
							linecolor= "black",
							linewidth= 0.5,
							mirror= "allticks",
										),
			)
		plot(trace, layout)
		# DF
	end

	function plot_delay_histo()

		alphas = Dict(
				0 => Dict("alpha_txt" => "0_0","name" =>"alpha = 0.0" , "y_dtick" => 0.05,"yaxis" => "y", "testcases"=> Dict()) ,
				0.5 => Dict("alpha_txt" => "0_5","name" =>"alpha = 0.5" , "y_dtick" => 0.02,"yaxis" => "y2", "testcases"=> Dict()) ,
				1 => Dict("alpha_txt" => "1_0","name" =>"alpha = 1.0" , "y_dtick" => 0.02,"yaxis" => "y3", "testcases"=> Dict()) ,
			)
		trace = GenericTrace[]
		for (alpha, dic_alpha) in sort(alphas)
			if alpha ∈[0,1]
				alpha_txt = string(Int(alpha), "_0")
			else
				alpha_txt = "0_5"
			end
			testcases = Dict(
					# "delta1.0" => string("chronograf_results/alpha", alpha," - delta1 - messages.csv"),
					"delta1.0" => string("chronograf_results/cccd - alpha", alpha_txt," - delta1_0 - messages.csv"),
					"delta0.8" => string("chronograf_results/cccd - alpha", alpha_txt," - delta0_8 - messages.csv"),
					"delta0.6" => string("chronograf_results/cccd - alpha", alpha_txt," - delta0_6 - messages.csv"),
					"delta0.4" => string("chronograf_results/cccd - alpha", alpha_txt," - delta0_4 - messages.csv"),
					"delta0.1" => string("chronograf_results/cccd - alpha", alpha_txt," - delta0_1 - messages.csv"),
				)
			DF = DataFrame()
			dic_alpha["testcases"] = testcases
			for (name,filepath) in testcases
				Df = RPiAsync.create_dataframe(filepath)
				Df.alpha = fill(alpha, size(Df,1))
				DF = vcat(DF, Df)
			end

			data = histogram(
						x = DF.dt_comm,
						opacity = 0.75,
						name = dic_alpha["name"],
						yaxis = dic_alpha["yaxis"],
						histnorm = "probability",
						# nbinsx = 100,
						xbins = Dict(
										:start => 0,
										:end => 3,
										:size => 0.02
							),
			)
			push!(trace, data)
		end

		# DF
		layout = Layout(
							height = 250,
							width = 328,
							margin = attr(
										l = 20,
										r = 10,
										t = 10,
										b = 25,
										),			
							showlegend = true,
							legend = attr(
										x = 0.65,
										y = 0.99,
										font_size = 10,
										font_family = "PT Sans Narrow",
										),
							xaxis = attr(
								range = [0,1.1],
								title = attr(
										text = "Délai de communication (s)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								anchor = "y3",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								# zeroline=true,
								ticks = "inside",
								dtick = 0.5,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								# range = [0,3],
								# title = attr(
								# 	text = "Délai de communication (s)",
								# 	font_size = 10,
								# 	font_color = "#444444",
								# 	font_family = "PT Sans Narrow",
								# ), 
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=false,
								ticks = "inside",
								dtick = alphas[0]["y_dtick"],
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								domain = [0.66, 1],
										),
							yaxis2 = attr(
								# range = [0,3],
								# title = attr(
								# 	text = "Délai de communication (s)",
								# 	font_size = 10,
								# 	font_color = "#444444",
								# 	font_family = "PT Sans Narrow",
								# ), 
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=false,
								ticks = "inside",
								dtick = alphas[0.5]["y_dtick"],
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								domain = [0.33, 0.63],
										),
							yaxis3 = attr(
								# range = [0,3],
								# title = attr(
								# 	text = "Délai de communication (s)",
								# 	font_size = 10,
								# 	font_color = "#444444",
								# 	font_family = "PT Sans Narrow",
								# ), 
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=false,
								ticks = "inside",
								dtick = alphas[1]["y_dtick"],
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								domain = [0, 0.30],
										),
			)
		plot(trace, layout)
		# DF
	end

	##############################################################
	# Fixes the estimated communication delays by arranging the clock
	##############################################################

	function fix_comm_delays!(Df::DataFrame)
		consumers = [i for i in params.Ω if params.types[i] == "cons"]
		producers = [i for i in params.Ω if params.types[i] == "prod"]

		Df.dt_comm_corr = fill(0.0, size(Df,1))
		for i1 in consumers
			i = i1 - 1
			for j1 in producers
				j = j1 - 1
				ida = (Df.from_i .== i) .& (Df.to_j .== j)
				idb = (Df.from_i .== j) .& (Df.to_j .== i)
				da = Df[ida, :]
				db = Df[idb, :]
				ma = median(da.dt_comm)
				mb = median(db.dt_comm)
				dt = (mb-ma)/2

				Df[ida,:dt_comm_corr] = Df[ida,:dt_comm] .+ dt
				Df[idb,:dt_comm_corr] = Df[idb,:dt_comm] .- dt
			end
		end
	end

	##############################################################
	# Constant communication delay test
	##############################################################

	function test01()
		testcases = Dict(
				"delta1.0" => "chronograf_results/cccd - alpha1_0 - delta1_0 - monitor.csv",
				"delta0.8" => "chronograf_results/cccd - alpha1_0 - delta0_8 - monitor.csv",
				"delta0.6" => "chronograf_results/cccd - alpha1_0 - delta0_6 - monitor.csv",
				"delta0.4" => "chronograf_results/cccd - alpha1_0 - delta0_4 - monitor.csv",
				"delta0.1" => "chronograf_results/cccd - alpha1_0 - delta0_1 - monitor.csv",
			)

		DF = DataFrame()

		trace = GenericTrace[]
		for (name,filepath) in testcases
			Df = RPiAsync.create_dataframe_monitor(filepath)
			gdf = groupby(Df, :i, sort = true)

			Df.testcase = fill(name, size(Df,1))

			Df.global_primal_res = zeros(size(Df,1))
			Df.global_dual_res = zeros(size(Df,1))
			for id in 1:size(Df,1)
				t0 = Df.datetime[id]
				# Find pointers for each subgroup
				pointers = [findlast(x->x==true, gdf[idf].datetime .<= t0) for idf in 1:length(gdf)]
				if all(pointers .!= nothing)
					# Compute global primal residual
					Df.global_primal_res[id] = sum([gdf[idf].local_primal_res[pointers[idf]] for idf in 1:length(gdf)])
					Df.global_dual_res[id] = sum([gdf[idf].local_dual_res[pointers[idf]] for idf in 1:length(gdf)])
				end
			end
			Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[1])/1000 # time in seconds

			DF = vcat(DF, Df)
			Df1 = Df[log10.(Df.global_primal_res) .> -20,:]
			data = scatter(;x=Df1.time_sec, y=log10.(Df1.global_primal_res), name = name)
			push!(trace, data)
		end
		plot(trace)

		# DF
	end

	function test02()
		testcases = Dict(
				"delta1.0" => "chronograf_results/cccd - alpha0_0 - delta1_0 - monitor.csv",
				"delta0.8" => "chronograf_results/cccd - alpha0_0 - delta0_8 - monitor.csv",
				"delta0.6" => "chronograf_results/cccd - alpha0_0 - delta0_6 - monitor.csv",
				"delta0.4" => "chronograf_results/cccd - alpha0_0 - delta0_4 - monitor.csv",
				"delta0.1" => "chronograf_results/cccd - alpha0_0 - delta0_1 - monitor.csv",
			)

		DF = DataFrame()

		trace = GenericTrace[]
		for (name,filepath) in testcases
			Df = RPiAsync.create_dataframe_monitor(filepath)
			gdf = groupby(Df, :i, sort = true)

			Df.testcase = fill(name, size(Df,1))

			Df.global_primal_res = zeros(size(Df,1))
			Df.global_dual_res = zeros(size(Df,1))
			for id in 1:size(Df,1)
				t0 = Df.datetime[id]
				# Find pointers for each subgroup
				pointers = [findlast(x->x==true, gdf[idf].datetime .<= t0) for idf in 1:length(gdf)]
				if all(pointers .!= nothing)
					# Compute global primal residual
					Df.global_primal_res[id] = sum([gdf[idf].local_primal_res[pointers[idf]] for idf in 1:length(gdf)])
					Df.global_dual_res[id] = sum([gdf[idf].local_dual_res[pointers[idf]] for idf in 1:length(gdf)])
				end
			end
			Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[1])/1000 # time in seconds

			DF = vcat(DF, Df)
			Df1 = Df[log10.(Df.global_primal_res) .> -20,:]

			data = scatter(;x=Df1.time_sec, y=log10.(Df1.global_primal_res), name = name)
			push!(trace, data)
		end
		plot(trace)

		# DF
	end

	function test03()
		testcases = Dict(
				# "delta1.0" => "chronograf_results/cccd - alpha4 - delta1 - monitor.csv",
				"delta1.0" => "chronograf_results/cccd - alpha0_5 - delta1_0 - monitor.csv",
				"delta0.8" => "chronograf_results/cccd - alpha0_5 - delta0_8 - monitor.csv",
				"delta0.6" => "chronograf_results/cccd - alpha0_5 - delta0_6 - monitor.csv",
				"delta0.4" => "chronograf_results/cccd - alpha0_5 - delta0_4 - monitor.csv",
				"delta0.1" => "chronograf_results/cccd - alpha0_5 - delta0_1 - monitor.csv",
			)

		DF = DataFrame()

		trace = GenericTrace[]
		for (name,filepath) in testcases
			Df = RPiAsync.create_dataframe_monitor(filepath)
			gdf = groupby(Df, :i, sort = true)

			Df.testcase = fill(name, size(Df,1))

			Df.global_primal_res = zeros(size(Df,1))
			Df.global_dual_res = zeros(size(Df,1))
			for id in 1:size(Df,1)
				t0 = Df.datetime[id]
				# Find pointers for each subgroup
				pointers = [findlast(x->x==true, gdf[idf].datetime .<= t0) for idf in 1:length(gdf)]
				if all(pointers .!= nothing)
					# Compute global primal residual
					Df.global_primal_res[id] = sum([gdf[idf].local_primal_res[pointers[idf]] for idf in 1:length(gdf)])
					Df.global_dual_res[id] = sum([gdf[idf].local_dual_res[pointers[idf]] for idf in 1:length(gdf)])
				end
			end
			Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[1])/1000 # time in seconds

			DF = vcat(DF, Df)
			Df1 = Df[log10.(Df.global_primal_res) .> -20,:]
			data = scatter(;x=Df1.time_sec, y=log10.(Df1.global_primal_res), name = name)
			push!(trace, data)
		end
		plot(trace)

		# DF
	end

	##############################################################
	# Dataframes and plot functions
	##############################################################

	function DF_primal_res(txt::String)
		alphas = Dict(
				0 => Dict( "alpha_txt" => "0_0","name" =>"alpha = 0" , "marker" => "circle","yaxis" => "y", "testcases"=> Dict()) ,
				0.5 => Dict( "alpha_txt" => "0_5","name" =>"alpha = 2" , "marker" => "diamond","yaxis" => "y2", "testcases"=> Dict()) ,
				1 => Dict( "alpha_txt" => "1_0","name" =>"alpha = 4" , "marker" => "star-diamond","yaxis" => "y3", "testcases"=> Dict()) ,
			)

		DF = DataFrame()
		for (alpha, dic_alpha) in sort(alphas)

			testcases = Dict(
					1.0 => Dict("name"=> "delta = 1.0", "marker" => "circle","color" => "#636EFA","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta1_0",txt," - monitor.csv")),
					0.8 => Dict("name"=> "delta = 0.8", "marker" => "diamond","color" => "#EF553B","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_8",txt," - monitor.csv")),
					0.6 => Dict("name"=> "delta = 0.6", "marker" => "star-diamond","color" => "#00CC96","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_6",txt," - monitor.csv")),
					0.4 => Dict("name"=> "delta = 0.4", "marker" => "triangle-up","color" => "#AB63FA","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_4",txt," - monitor.csv")),
					# 0.2 => Dict("name"=> "delta = 0.2", "marker" => "triangle-down","color" => "#19D3F3","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_2",txt," - monitor.csv")),
					0.1 => Dict("name"=> "delta = 0.1", "marker" => "triangle-down","color" => "#FFA15A","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_1",txt," - monitor.csv")),
				)
			dic_alpha["testcases"] = testcases

			for (delta,delta_dic) in testcases
				name = delta_dic["name"]
				filepath = delta_dic["filepath"]
				if isfile(filepath)
					Df = RPiAsync.create_dataframe_monitor(filepath)
					Df.alpha = fill(alpha, size(Df,1))
					gdf = groupby(Df, :i, sort = true)

					Df.testcase = fill(name, size(Df,1))
					Df.delta = fill(delta, size(Df,1))
					Df.psi = fill(0, size(Df,1))
					Df.marker = fill(delta_dic["marker"], size(Df,1))
					Df.color = fill(delta_dic["color"], size(Df,1))

					Df.global_primal_res = zeros(size(Df,1))
					Df.global_dual_res = zeros(size(Df,1))
					last_id = 1
					for id in 1:size(Df,1)
						t0 = Df.datetime[id]
						# Find pointers for each subgroup
						pointers = [findlast(x->x==true, gdf[idf].datetime .<= t0) for idf in 1:length(gdf)]
						if all(pointers .!= nothing)
							# Compute global primal residual
							Df.global_primal_res[id] = sum([gdf[idf].local_primal_res[pointers[idf]] for idf in 1:length(gdf)])
							Df.global_dual_res[id] = sum([gdf[idf].local_dual_res[pointers[idf]] for idf in 1:length(gdf)])
						else
							last_id = id
						end
					end
					Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[last_id+1])/1000 # time in seconds

					Df1 = Df[log10.(Df.global_primal_res) .> -20,:]
					DF = vcat(DF, Df1)
				end
			end
		end
		DF
	end

	function DF_primal_res_loss()
		alphas = Dict(
				0 => Dict( "alpha_txt" => "0_0","name" =>"alpha = 0" , "color" => "#855c75","marker" => "circle","yaxis" => "y", "testcases"=> Dict()) ,
				0.5 => Dict( "alpha_txt" => "0_5","name" =>"alpha = 2" , "color" => "#d9af6b","marker" => "diamond","yaxis" => "y2", "testcases"=> Dict()) ,
				1 => Dict( "alpha_txt" => "1_0","name" =>"alpha = 4" , "color" => "#af6458","marker" => "x","yaxis" => "y3", "testcases"=> Dict()) ,
			)
		ps = Dict(
				0 => Dict(	"p"=>0, "p_txt" => ""),
				0.01 => Dict(	"p"=>0.01, "p_txt" => " - p0_01"),
				0.02 => Dict(	"p"=>0.02, "p_txt" => " - p0_02"),
				0.03 => Dict(	"p"=>0.03, "p_txt" => " - p0_03"),
				0.04 => Dict(	"p"=>0.04, "p_txt" => " - p0_04"),
				0.05 => Dict(	"p"=>0.05, "p_txt" => " - p0_05"),
				0.06 => Dict(	"p"=>0.06, "p_txt" => " - p0_06"),
				0.07 => Dict(	"p"=>0.07, "p_txt" => " - p0_07"),
				0.08 => Dict(	"p"=>0.08, "p_txt" => " - p0_08"),
				0.09 => Dict(	"p"=>0.09, "p_txt" => " - p0_09"),
				0.10 => Dict(	"p"=>0.10, "p_txt" => " - p0_10"),
				0.11 => Dict(	"p"=>0.11, "p_txt" => " - p0_11"),
				0.12 => Dict(	"p"=>0.12, "p_txt" => " - p0_12"),
				0.13 => Dict(	"p"=>0.13, "p_txt" => " - p0_13"),
				0.14 => Dict(	"p"=>0.14, "p_txt" => " - p0_14"),
				0.15 => Dict(	"p"=>0.15, "p_txt" => " - p0_15"),
				0.16 => Dict(	"p"=>0.16, "p_txt" => " - p0_16"),
				0.17 => Dict(	"p"=>0.17, "p_txt" => " - p0_17"),
				0.18 => Dict(	"p"=>0.18, "p_txt" => " - p0_18"),
				0.19 => Dict(	"p"=>0.19, "p_txt" => " - p0_19"),
				0.20 => Dict(	"p"=>0.20, "p_txt" => " - p0_20"),
				0.50 => Dict(	"p"=>0.50, "p_txt" => " - p0_50"),
			)

		DF = DataFrame()
		for (alpha, dic_alpha) in sort(alphas)
			for (p, dic_p) in sort(ps)
				testcases = Dict(
						1.0 => Dict("name"=> "delta = 1.0", "marker" => "circle","color" => "#636EFA","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta1_0",dic_p["p_txt"]," - monitor.csv")),
						# 0.8 => Dict("name"=> "delta = 0.8", "marker" => "diamond","color" => "#EF553B","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_8",dic_p["p_txt"]," - monitor.csv")),
						# 0.6 => Dict("name"=> "delta = 0.6", "marker" => "star-diamond","color" => "#00CC96","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_6",dic_p["p_txt"]," - monitor.csv")),
						# 0.4 => Dict("name"=> "delta = 0.4", "marker" => "triangle-up","color" => "#AB63FA","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_4",dic_p["p_txt"]," - monitor.csv")),
						0.1 => Dict("name"=> "delta = 0.1", "marker" => "triangle-down","color" => "#FFA15A","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_1",dic_p["p_txt"]," - monitor.csv")),
					)
				dic_alpha["testcases"] = testcases

				for (delta,delta_dic) in testcases
					name = delta_dic["name"]
					filepath = delta_dic["filepath"]

					if isfile(filepath)
						Df = RPiAsync.create_dataframe_monitor(filepath)
						Df.alpha = fill(alpha, size(Df,1))
						Df.p = fill(p, size(Df,1))
						gdf = groupby(Df, :i, sort = true)

						Df.testcase = fill(name, size(Df,1))
						Df.delta = fill(delta, size(Df,1))
						Df.marker = fill(delta_dic["marker"], size(Df,1))
						Df.color = fill(delta_dic["color"], size(Df,1))
						# Df.marker = fill(dic_alpha["marker"],size(Df,1))
						# Df.color = fill(dic_alpha["color"],size(Df,1))

						Df.global_primal_res = zeros(size(Df,1))
						Df.global_dual_res = zeros(size(Df,1))
						last_id = 1
						for id in 1:size(Df,1)
							t0 = Df.datetime[id]
							# Find pointers for each subgroup
							pointers = [findlast(x->x==true, gdf[idf].datetime .<= t0) for idf in 1:length(gdf)]
							if all(pointers .!= nothing)
								# Compute global primal residual
								Df.global_primal_res[id] = sum([gdf[idf].local_primal_res[pointers[idf]] for idf in 1:length(gdf)])
								Df.global_dual_res[id] = sum([gdf[idf].local_dual_res[pointers[idf]] for idf in 1:length(gdf)])
							else
								last_id = id
							end
						end
						Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[last_id+1])/1000 # time in seconds

						Df1 = Df[log10.(Df.global_primal_res) .> -20,:]
						DF = vcat(DF, Df1)
					end
				end
			end
		end
		DF
	end

	function DF_primal_res_pause()
		alphas = Dict(
				0 => Dict( "alpha_txt" => "0_0","name" =>"alpha = 0" , "color" => "#855c75","marker" => "circle","yaxis" => "y", "testcases"=> Dict()) ,
				# 0.5 => Dict( "alpha_txt" => "0_5","name" =>"alpha = 2" , "color" => "#d9af6b","marker" => "diamond","yaxis" => "y2", "testcases"=> Dict()) ,
				# 1 => Dict( "alpha_txt" => "1_0","name" =>"alpha = 4" , "color" => "#af6458","marker" => "x","yaxis" => "y3", "testcases"=> Dict()) ,
			)

		DF = DataFrame()
		for (alpha, dic_alpha) in sort(alphas)
			testcases = Dict(
					1.0 => Dict("name"=> "delta = 1.0", "marker" => "circle","color" => "#636EFA","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta1_0 - pause6"," - monitor.csv")),
					0.8 => Dict("name"=> "delta = 0.8", "marker" => "diamond","color" => "#EF553B","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_8 - pause6"," - monitor.csv")),
					0.6 => Dict("name"=> "delta = 0.6", "marker" => "star-diamond","color" => "#00CC96","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_6 - pause6"," - monitor.csv")),
					0.4 => Dict("name"=> "delta = 0.4", "marker" => "triangle-up","color" => "#AB63FA","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_4 - pause6"," - monitor.csv")),
					0.1 => Dict("name"=> "delta = 0.1", "marker" => "triangle-down","color" => "#FFA15A","filepath" => string("chronograf_results/cccd - alpha", dic_alpha["alpha_txt"]," - delta0_1 - pause6"," - monitor.csv")),
				)
			dic_alpha["testcases"] = testcases

			for (delta,delta_dic) in testcases
				name = delta_dic["name"]
				filepath = delta_dic["filepath"]

				if isfile(filepath)
					Df = RPiAsync.create_dataframe_monitor(filepath)
					Df.alpha = fill(alpha, size(Df,1))
					gdf = groupby(Df, :i, sort = true)

					Df.testcase = fill(name, size(Df,1))
					Df.delta = fill(delta, size(Df,1))
					Df.marker = fill(delta_dic["marker"], size(Df,1))
					Df.color = fill(delta_dic["color"], size(Df,1))
					# Df.marker = fill(dic_alpha["marker"],size(Df,1))
					# Df.color = fill(dic_alpha["color"],size(Df,1))

					Df.global_primal_res = zeros(size(Df,1))
					Df.global_dual_res = zeros(size(Df,1))
					last_id = 1
					for id in 1:size(Df,1)
						t0 = Df.datetime[id]
						# Find pointers for each subgroup
						pointers = [findlast(x->x==true, gdf[idf].datetime .<= t0) for idf in 1:length(gdf)]
						if all(pointers .!= nothing)
							# Compute global primal residual
							Df.global_primal_res[id] = sum([gdf[idf].local_primal_res[pointers[idf]] for idf in 1:length(gdf)])
							Df.global_dual_res[id] = sum([gdf[idf].local_dual_res[pointers[idf]] for idf in 1:length(gdf)])
						else
							last_id = id
						end
					end
					Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[last_id+1])/1000 # time in seconds

					Df1 = Df[log10.(Df.global_primal_res) .> -20,:]
					DF = vcat(DF, Df1)
				end
			end
		end
		DF
	end

	function DF_messages()
		alphas = Dict(
				0 => Dict( "txt" => "- alpha0_0 ","name" =>"alpha = 0" , "marker" => "circle","yaxis" => "y", "testcases"=> Dict()) ,
				0.5 => Dict( "txt" => "- alpha0_5 ","name" =>"alpha = 0.5" , "marker" => "diamond","yaxis" => "y2", "testcases"=> Dict()) ,
				1 => Dict( "txt" => "- alpha1_0 ","name" =>"alpha = 1" , "marker" => "star-diamond","yaxis" => "y3", "testcases"=> Dict()) ,
			)

		deltas = Dict(
					1.0 => Dict("name"=> "delta = 1.0", "marker" => "circle","color" => "#636EFA",  		"txt" =>"- delta1_0 "),
					0.8 => Dict("name"=> "delta = 0.8", "marker" => "diamond","color" => "#EF553B", 		"txt" =>"- delta0_8 "),
					0.6 => Dict("name"=> "delta = 0.6", "marker" => "star-diamond","color" => "#00CC96", 	"txt" =>"- delta0_6 "),
					0.4 => Dict("name"=> "delta = 0.4", "marker" => "triangle-up","color" => "#AB63FA", 	"txt" =>"- delta0_4 "),
					# 0.2 => Dict("name"=> "delta = 0.2", "marker" => "triangle-down","color" => "#19D3F3","txt" =>"- delta0_2 " ),
					0.1 => Dict("name"=> "delta = 0.1", "marker" => "triangle-down","color" => "#FFA15A", 	"txt" =>"- delta0_1 "),
				)

		psis = Dict(
				0 => Dict(	"p"=>0, "txt" => "", "name" => "psi = 0"),
				# 0.01 => Dict(	"p"=>0.01, "txt" => " - p0_01 ", "name"=> "psi = 0.01"),
				# 0.02 => Dict(	"p"=>0.02, "txt" => " - p0_02 ", "name"=> "psi = 0.02"),
				# 0.03 => Dict(	"p"=>0.03, "txt" => " - p0_03 ", "name"=> "psi = 0.03"),
				# 0.04 => Dict(	"p"=>0.04, "txt" => " - p0_04 ", "name"=> "psi = 0.04"),
				# 0.05 => Dict(	"p"=>0.05, "txt" => " - p0_05 ", "name"=> "psi = 0.05"),
				# 0.06 => Dict(	"p"=>0.06, "txt" => " - p0_06 ", "name"=> "psi = 0.06"),
				# 0.07 => Dict(	"p"=>0.07, "txt" => " - p0_07 ", "name"=> "psi = 0.07"),
				# 0.08 => Dict(	"p"=>0.08, "txt" => " - p0_08 ", "name"=> "psi = 0.08"),
				# 0.09 => Dict(	"p"=>0.09, "txt" => " - p0_09 ", "name"=> "psi = 0.09"),
				# 0.10 => Dict(	"p"=>0.10, "txt" => " - p0_10 ", "name"=> "psi = 0.10"),
				# 0.11 => Dict(	"p"=>0.11, "txt" => " - p0_11 ", "name"=> "psi = 0.11"),
				# 0.12 => Dict(	"p"=>0.12, "txt" => " - p0_12 ", "name"=> "psi = 0.12"),
				# 0.13 => Dict(	"p"=>0.13, "txt" => " - p0_13 ", "name"=> "psi = 0.13"),
				# 0.14 => Dict(	"p"=>0.14, "txt" => " - p0_14 ", "name"=> "psi = 0.14"),
				# 0.15 => Dict(	"p"=>0.15, "txt" => " - p0_15 ", "name"=> "psi = 0.15"),
				# 0.16 => Dict(	"p"=>0.16, "txt" => " - p0_16 ", "name"=> "psi = 0.16"),
				# 0.17 => Dict(	"p"=>0.17, "txt" => " - p0_17 ", "name"=> "psi = 0.17"),
				# 0.18 => Dict(	"p"=>0.18, "txt" => " - p0_18 ", "name"=> "psi = 0.18"),
				# 0.19 => Dict(	"p"=>0.19, "txt" => " - p0_19 ", "name"=> "psi = 0.19"),
				# 0.20 => Dict(	"p"=>0.20, "txt" => " - p0_20 ", "name"=> "psi = 0.20"),
				# 0.50 => Dict(	"p"=>0.50, "txt" => " - p0_50 ", "name"=> "psi = 0.50"),
			)


		DF = DataFrame()
		for (alpha, alpha_dic) in sort(alphas)
			for (delta,delta_dic) in deltas
				for (psi, psi_dic) in psis
					filepath = string("chronograf_results/cccd ", alpha_dic["txt"],delta_dic["txt"],psi_dic["txt"],"- messages.csv")
					if isfile(filepath)
						Df = RPiAsync.create_dataframe(filepath)
						Df.alpha = fill(alpha, size(Df,1))
						Df.delta = fill(delta, size(Df,1))
						Df.psi = fill(psi, size(Df,1))
						gdf = groupby(Df, [:from_i, :to_j], sort = true)

						# name = string(alpha_dic["name"],", ", delta_dic["name"],", ",psi_dic["name"])
						name = delta_dic["name"]
						Df.testcase = fill(name, size(Df,1))
						Df.delta = fill(delta, size(Df,1))
						Df.marker = fill(delta_dic["marker"], size(Df,1))
						Df.color = fill(delta_dic["color"], size(Df,1))

						last_id = 1
						Df.messages = fill(0,size(Df,1))
						for id in 1:size(Df,1)
							t0 = Df.datetime[id]
							# Find pointers for each subgroup
							from_i = Df.from_i[id]
							# idfs = [idf for idf in 1:length(gdf) if gdf[idf].from_i[1] == from_i]
							idfs = [idf for idf in 1:length(gdf)]
							pointers = [(idf in idfs) ? findlast(x->x==true, gdf[idf].datetime .<= t0) : 0 for idf in 1:length(gdf) ]
							if all(pointers .!= nothing)
								Df.messages[id] = sum([gdf[idf].k_ij[pointers[idf]] for idf in idfs])
							else
								last_id = id
							end
						end

						Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[last_id+1])/1000 # time in seconds
						DF = vcat(DF, Df)
					end
				end
			end
		end
		DF
	end

	function plot_primal_res(DF::DataFrame, alpha::Int64)
		@assert alpha ∈[0,2,4] "alpha incorrect"

		Df = DF[DF.alpha .== alpha, :]
		gdf = groupby(Df, :testcase, sort = true)
		trace = GenericTrace[]
		for df in gdf
			data = scatter(;
						x=df.time_sec, 
						y=df.global_primal_res, 
						name = df.testcase[1],
						mode = "lines+markers",
						marker = attr(
								symbol = df.marker[1],
								maxdisplayed = 10,
								color = df.color[1],
								),
						)
			push!(trace, data)
		end
		layout = Layout(
							height = 150,
							width = 328,
							margin = attr(
										l = 30,
										r = 10,
										t = 10,
										b = 25,
										),			
							showlegend = true,
							legend = attr(
										x = 0.65,
										y = 0.95,
										font_size = 10,
										font_family = "PT Sans Narrow",
										),
							xaxis = attr(
								range = [0,3000],
								title = attr(
										text = "Temps (s)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								anchor = "y3",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								# zeroline=true,
								ticks = "inside",
								# dtick = 1,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								range = [-20,3],
								title = attr(
									text = "Résidu primal",
									font_size = 10,
									font_color = "#444444",
									font_family = "PT Sans Narrow",
								), 
								type = "log",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=false,
								ticks = "inside",
								dtick = 6,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								# domain = [0.66, 1],
										),
			)

		plot(trace, layout)
	end

	function plot_primal_res(DF::DataFrame, delta::Float64)
		@assert delta ∈[0.1,0.4,0.6,0.8,1.0] "delta incorrect"

		Df = DF[DF.delta .== delta, :]
		gdf = groupby(Df, :alpha, sort = true)
		trace = GenericTrace[]
		for df in gdf
			data = scatter(;
						x=df.time_sec, 
						y=df.global_primal_res, 
						name = string("alpha = ",df.alpha[1]),
						mode = "lines+markers",
						marker = attr(
								symbol = df.marker[1],
								maxdisplayed = 10,
								color = df.color[1],
								),
						)
			push!(trace, data)
		end
		layout = Layout(
							height = 280,
							width = 328,
							margin = attr(
										l = 30,
										r = 10,
										t = 10,
										b = 25,
										),			
							showlegend = true,
							legend = attr(
										x = 0.65,
										y = 0.95,
										font_size = 10,
										font_family = "PT Sans Narrow",
										),
							xaxis = attr(
								range = [0,3000],
								title = attr(
										text = "Temps (s)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								anchor = "y3",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								# zeroline=true,
								ticks = "inside",
								# dtick = 1,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								range = [-20,3],
								title = attr(
									text = "Résidu primal",
									font_size = 10,
									font_color = "#444444",
									font_family = "PT Sans Narrow",
								), 
								type = "log",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=false,
								ticks = "inside",
								dtick = 6,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								# domain = [0.66, 1],
										),
			)

		plot(trace, layout)
	end

	function plot_primal_res(DF::DataFrame)
		alphas = Dict(
				0 => Dict("name" =>"alpha = 0" , "yaxis" => "y" ) ,
				0.5 => Dict("name" =>"alpha = 0.5" , "yaxis" => "y2") ,
				1 => Dict("name" =>"alpha = 1" , "yaxis" => "y3") ,
			)

		trace = GenericTrace[]

		for (alpha, dic_alpha) in sort(alphas)
			Df = DF[DF.alpha .== alpha, :]
			gdf = groupby(Df, :testcase, sort = true)
			showlegend = (alpha ==0) ? true : false

			for df in gdf
				data = scatter(;
							x=df.time_sec ./ 0.5, 
							y=df.global_primal_res, 
							yaxis = dic_alpha["yaxis"],
							name = df.testcase[1],
							showlegend = showlegend,
							mode = "lines+markers",
							marker = attr(
									symbol = df.marker[1],
									maxdisplayed = 10,
									color = df.color[1],
									),
							)
				push!(trace, data)
			end
		end
		# yrange = [-12,3]
		yrange = [-12,3]
		xrange = [0,300]
		layout = Layout(
					height = 360,
					width = 328,
					margin = attr(
								l = 35,
								r = 10,
								t = 10,
								b = 25,
								),			
					showlegend = true,
					legend = attr(
								x = 0.65,
								y = 0.99,
								font_size = 10,
								font_family = "PT Sans Narrow",
								),
					xaxis = attr(
						# range = [0,700],
						range = xrange,
						title = attr(
								text = "Temps relatif",
								font_size = 10,
								font_color = "#444444",
								font_family = "PT Sans Narrow",
							), 
						anchor = "y3",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						# zeroline=true,
						ticks = "inside",
						dtick = 50,
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
								),
					yaxis = attr(
						range = yrange,
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.66, 1],
								),
					yaxis2 = attr(
						range = yrange,
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.33, 0.63],
								),
					yaxis3 = attr(
						range = yrange,
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0, 0.30],
								),
			    	annotations = [
							attr(
							      visible = true,
							      text = alphas[0]["name"],
							      font = attr( 
							          size = 11, 
							          # color = "black", 
							          family = "PT Sans Narrow"),
							      showarrow = false,
							      yref = "y",
							      x = sum(xrange)/2,
							      y = 1,
							  ),
							attr(
                                  visible = true,
                                  text = alphas[0.5]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y2",
                                  x = sum(xrange)/2,
                                  y = 1,
                              ),
							attr(
                                  visible = true,
                                  text = alphas[1]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y3",
                                  x = sum(xrange)/2,
                                  y = 1,
                              ),
                      ],
			)

		plot(trace, layout)
	end

	function plot_message(DF::DataFrame, DF_prim::DataFrame)
		alphas = Dict(
				0 => Dict("name" =>"alpha = 0" , "yaxis" => "y" ) ,
				0.5 => Dict("name" =>"alpha = 0.5" , "yaxis" => "y2") ,
				1 => Dict("name" =>"alpha = 1" , "yaxis" => "y3") ,
			)

		trace = GenericTrace[]

		for (alpha, dic_alpha) in sort(alphas)
			Df = DF[DF.alpha .== alpha, :]
			gdf = groupby(Df, :testcase, sort = true)
			showlegend = (alpha ==0) ? true : false

			for df in gdf
				delta = df.delta[1]
				psi = df.psi[1]
				df_prim = DF_prim[(DF_prim.alpha .== alpha).&(DF_prim.delta .== delta).&(DF_prim.psi .==psi),:]
				df_prim = df_prim[log10.(df_prim.global_primal_res) .> -12,:]
				t_end = df_prim.time_sec[end]
				df2 = df[df.time_sec .< t_end,:]
				data = scatter(;
							x=df2.time_sec ./ 0.5, 
							y=df2.messages, 
							yaxis = dic_alpha["yaxis"],
							name = df2.testcase[1],
							showlegend = showlegend,
							mode = "lines+markers",
							marker = attr(
									symbol = df2.marker[1],
									maxdisplayed = 10,
									color = df2.color[1],
									),
							)
				push!(trace, data)
			end
		end
		# yrange = [-12,3]
		yrange = [0,10000]
		xrange = [0,300]
		layout = Layout(
					height = 360,
					width = 328,
					margin = attr(
								l = 35,
								r = 10,
								t = 10,
								b = 25,
								),			
					showlegend = true,
					legend = attr(
								x = 0.67,
								y = 0.99,
								font_size = 10,
								font_family = "PT Sans Narrow",
								),
					xaxis = attr(
						range = xrange,
						title = attr(
								text = "Temps relatif",
								font_size = 10,
								font_color = "#444444",
								font_family = "PT Sans Narrow",
							), 
						anchor = "y3",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						# zeroline=true,
						ticks = "inside",
						dtick = 50,
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
								),
					yaxis = attr(
						range = yrange,
						title = attr(
							text = "Nombre de messages",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						# type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						# tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						# ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.66, 1],
								),
					yaxis2 = attr(
						range = yrange,
						title = attr(
							text = "Nombre de messages",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						# type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						# tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						# ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.33, 0.63],
								),
					yaxis3 = attr(
						range = yrange,
						title = attr(
							text = "Nombre de messages",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						# type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						# tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						# ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0, 0.30],
								),
			    	annotations = [
							attr(
							      visible = true,
							      text = alphas[0]["name"],
							      font = attr( 
							          size = 11, 
							          # color = "black", 
							          family = "PT Sans Narrow"),
							      showarrow = false,
							      yref = "y",
							      x = sum(xrange)/2,
							      y = 9000,
							  ),
							attr(
                                  visible = true,
                                  text = alphas[0.5]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y2",
                                  x = sum(xrange)/2,
                                  y = 9000,
                              ),
							attr(
                                  visible = true,
                                  text = alphas[1]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y3",
                                  x = sum(xrange)/2,
                                  y = 9000,
                              ),
                      ],
			)

		plot(trace, layout)
	end

	function plot_message2(DF::DataFrame, DF_prim::DataFrame)
		alphas = Dict(
				0 => Dict("name" =>"alpha = 0" , "yaxis" => "y" ) ,
				0.5 => Dict("name" =>"alpha = 0.5" , "yaxis" => "y2") ,
				1 => Dict("name" =>"alpha = 1" , "yaxis" => "y3") ,
			)

		deltaT_bin = 0.5

		DF.time_sec_bin = floor.(DF.time_sec ./ deltaT_bin) .* deltaT_bin
		df = by(DF, [:time_sec_bin, :alpha, :delta, :psi], messages = :messages => mean)
		DF_prim.time_sec_bin = floor.(DF_prim.time_sec ./ deltaT_bin) .* deltaT_bin
		df_prim = by(DF_prim, [:time_sec_bin, :alpha, :delta, :psi], 
					global_primal_res = :global_primal_res => mean, 
					marker = :marker => minimum ,
					color = :color => minimum
					)
		DF_join = join(df, df_prim, on=[:time_sec_bin, :alpha, :delta, :psi])
		trace = GenericTrace[]
		for (alpha, dic_alpha) in sort(alphas)
			Df = DF_join[DF_join.alpha .== alpha, :]
			gdf = groupby(Df, :delta, sort = true)
			showlegend = (alpha ==0) ? true : false

			for df in gdf
				data = scatter(;
							x=df.messages, 
							y=df.global_primal_res, 
							yaxis = dic_alpha["yaxis"],
							name = string("delta = ",df.delta[1]),
							showlegend = showlegend,
							mode = "lines+markers",
							marker = attr(
									symbol = df.marker[1],
									maxdisplayed = 10,
									color = df.color[1],
									),
							)
				push!(trace, data)
			end
		end

		yrange = [-12,3]
		xrange = [0,10000]
		layout = Layout(
					height = 360,
					width = 328,
					margin = attr(
								l = 35,
								r = 10,
								t = 10,
								b = 25,
								),			
					showlegend = true,
					legend = attr(
								x = 0.67,
								y = 0.99,
								font_size = 10,
								font_family = "PT Sans Narrow",
								),
					xaxis = attr(
						range = xrange,
						title = attr(
								text = "Nombre de messages échangés",
								font_size = 10,
								font_color = "#444444",
								font_family = "PT Sans Narrow",
							), 
						anchor = "y3",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						# zeroline=true,
						ticks = "inside",
						dtick = 2000,
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
								),
					yaxis = attr(
						range = yrange,
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.66, 1],
								),
					yaxis2 = attr(
						range = yrange,
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.33, 0.63],
								),
					yaxis3 = attr(
						range = yrange,
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0, 0.30],
								),
			    	annotations = [
							attr(
							      visible = true,
							      text = alphas[0]["name"],
							      font = attr( 
							          size = 11, 
							          # color = "black", 
							          family = "PT Sans Narrow"),
							      showarrow = false,
							      yref = "y",
							      x = sum(xrange)/2,
							      y = 1,
							  ),
							attr(
                                  visible = true,
                                  text = alphas[0.5]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y2",
                                  x = sum(xrange)/2,
                                  y = 1,
                              ),
							attr(
                                  visible = true,
                                  text = alphas[1]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y3",
                                  x = sum(xrange)/2,
                                  y = 1,
                              ),
                      ],
			)

		plot(trace, layout)
	end

	function plot_primal_res_message(DF_res::DataFrame, DF_mes::DataFrame)
		alphas = Dict(
				0 => Dict("name" =>"alpha = 0" , "yaxis" => Dict("primal_res" => "y", "messages" => "y3"), "xaxis" => Dict("primal_res" => "x", "messages" => "x") ) ,
				# 0.5 => Dict("name" =>"alpha = 0.5" , "yaxis" => "y2") ,
				1 => Dict("name" =>"alpha = 1" , "yaxis" => Dict("primal_res" => "y2", "messages" => "y4"), "xaxis" => Dict("primal_res" => "x2", "messages" => "x2")) ,
			)
		types = 

		trace = GenericTrace[]

		for (alpha, dic_alpha) in sort(alphas)
			Df_mes = DF_mes[DF_mes.alpha .== alpha, :]
			gdf = groupby(Df_mes, :testcase, sort = true)
			showlegend = (alpha ==0) ? true : false
			for df in gdf
				data = scatter(;
							x=df.time_sec ./ 0.5, 
							y=df.messages, 
							yaxis = dic_alpha["yaxis"]["messages"],
							xaxis = dic_alpha["xaxis"]["primal_res"],
							name = df.testcase[1],
							showlegend = showlegend,
							mode = "lines+markers",
							marker = attr(
									symbol = df.marker[1],
									maxdisplayed = 10,
									color = df.color[1],
									),
							)
				push!(trace, data)
			end
			Df_res = DF_res[DF_res.alpha .== alpha, :]
			gdf = groupby(Df_res, :testcase, sort = true)
			for df in gdf
				data = scatter(;
							x=df.time_sec ./ 0.5, 
							y=df.global_primal_res, 
							yaxis = dic_alpha["yaxis"]["primal_res"],
							xaxis = dic_alpha["xaxis"]["primal_res"],
							name = df.testcase[1],
							showlegend = showlegend,
							mode = "lines+markers",
							marker = attr(
									symbol = df.marker[1],
									maxdisplayed = 10,
									color = df.color[1],
									),
							)
				push!(trace, data)
			end
		end
		yrange = [-12,3]
		yrange_mes = [0,10000]
		xrange = [0,300]
		ydomains = [[0,0.48], [0.52,1]]
		xdomains = [[0,0.45], [0.55,1]]
		layout = Layout(
					height = 230,
					width = 328,
					margin = attr(
								l = 35,
								r = 10,
								t = 10,
								b = 25,
								),			
					showlegend = false,
					legend = attr(
								x = 0.65,
								y = 0.99,
								font_size = 10,
								font_family = "PT Sans Narrow",
								),
					xaxis = attr(
						# range = [0,700],
						range = xrange,
						title = attr(
								text = "Unités de temps",
								font_size = 10,
								font_color = "#444444",
								font_family = "PT Sans Narrow",
							), 
						anchor = "y3",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						# zeroline=true,
						ticks = "inside",
						dtick = 100,
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = xdomains[1],
								),
					xaxis2 = attr(
						# range = [0,700],
						range = xrange,
						title = attr(
								text = "Unités de temps",
								font_size = 10,
								font_color = "#444444",
								font_family = "PT Sans Narrow",
							), 
						anchor = "y4",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						# zeroline=true,
						ticks = "inside",
						dtick = 100,
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = xdomains[2],
								),
					yaxis = attr(
						range = yrange,
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						anchor = "x",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = ydomains[2],
								),
					yaxis2 = attr(
						range = yrange,
						# title = attr(
						# 	text = "Résidu primal",
						# 	font_size = 10,
						# 	font_color = "#444444",
						# 	font_family = "PT Sans Narrow",
						# ), 
						type = "log",
						anchor = "x2",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = ydomains[2],
								),
					yaxis3 = attr(
						range = yrange_mes,
						title = attr(
							text = "Nombre de messages",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						# type = "log",
						anchor = "x",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						# tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						# ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = ydomains[1],
								),
					yaxis4 = attr(
						range = yrange_mes,
						# title = attr(
						# 	text = "Résidu primal",
						# 	font_size = 10,
						# 	font_color = "#444444",
						# 	font_family = "PT Sans Narrow",
						# ), 
						# type = "log",
						anchor = "x2",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						# tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						# ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = ydomains[1],
								),
			    # 	annotations = [
							# attr(
							#       visible = true,
							#       text = alphas[0]["name"],
							#       font = attr( 
							#           size = 11, 
							#           # color = "black", 
							#           family = "PT Sans Narrow"),
							#       showarrow = false,
							#       yref = "y",
							#       x = sum(xrange)/2,
							#       y = 1,
							#   ),
							# # attr(
       # #                            visible = true,
       # #                            text = alphas[0.5]["name"],
       # #                            font = attr( 
       # #                                size = 11, 
       # #                                # color = "black", 
       # #                                family = "PT Sans Narrow"),
       # #                            showarrow = false,
       # #                            yref = "y2",
       # #                            x = sum(xrange)/2,
       # #                            y = 1,
       # #                        ),
							# attr(
       #                            visible = true,
       #                            text = alphas[1]["name"],
       #                            font = attr( 
       #                                size = 11, 
       #                                # color = "black", 
       #                                family = "PT Sans Narrow"),
       #                            showarrow = false,
       #                            yref = "y3",
       #                            x = sum(xrange)/2,
       #                            y = 1,
       #                        ),
       #                ],
			)

		plot(trace, layout)
	end

	function plot_primal_res_loss(DF::DataFrame)
		alphas = Dict(
				0 => Dict("name" =>"alpha = 0" , "yaxis" => "y" ) ,
				0.5 => Dict("name" =>"alpha = 0.5" , "yaxis" => "y2") ,
				1 => Dict("name" =>"alpha = 1" , "yaxis" => "y3") ,
			)
		ps = Dict(
				0 => Dict(	"p"=>0, "p_txt" => "p = 0", "line" => ""),
				0.01 => Dict(	"p"=>0.01, "p_txt" => " - p = 0.01", "line" => "dash"),
				0.05 => Dict(	"p"=>0.05, "p_txt" => " - p = 0.05", "line" => "dot"),
			)

		trace = GenericTrace[]

		for (alpha, dic_alpha) in sort(alphas)
			for (p, dic_p) in sort(ps)
				Df = DF[(DF.alpha .== alpha).&(DF.p .== p), :]
				gdf = groupby(Df, :testcase, sort = true)
				showlegend = (alpha == 0) ? true : false

				for df in gdf
					data = scatter(;
								x=df.time_sec, 
								y=df.global_primal_res, 
								yaxis = dic_alpha["yaxis"],
								name = string("delta = ",df.delta[1], ", p = ", df.p[1]),
								showlegend = showlegend,
								mode = "lines+markers",
								marker = attr(
										symbol = df.marker[1],
										maxdisplayed = 10,
										color = df.color[1],
										),
								line = attr(
										dash = dic_p["line"]
										),
								)
					push!(trace, data)
				end
			end
		end
		layout = Layout(
					# height = 360,
					# width = 328,
					# margin = attr(
					# 			l = 35,
					# 			r = 10,
					# 			t = 10,
					# 			b = 25,
					# 			),			
					showlegend = true,
					legend = attr(
								x = 0.65,
								y = 0.99,
								font_size = 10,
								font_family = "PT Sans Narrow",
								),
					xaxis = attr(
						# range = [0,1000],
						range = [0,700],
						title = attr(
								text = "Temps (s)",
								font_size = 10,
								font_color = "#444444",
								font_family = "PT Sans Narrow",
							), 
						anchor = "y3",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						# zeroline=true,
						ticks = "inside",
						# dtick = 1,
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
								),
					yaxis = attr(
						range = [-12,3],
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.66, 1],
								),
					yaxis2 = attr(
						range = [-12,3],
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0.33, 0.63],
								),
					yaxis3 = attr(
						range = [-12,3],
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-6, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						domain = [0, 0.30],
								),
			    	annotations = [
							attr(
							      visible = true,
							      text = alphas[0]["name"],
							      font = attr( 
							          size = 11, 
							          # color = "black", 
							          family = "PT Sans Narrow"),
							      showarrow = false,
							      yref = "y",
							      x = 350,
							      y = 1,
							  ),
							attr(
                                  visible = true,
                                  text = alphas[0.5]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y2",
                                  x = 350,
                                  y = 1,
                              ),
							attr(
                                  visible = true,
                                  text = alphas[1]["name"],
                                  font = attr( 
                                      size = 11, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y3",
                                  x = 350,
                                  y = 1,
                              ),
                      ],
			)

		plot(trace, layout)
	end

	function plot_local_primal_res(;alpha::Number, delta::Number, ntrig_texts::Array{String,1}= [""])
		trace = GenericTrace[]
		for txt in ntrig_texts
			plot_local_primal_res(trace; alpha=alpha, delta=delta, ntrig_text = txt)
		end
		layout = Layout(
							# height = 150,
							# width = 328,
							height = 350,
							width = 700,
							margin = attr(
										l = 30,
										r = 10,
										t = 10,
										b = 25,
										),			
							showlegend = true,
							legend = attr(
										x = 0.65,
										y = 0.95,
										font_size = 10,
										font_family = "PT Sans Narrow",
										),
							xaxis = attr(
								range = [0,1000],
								title = attr(
										text = "Temps (s)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								anchor = "y3",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								# zeroline=true,
								ticks = "inside",
								# dtick = 1,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								range = [-20,3],
								title = attr(
									text = "Résidu primal",
									font_size = 10,
									font_color = "#444444",
									font_family = "PT Sans Narrow",
								), 
								type = "log",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=false,
								ticks = "inside",
								dtick = 6,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								# domain = [0.66, 1],
										),
			)
		plot(trace, layout)
	end

	function plot_local_primal_res(trace::Array{GenericTrace};alpha::Number, delta::Number, ntrig_text::String = "")
		@assert alpha ∈[0,0.5,1] "Uncorrect alpha value"
		@assert delta ∈[0.1,0.4,0.6,0.8,1.0] "Uncorrect delta value"

		alpha_text = Dict(
						0=> "0_0",
						0.5=>"0_5",
						1=>"1_0",
			)
		delta_text = Dict(
						0.1 => "0_1",
						0.4 => "0_4",
						0.6 => "0_6",
						0.8 => "0_8",
						1.0 => "1_0",
			)

		filepath = string("chronograf_results/cccd - alpha", alpha_text[alpha]," - delta",delta_text[delta],ntrig_text," - monitor.csv")

		Df = RPiAsync.create_dataframe_monitor(filepath)
		Df.alpha = fill(alpha, size(Df,1))
		Df.delta = fill(delta, size(Df,1))
		Df.time_sec = Dates.value.(Df.datetime .- Df.datetime[1])/1000 # time in seconds

		gdf = groupby(Df, :i, sort = true)
		for df in gdf
			data = scatter(x=df.time_sec, y=df.local_primal_res, name = string(df.i[1]))
			push!(trace,data)
		end
	end

	function plot_async_gain(DF::DataFrame, residual::Float64)
		Df_final = DataFrame()
		gdf_alpha = groupby(DF, :alpha, sort = true)

		# Traitement de données
		for df_alpha in gdf_alpha
			gdf_p = groupby(df_alpha, :p, sort = true)
			for df_p in gdf_p
				# Comparaison entre delta = 1 et delta = 0.1
				df_sync = df_p[df_p.delta .== 1, :]
				df_async = df_p[df_p.delta .== 0.1, :]

				# Durée de convergence à résidu <10^-12
				id_sync = findlast(x->x==true, df_sync.global_primal_res .> residual)
				id_async = findlast(x->x==true, df_async.global_primal_res .> residual)

				# id_sync = findfirst(x->x==false, df_sync.global_primal_res .> residual)
				# id_async = findfirst(x->x==false, df_async.global_primal_res .> residual)

				t_sync = df_sync[id_sync,:time_sec]
				t_async = df_async[id_async,:time_sec]
				time_gain = (t_sync - t_async)/t_sync

				df = DataFrame(	:alpha => df_p.alpha[1], 
								:p => df_p.p[1],
								:epsilon => residual,
								:t_sync => t_sync,
								:t_async => t_async, 
								:time_gain => time_gain,
								:marker =>df_p.marker[1],
								:color =>df_p.color[1],
								)
				Df_final = vcat(Df_final, df)
			end
		end

		# Affichage des résultats
		trace = GenericTrace[]
		for alpha in sort(unique(Df_final.alpha))
			if alpha == 0
				marker = "circle"
			elseif alpha == 0.5
				marker = "square"
			else
				marker = "diamond"
			end
			df = Df_final[Df_final.alpha .== alpha,:]
			data = scatter(;
				x=df.p.*100, 
				y=df.time_gain.*100,
				# y=df.t_sync.-df.t_async,
				# y=df.t_async, 
				yaxis = "y",
				name = string("alpha = ", alpha),
				showlegend = true,
				mode = "markers",
				marker = attr(
						symbol = marker,
						# maxdisplayed = 10,
						# color = df.color[1],
						),
				)
			push!(trace,data)
		end

		Df_final
		layout = Layout(
							height = 200,
							width = 328,
							margin = attr(
										l = 30,
										r = 10,
										t = 20,
										b = 25,
										),			
							showlegend = true,
							title = attr(
										text = "Gain de temps de l'algorithme asynchrone (%)",
										font_size = 12,
										font_family = "PT Sans Narrow",
								),
							legend = attr(
										x = 0.65,
										y = 0.01,
										font_size = 10,
										font_family = "PT Sans Narrow",
										),
							xaxis = attr(
								range = [0,10],
								title = attr(
										text = "Pertes de messages (%)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								anchor = "y",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=true,
								ticks = "inside",
								dtick = 1,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								# range = [-20,3],
								title = attr(
									# text = "Gain de temps de la solution asynchrone (%)",
									font_size = 10,
									font_color = "#444444",
									font_family = "PT Sans Narrow",
								), 
								type = "linear",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=true,
								ticks = "inside",
								dtick = 10,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								# domain = [0.66, 1],
										),
			)
		plot(trace, layout)
	end

	function plot_async_gain2(DF::DataFrame, residual::Float64)
		Df_final = DataFrame()
		gdf_alpha = groupby(DF, :alpha, sort = true)

		# Traitement de données
		for df_alpha in gdf_alpha
			gdf_p = groupby(df_alpha, :p, sort = true)
			for df_p in gdf_p
				# Comparaison entre delta = 1 et delta = 0.1
				df_sync = df_p[df_p.delta .== 1, :]
				df_async = df_p[df_p.delta .== 0.1, :]

				# Durée de convergence à résidu <10^-12
				id_sync = findlast(x->x==true, df_sync.global_primal_res .> residual)
				id_async = findlast(x->x==true, df_async.global_primal_res .> residual)

				# id_sync = findfirst(x->x==false, df_sync.global_primal_res .> residual)
				# id_async = findfirst(x->x==false, df_async.global_primal_res .> residual)

				t_sync = df_sync[id_sync,:time_sec]
				t_async = df_async[id_async,:time_sec]
				time_gain = (t_sync - t_async)/t_sync

				df = DataFrame(	:alpha => df_p.alpha[1], 
								:p => df_p.p[1],
								:epsilon => residual,
								:t_sync => t_sync,
								:t_async => t_async, 
								:time_gain => time_gain,
								)
				Df_final = vcat(Df_final, df)
			end
		end
		marker_async = DF[DF.delta.==0.1,:marker][1]
		marker_sync = DF[DF.delta.==1.0,:marker][1]
		color_async = DF[DF.delta.==0.1,:color][1]
		color_sync = DF[DF.delta.==1.0,:color][1]

		# Affichage des résultats
		trace = GenericTrace[]
		for alpha in sort(unique(Df_final.alpha))
			df = Df_final[Df_final.alpha .== alpha,:]
			if alpha == 0
				yax = "y"
				showleg = true
			elseif alpha == 0.5
				yax = "y2"
				showleg = false
				continue
			else
				yax = "y3"
				showleg = false
			end
			data = scatter(;
				x=df.p.*100, 
				y=df.t_sync./0.5, 
				yaxis = yax,
				name = string("delta = 1.0"),
				showlegend = showleg,
				mode = "markers",
				marker = attr(
						symbol = marker_sync,
						color = color_sync,
						),
				)
			push!(trace,data)
			data = scatter(;
				x=df.p.*100, 
				y=df.t_async./0.5, 
				yaxis = yax,
				name = string("delta = 0.1"),
				showlegend = showleg,
				mode = "markers",
				marker = attr(
						symbol = marker_async,
						# maxdisplayed = 10,
						color = color_async,
						),
				)
			push!(trace,data)
		end

		Df_final
		ydtick = 100
		yrange = [0,260]
		xrange = [0,20]
		layout = Layout(
							height = 280,
							width = 328,
							margin = attr(
										l = 40,
										r = 10,
										t = 25,
										b = 25,
										),		
							showlegend = true,
							# title = attr(
							# 			text = "Temps de convergence (s) à ε = 10<sup>-8</sup>",
							# 			font_size = 12,
							# 			font_family = "PT Sans Narrow",
							# 	),
							legend = attr(
										x = 0.65,
										y = 0.01,
										font_size = 10,
										font_family = "PT Sans Narrow",
										),
							xaxis = attr(
								range = xrange,
								title = attr(
										text = "Pertes de messages ψ (%)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								anchor = "y3",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=true,
								ticks = "inside",
								dtick = 5,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								range = yrange,
								title = attr(
									# text = "Temps relatif, pour ε = 10<sup>-6</sup>",
									text = "Temps de convergence<br>relatif",
									font_size = 10,
									font_color = "#444444",
									font_family = "PT Sans Narrow",
								), 
								type = "linear",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=true,
								ticks = "inside",
								dtick = ydtick,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								domain = [0.52, 1],
										),
							# yaxis2 = attr(
							# 	range = yrange,
							# 	title = attr(
							# 		# text = "Gain de temps de la solution asynchrone (%)",
							# 		font_size = 10,
							# 		font_color = "#444444",
							# 		font_family = "PT Sans Narrow",
							# 	), 
							# 	type = "linear",
							# 	tickfont_size = 10,
							# 	tickfont_family = "PT Sans Narrow",
							# 	zeroline=true,
							# 	ticks = "inside",
							# 	dtick = ydtick,
							# 	showline = true,
							# 	linecolor= "black",
							# 	linewidth= 0.5,
							# 	mirror= "allticks",
							# 	domain = [0.34, 0.65],
							# 			),
							yaxis3 = attr(
								range = yrange,
								title = attr(
									text = "Temps de convergence<br>relatif",
									font_size = 10,
									font_color = "#444444",
									font_family = "PT Sans Narrow",
								), 
								type = "linear",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=true,
								ticks = "inside",
								dtick = ydtick,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								domain = [0, 0.48],
										),
							annotations = [
								attr(
								      visible = true,
								      text = "alpha = 0",
								      font = attr( 
								          size = 11, 
								          # color = "black", 
								          family = "PT Sans Narrow"),
								      showarrow = false,
								      yref = "y",
								      x = (xrange[2]-xrange[1])/2,
								      y = yrange[2]*0.9,
								  ),
								# attr(
	       #                            visible = true,
	       #                            text = "alpha = 0.5",
	       #                            font = attr( 
	       #                                size = 11, 
	       #                                # color = "black", 
	       #                                family = "PT Sans Narrow"),
	       #                            showarrow = false,
	       #                            yref = "y2",
	       #                            x = (xrange[2]-xrange[1])/2,
	       #                            y = yrange[2]*0.9,
	       #                        ),
								attr(
	                                  visible = true,
	                                  text = "alpha = 1",
	                                  font = attr( 
	                                      size = 11, 
	                                      # color = "black", 
	                                      family = "PT Sans Narrow"),
	                                  showarrow = false,
	                                  yref = "y3",
	                                  x = (xrange[2]-xrange[1])/2,
	                                  y = yrange[2]*0.9,
	                              ),
                      		],
			)
		plot(trace, layout)
	end

	function plot_loss(DF::DataFrame, residual::Float64)
		Df_final = DataFrame()
		gdf_alpha = groupby(DF, :alpha, sort = true)

		# Traitement de données
		for df_alpha in gdf_alpha
			gdf_p = groupby(df_alpha, :p, sort = true)
			for df_p in gdf_p
				# Comparaison entre delta = 1 et delta = 0.1
				df_sync = df_p[df_p.delta .== 1, :]
				df_async = df_p[df_p.delta .== 0.1, :]

				# Durée de convergence à résidu <10^-12
				id_sync = findlast(x->x==true, df_sync.global_primal_res .> residual)
				id_async = findlast(x->x==true, df_async.global_primal_res .> residual)

				# id_sync = findfirst(x->x==false, df_sync.global_primal_res .> residual)
				# id_async = findfirst(x->x==false, df_async.global_primal_res .> residual)

				t_sync = df_sync[id_sync,:time_sec]
				t_async = df_async[id_async,:time_sec]
				time_gain = (t_sync - t_async)/t_sync

				df = DataFrame(	:alpha => df_p.alpha[1], 
								:p => df_p.p[1],
								:epsilon => residual,
								:t_sync => t_sync,
								:t_async => t_async, 
								:time_gain => time_gain,
								)
				Df_final = vcat(Df_final, df)
			end
		end
		marker_async = Dict(0.0 => "triangle-down", 1.0 => "triangle-down-open")
		marker_sync = Dict(0.0 => "circle", 1.0 => "circle-open")
		color_async = DF[DF.delta.==0.1,:color][1]
		color_sync = DF[DF.delta.==1.0,:color][1]

		# Affichage des résultats
		trace = GenericTrace[]
		for alpha in sort(unique(Df_final.alpha))
			if alpha != 0.5
				df = Df_final[Df_final.alpha .== alpha,:]
				data = scatter(;
					x=df.p.*100, 
					y=df.t_sync./0.5, 
					yaxis = "y",
					name = string("alpha = ",alpha," ,delta = 1.0"),
					# showlegend = showleg,
					mode = "markers",
					marker = attr(
							symbol = marker_sync[alpha],
							color = color_sync,
							),
					)
				push!(trace,data)
				data = scatter(;
					x=df.p.*100, 
					y=df.t_async./0.5, 
					yaxis = "y",
					name = string("alpha = ",alpha," ,delta = 0.1"),
					# showlegend = showleg,
					mode = "markers",
					marker = attr(
							symbol = marker_async[alpha],
							# maxdisplayed = 10,
							color = color_async,
							),
					)
				push!(trace,data)
			end
		end

		Df_final
		ydtick = 100
		yrange = [80,300]
		xrange = [0,20]
		layout = Layout(
							# height = 400,
							# width = 328,
							# margin = attr(
							# 			l = 30,
							# 			r = 10,
							# 			t = 25,
							# 			b = 25,
							# 			),		
							showlegend = true,
							# title = attr(
							# 			text = "Temps de convergence (s) à ε = 10<sup>-8</sup>",
							# 			font_size = 12,
							# 			font_family = "PT Sans Narrow",
							# 	),
							legend = attr(
										x = 0.65,
										y = 0.01,
										font_size = 10,
										font_family = "PT Sans Narrow",
										),
							xaxis = attr(
								range = xrange,
								title = attr(
										text = "Pertes de messages (%)",
										font_size = 10,
										font_color = "#444444",
										font_family = "PT Sans Narrow",
									), 
								anchor = "y3",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=true,
								ticks = "inside",
								dtick = 5,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
										),
							yaxis = attr(
								range = yrange,
								title = attr(
									# text = "Gain de temps de la solution asynchrone (%)",
									font_size = 10,
									font_color = "#444444",
									font_family = "PT Sans Narrow",
								), 
								type = "linear",
								tickfont_size = 10,
								tickfont_family = "PT Sans Narrow",
								zeroline=true,
								ticks = "inside",
								dtick = ydtick,
								showline = true,
								linecolor= "black",
								linewidth= 0.5,
								mirror= "allticks",
								# domain = [0.67, 1],
										),
							# annotations = [
							# 	attr(
							# 	      visible = true,
							# 	      text = "alpha = 0",
							# 	      font = attr( 
							# 	          size = 11, 
							# 	          # color = "black", 
							# 	          family = "PT Sans Narrow"),
							# 	      showarrow = false,
							# 	      yref = "y",
							# 	      x = (xrange[2]-xrange[1])/2,
							# 	      y = yrange[2]*0.9,
							# 	  ),
							# 	attr(
	      #                             visible = true,
	      #                             text = "alpha = 0.5",
	      #                             font = attr( 
	      #                                 size = 11, 
	      #                                 # color = "black", 
	      #                                 family = "PT Sans Narrow"),
	      #                             showarrow = false,
	      #                             yref = "y2",
	      #                             x = (xrange[2]-xrange[1])/2,
	      #                             y = yrange[2]*0.9,
	      #                         ),
							# 	attr(
	      #                             visible = true,
	      #                             text = "alpha = 1",
	      #                             font = attr( 
	      #                                 size = 11, 
	      #                                 # color = "black", 
	      #                                 family = "PT Sans Narrow"),
	      #                             showarrow = false,
	      #                             yref = "y3",
	      #                             x = (xrange[2]-xrange[1])/2,
	      #                             y = yrange[2]*0.9,
	      #                         ),
       #                		],
			)
		plot(trace, layout)
	end

	function plot_primal_res_pause(DF::DataFrame)
		alphas = Dict(
				0 => Dict("name" =>"alpha = 0" , "yaxis" => "y" ) ,
				# 0.5 => Dict("name" =>"alpha = 0.5" , "yaxis" => "y2") ,
				# 1 => Dict("name" =>"alpha = 1" , "yaxis" => "y3") ,
			)

		trace = GenericTrace[]

		for (alpha, dic_alpha) in sort(alphas)
			Df = DF[DF.alpha .== alpha, :]
			gdf = groupby(Df, :delta, sort = true)
			showlegend = (alpha ==0) ? true : false

			for df in gdf
				data = scatter(;
							x=df.time_sec ./0.5, 
							y=df.global_primal_res, 
							yaxis = dic_alpha["yaxis"],
							name = df.testcase[1],
							showlegend = showlegend,
							mode = "lines+markers",
							marker = attr(
									symbol = df.marker[1],
									maxdisplayed = 10,
									color = df.color[1],
									),
							)
				push!(trace, data)
			end
		end
		layout = Layout(
					height = 200,
					width = 328,
					margin = attr(
								l = 35,
								r = 10,
								t = 10,
								b = 25,
								),			
					showlegend = true,
					legend = attr(
								x = 0.65,
								y = 0.99,
								font_size = 10,
								font_family = "PT Sans Narrow",
								# traceorder = "reversed",
								),
					xaxis = attr(
						# range = [0,1000],
						range = [0,300],
						title = attr(
								text = "Temps relatif",
								font_size = 10,
								font_color = "#444444",
								font_family = "PT Sans Narrow",
							), 
						anchor = "y3",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						# zeroline=true,
						ticks = "inside",
						dtick = 60,
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
								),
					yaxis = attr(
						range = [-15,3],
						title = attr(
							text = "Résidu primal",
							font_size = 10,
							font_color = "#444444",
							font_family = "PT Sans Narrow",
						), 
						type = "log",
						tickfont_size = 10,
						tickfont_family = "PT Sans Narrow",
						zeroline=false,
						ticks = "inside",
						tickvals = [1e0, 1e-3, 1e-6, 1e-9, 1e-12, 1e-18],
						ticktext = ["1", "10<sup>-3</sup>", "10<sup>-6</sup>", "10<sup>-9</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
						showline = true,
						linecolor= "black",
						linewidth= 0.5,
						mirror= "allticks",
						# domain = [0.66, 1],
								),
					# yaxis2 = attr(
					# 	range = [-12,3],
					# 	title = attr(
					# 		text = "Résidu primal",
					# 		font_size = 10,
					# 		font_color = "#444444",
					# 		font_family = "PT Sans Narrow",
					# 	), 
					# 	type = "log",
					# 	tickfont_size = 10,
					# 	tickfont_family = "PT Sans Narrow",
					# 	zeroline=false,
					# 	ticks = "inside",
					# 	tickvals = [1e0, 1e-6, 1e-12, 1e-18],
					# 	ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
					# 	showline = true,
					# 	linecolor= "black",
					# 	linewidth= 0.5,
					# 	mirror= "allticks",
					# 	domain = [0.33, 0.63],
					# 			),
					# yaxis3 = attr(
					# 	range = [-12,3],
					# 	title = attr(
					# 		text = "Résidu primal",
					# 		font_size = 10,
					# 		font_color = "#444444",
					# 		font_family = "PT Sans Narrow",
					# 	), 
					# 	type = "log",
					# 	tickfont_size = 10,
					# 	tickfont_family = "PT Sans Narrow",
					# 	zeroline=false,
					# 	ticks = "inside",
					# 	tickvals = [1e0, 1e-6, 1e-12, 1e-18],
					# 	ticktext = ["1", "10<sup>-6</sup>", "10<sup>-12</sup>", "10<sup>-18</sup>"],
					# 	showline = true,
					# 	linecolor= "black",
					# 	linewidth= 0.5,
					# 	mirror= "allticks",
					# 	domain = [0, 0.30],
					# 			),
			    # 	annotations = [
							# attr(
							#       visible = true,
							#       text = alphas[0]["name"],
							#       font = attr( 
							#           size = 11, 
							#           # color = "black", 
							#           family = "PT Sans Narrow"),
							#       showarrow = false,
							#       yref = "y",
							#       x = 500,
							#       y = 1,
							#   ),
							# attr(
       #                            visible = true,
       #                            text = alphas[0.5]["name"],
       #                            font = attr( 
       #                                size = 11, 
       #                                # color = "black", 
       #                                family = "PT Sans Narrow"),
       #                            showarrow = false,
       #                            yref = "y2",
       #                            x = 500,
       #                            y = 1,
       #                        ),
							# attr(
       #                            visible = true,
       #                            text = alphas[1]["name"],
       #                            font = attr( 
       #                                size = 11, 
       #                                # color = "black", 
       #                                family = "PT Sans Narrow"),
       #                            showarrow = false,
       #                            yref = "y3",
       #                            x = 500,
       #                            y = 1,
       #                        ),
                      # ],
			)

		plot(trace, layout)
	end


end