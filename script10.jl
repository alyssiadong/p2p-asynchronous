# Simulations julia associées aux résolutions de RPiAsync

# include("PowerNetwork.jl")
# include("CommunicationDelays.jl")
# include("Planification.jl")
# include("market_sim.jl")
# include("write_results.jl")

include("RPiAsync.jl")
using DataFrames
using JLD
using ORCA

par = PowerNetwork.power_network("n9_p3_c6_0xcccd.csv", "full_P2P")
JLDfile = WR.JLDFile("n9_p3_c6_0xcccd_rho1.jld", 
	"results/n9_p3_c6_0xcccd_rho1.jld",
	"n9_p3_c6_0xcccd.csv")

df = JLDfile.df_params
res = WR.readRes(JLDfile, df, ["primal_res"])

function make_DF(df::DataFrame, res::Dict)

	deltas = Dict(
				1.0 => Dict("testcase"=> "delta=1.0", "marker"=>"circle",		 	"color"=>"#636EFA"),
				0.8 => Dict("testcase"=> "delta=0.8", "marker"=>"diamond",		 	"color"=>"#EF553B"),
				0.6 => Dict("testcase"=> "delta=0.6", "marker"=>"star-diamond",		"color"=>"#00CC96"),
				0.4 => Dict("testcase"=> "delta=0.4", "marker"=>"triangle-up",		"color"=>"#AB63FA"),
				0.1 => Dict("testcase"=> "delta=0.1", "marker"=>"triangle-down",	"color"=>"#FFA15A"),
		)

	DF = DataFrame()
	for i in 1:size(df,1)
		NID = df.NID[i]
		δt = df.sampling_period[i]
		global_primal_res = dropdims(sum(res[NID]["primal_res"],dims=1),dims=1)
		time_sec = collect(1:length(global_primal_res)) .*δt
		df_block = DataFrame(	:alpha => df.alpha[i] .*ones(size(time_sec)),
							:delta => df.delta[i] .*ones(size(time_sec)),
							:p =>0 .*ones(size(time_sec)),
							:time_sec =>time_sec,
							:global_primal_res=>global_primal_res,
							:testcase =>fill(deltas[df.delta[i]]["testcase"], length(time_sec)),
							:marker =>fill(deltas[df.delta[i]]["marker"], length(time_sec)),
							:color =>fill(deltas[df.delta[i]]["color"], length(time_sec)),
			)
		DF = vcat(DF, df_block)
	end
	DF
end

DF = make_DF(df, res)
RPiAsync.plot_primal_res(DF)
# savefig(RPiAsync.plot_primal_res(DF), "images_SGE/convergence_simu.pdf", format="pdf")