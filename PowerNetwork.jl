module PowerNetwork
    export power_network, _unpack_power_network_params, _unpack_power_network_topology
    using CSV
    using LightGraphs, SimpleWeightedGraphs
    using LinearAlgebra, SparseArrays
    using DataFrames
    using OSQP
    using PlotlyJS

### Parameters' structure
    struct power_network
        
        filename::String                        # Path to the .csv file.
        topo::String                            # "central" or "full_P2P"
        assets::Array{Int}                      # assets[n] List of all assets
        type_assets::Array{String}              # type_assets[n] type of asset n : "Prod" or "Cons" or "CA"
                                                # cost function : fn(pn) = an*pn^2 + bn*pn
                                                #   s.t. Pmin_n ≤ pn ≤ Pmax_n
        gencost_a::Array{Float64}               # gencost_a[n] = an
        gencost_b::Array{Float64}               # gencost_b[n] = bn
        Pmin::Array{Float64}                    # Pmin[n] = Pmin_n
        Pmax::Array{Float64}                    # Pmax[n] = Pmax_n
        loc::Array{Float64}                     # loc[n, 1:2] = x and y position of asset n
        Ω::Array{Int}                           # Ω[:] Set of market participants
        A::Vector{Vector{Int}}                  # A[i][:] Available assets of market participant i
        # ω::Vector{Vector{Int}}                 # ω[i][:] Trading partners of market participant i
        comm_graph::SimpleWeightedGraph{Int64}  # Communication graph between commercial partners

        preferred_rho::Number
        function power_network(test_market::Union{String, Array{Int,1}}, topo::String)
            ### Power network parameters initialization with any agents testcase.
            ### Every agent only has one asset.
            @assert topo ∈ ["central", "full_P2P"] "Wrong topology."

            Df = DataFrame()
            if typeof(test_market) == String            # Existing testcase.
                @assert endswith(test_market, ".csv" ) "'test_market' must be a path to a .csv file."
                Df = CSV.read(string("testcase/",test_market))
                filename = test_market
            elseif typeof(test_market) == Array{Int,1}    # New testcase.
                @assert length(test_market) == 2 "Length of 'test_market' must be 2 (N_prod, N_cons)."
                Df, filename = new_testcase(test_market[1], test_market[2])
            else
                error("Incorrect type.")
            end

            if topo == "central"
                ### Centralized configuration, with central agent put last
                type_assets = vcat(Df.type, "CA")
                gen_a = vcat(Df.gencost_a, 0.0)
                gen_b = vcat(Df.gencost_b, 0.0)
                P_min = vcat(Df.Pmin, 0.0)
                P_max = vcat(Df.Pmax, 0.0)
                loc = vcat(hcat(Df.loc_x, Df.loc_y), [0.5  0.5])

                N = size(Df,1)
                Ω = vcat(Df.id_agent, N+1)

                A0 = Vector{Vector{Int}}([[i] for i∈Df.id_agent])
                A = vcat(A0, [[N+1]])

                # ω0 =  fill([N+1], size(Df,1))
                # ω = Vector{Vector{Int}}(vcat(ω0, [Df.id_agent]))
                # comm_graph = SimpleGraph(length(Ω), ω)
                
                I = vcat( 1:N, fill(N+1,N)) 
                J = vcat( fill(N+1, N), 1:N)
                dist = [distance(loc[N+1,:],loc[i,:]) for i=1:N]
                V = repeat(dist, 2)
                adj_matrix = sparse(I, J, V)
                comm_graph = SimpleWeightedGraph(adj_matrix)
            elseif topo == "full_P2P"
                ### Full peer to peer configuration : all consumers are partners
                ### with all producers
                type_assets = Df.type
                gen_a = Array{Float64}(Df.gencost_a)
                gen_b = Array{Float64}(Df.gencost_b)
                P_min = Array{Float64}(Df.Pmin)
                P_max = Array{Float64}(Df.Pmax)
                loc = hcat(Df.loc_x, Df.loc_y)

                Ω = Df.id_agent
                A = Vector{Vector{Int}}([[i] for i∈Df.id_agent])

                Cons = Df[Df.type.=="cons",:id_agent]
                Prod = Df[Df.type.=="prod",:id_agent]
                N_cons = size(Cons,1)
                N_prod = size(Prod,1)
                # ω = Vector{Vector{Int}}( vcat( repeat([Prod], N_cons),
                #     repeat([Cons], N_prod)))
                # comm_graph = SimpleGraph(length(Ω), ω)
                I = collect(reshape(repeat(Prod,1,N_cons)',N_cons*N_prod))
                J = repeat(Cons,N_prod)
                V = reshape([distance(loc[i,:], loc[j,:]) for j=Prod, i=Cons], N_cons*N_prod)

                # comm_graph = SimpleWeightedGraph(vcat(I,J),vcat(J,I),repeat(V,2))   # !!! this counts twice the distance !!!
                comm_graph = SimpleWeightedGraph(I,J,V)   
            end

            if startswith(filename, "n110_p30_c80_0x000f")
                preferred_rho = 15
            elseif startswith(filename, "n31_p10_c21_0xeeee")
                preferred_rho = 5
            elseif startswith(filename, "n31_p10_c21_0x001c")
                preferred_rho = 2
            elseif startswith(filename, "n3_p1_c2_0x0004")
                preferred_rho = 0.8
            elseif startswith(filename, "n9_p3_c6_0x0004")
                preferred_rho = 0.5
            elseif startswith(filename, "n9_p3_c6_0x0007")
                preferred_rho = 0.5
            elseif startswith(filename, "n9_p3_c6_0xcccc")
                preferred_rho = 10
            elseif startswith(filename, "n9_p3_c6_0xcccd")
                preferred_rho = 10
            else 
                preferred_rho = 1
                println("   Warning : preferred penalty factor (rho) set to ", preferred_rho, ". \n     Run the ’penalty_factor_tuning’ function to set the correct preferred rho.")
            end
            new(filename, topo, Df.id_agent, type_assets, gen_a, gen_b, 
                P_min, P_max, loc, Ω, A, comm_graph, preferred_rho)
        end

        function power_network(Df::DataFrame, test_market::String, topo::String)
            @assert topo ∈ ["central", "full_P2P"] "Wrong topology."
            test_market = "test"

            if topo == "central"
                ### Centralized configuration, with central agent put last
                type_assets = vcat(Df.type, "CA")
                gen_a = vcat(Df.gencost_a, 0.0)
                gen_b = vcat(Df.gencost_b, 0.0)
                P_min = vcat(Df.Pmin, 0.0)
                P_max = vcat(Df.Pmax, 0.0)
                loc = vcat(hcat(Df.loc_x, Df.loc_y), [0.5  0.5])

                N = size(Df,1)
                Ω = vcat(Df.id_agent, N+1)

                A0 = Vector{Vector{Int}}([[i] for i∈Df.id_agent])
                A = vcat(A0, [[N+1]])

                # ω0 =  fill([N+1], size(Df,1))
                # ω = Vector{Vector{Int}}(vcat(ω0, [Df.id_agent]))
                # comm_graph = SimpleGraph(length(Ω), ω)
                
                I = vcat( 1:N, fill(N+1,N)) 
                J = vcat( fill(N+1, N), 1:N)
                dist = [distance(loc[N+1,:],loc[i,:]) for i=1:N]
                V = repeat(dist, 2)
                adj_matrix = sparse(I, J, V)
                comm_graph = SimpleWeightedGraph(adj_matrix)

            elseif topo == "full_P2P"
                ### Full peer to peer configuration : all consumers are partners
                ### with all producers
                type_assets = Df.type
                gen_a = Array{Float64}(Df.gencost_a)
                gen_b = Array{Float64}(Df.gencost_b)
                P_min = Array{Float64}(Df.Pmin)
                P_max = Array{Float64}(Df.Pmax)
                loc = hcat(Df.loc_x, Df.loc_y)

                Ω = Df.id_agent
                A = Vector{Vector{Int}}([[i] for i∈Df.id_agent])

                Cons = Df[Df.type.=="cons",:id_agent]
                Prod = Df[Df.type.=="prod",:id_agent]
                N_cons = size(Cons,1)
                N_prod = size(Prod,1)
                # ω = Vector{Vector{Int}}( vcat( repeat([Prod], N_cons),
                #     repeat([Cons], N_prod)))
                # comm_graph = SimpleGraph(length(Ω), ω)
                I = collect(reshape(repeat(Prod,1,N_cons)',N_cons*N_prod))
                J = repeat(Cons,N_prod)
                V = reshape([distance(loc[i,:], loc[j,:]) for j=Prod, i=Cons], N_cons*N_prod)

                comm_graph = SimpleWeightedGraph(vcat(I,J),vcat(J,I),repeat(V,2))
            end
            println("   Warning : setting preferred penalty factor to 1.")
            new(test_market, topo, Df.id_agent, type_assets, gen_a, gen_b, 
                P_min, P_max, loc, Ω, A, comm_graph, 1.0)
        end
    end
    _unpack_power_network_params(x::power_network) = (
        x.assets,
        x.gencost_a, x.gencost_b,
        x.Pmin, x.Pmax,
        x.loc
        )
    _unpack_power_network_topology(x::power_network) = (
        x.Ω,
        x.A,
        x.comm_graph
    )


### Distance computation
    function distance(loc_a::Array{Float64}, loc_b::Array{Float64})
        ### Computes the euclidian distance between loc_a = [xa, ya]
        ### and loc_b = [xb, yb].
        return sqrt((loc_a[1]-loc_b[1])^2 + (loc_a[2]-loc_b[2])^2)
    end
### Creation of new test case
    function new_testcase(N_prod::Int, N_cons::Int)
        ### Creates a set of coefficients for N_prod producers and 
        ### N_cons consumers.
        ### Saves it in a .csv file.
        ### fi(xi) = ai*xi²+bi*xi+ci = Ai*(xi-xi0)²

        ### Number of testcase 
        ### filename of type : "n30_p10_c20_0x0020.csv"
        N_Ω = N_prod + N_cons
        filename = string("n", N_Ω, "_p", N_prod, "_c", N_cons, "_0x")
        files = readdir("testcase")

        files = files[map(x->startswith(x, filename), files)]
        number_max = 0
        for f in files
            rex = match(r"_0x", f)
            offset = rex.offset 
            number = parse(Int, f[offset+1:offset+6])
            if number>number_max
                number_max = number
            end
        end

        complete_filename = string(filename, 
            string(number_max+1, base = 16, pad = 4), 
            ".csv")

        ### Creating new agent coefficients
        a_mean_cons = 35e-3
        a_std_cons = 6.7e-3
        a_mean_prod = 35e-3
        a_std_prod = 6.7e-3

        b_mean_cons = 70
        b_std_cons = 10
        b_mean_prod = 28
        b_std_prod = 8

        xmin_cons = -1500
        xmax_cons = 0
        xmin_prod = 0
        xmax_prod = 1100

        A_prod = abs.(randn(N_prod)*a_std_prod.+a_mean_prod)
        A_cons = abs.(randn(N_cons)*a_std_cons.+a_mean_cons)
        B_prod = floor.(abs.(randn(N_prod)*b_std_prod.+b_mean_prod))
        B_cons = floor.(abs.(randn(N_cons)*b_std_cons.+b_mean_cons))
        X_prod = rand(xmin_prod:xmax_prod,N_prod)
        X_cons = rand(xmin_cons:xmax_cons,N_cons)


        # Concatenation
        A = vcat(A_prod, A_cons)
        B = vcat(B_prod, B_cons)
        xmax = vcat(X_prod, zeros(Int,N_cons))
        xmin = vcat(zeros(Int,N_prod), X_cons)
        type = vcat(fill("prod", N_prod), fill("cons", N_cons))

        Df = DataFrame(:gencost_a=>A, :gencost_b=>B, :Pmin=>xmin, :Pmax=>xmax, 
            :type=>type)

        # Location 
        Df.loc_x = rand(N_prod+N_cons)
        Df.loc_y = rand(N_prod+N_cons)

        # Id agent
        Df.id_agent = collect(1:N_Ω)

        ### Save file
        CSV.write(string("testcase/", complete_filename), Df)

        return Df, complete_filename
    end

### Optimal result
    function optimal_res(pn::power_network)
        ### Solves the problem in a centralized, synchronous way,
        ### using OSQP solver.
        ### OSQP solves quadratic problem that has the form :
        ###         min x'Px + q'x
        ###         s.t. l ≤ Ax ≤ u
        ### with P and A sparse matrix

        an = pn.gencost_a
        bn = pn.gencost_b
        pmin = pn.Pmin
        pmax = pn.Pmax
        N_Ω = length(an)

        # P = sparse(Diagonal(2 .*an))
        P = sparse(4 .*Diagonal(an))
        q = bn

        A = sparse(vcat(ones(N_Ω)',Matrix(1.0I,N_Ω,N_Ω)))
        l = vcat(0.0,pmin)
        u = vcat(0.0,pmax)

        m = OSQP.Model()
        OSQP.setup!(m, P=P, q=q,
            A=A, l=l, u=u, verbose=false)   # Model setup with the right arrays
        results = OSQP.solve!(m)            # Solve !
        T_sol = results.x                   # x = Arg min(problem)
    end

    function optimal_res_admm(pn::power_network, ρ::Number, Eprim2_rel::Number, γ::Number)
        ### Solves the problem in a distributed (tradewise), synchronous way,
        ### using OSQP solver.

        an = pn.gencost_a
        bn = pn.gencost_b
        pmin = pn.Pmin
        pmax = pn.Pmax
        N_Ω = length(an)
        N_iter = 5000
        kmax = 5000
        # Eprim2_rel = 1e-12          # As precise as possible
        println("epsilon  = ",Eprim2_rel*sum(max.(pmin.^2, pmax.^2)))

        T = zeros(N_Ω,N_Ω)
        Lambda = zeros(N_Ω,N_Ω)
        for k=1:N_iter
            if k > 1
                Lambda = Lambda - ρ/2*(T+T')
            end
            T_old = T[:,:]
            for i=1:N_Ω
                Ti = local_problem_resolution(i, T_old[i,:], T_old[:,i], 
                    Lambda[i,:], pn, ρ, γ)
                T[i,:] = Ti
            end
            if Eprim2_rel*sum(max.(pmin.^2, pmax.^2)) > sum((T+T').^2)
                kmax = k
                break
            end

            (k==N_iter) && println("Max iteration reached.")
        end

        return T, Lambda, kmax
    end

    function local_problem_resolution(i::Int,
    tij::Array{Float64}, tji::Array{Float64}, λij::Array{Float64},
    par::power_network, ρ::Number, γ::Number)
        # Solves agent n's local problem :
        #   (pi^k+1, ti^k+1) = argmin sum_a∈Ai( fi^a(p_i^a) )
        #               + sum_j∈ωi( ρ/2( (tij^k - tji^k)/2 - tij + λij^k/ρ)^2 )
        #           s.t.    sum_a∈Ai( pi^a ) = sum_j∈( tij )
        #                   pi^a∈Pi^a
        # !!!! Solve the particular case where the agent is associated to one
        # asset only.
        # Inputs :
        #   tij[j] = tij^k    (j∈[1, N_Ω])
        #   tji[j] = tji^k
        #   λij[j] = λij^k
        # Output :
        #   Ti[j] = tij^k+1   (j∈[1, N_Ω])

        ### Unpacking necessary parameters
        Ai = par.A[i]
        ωi = outneighbors(par.comm_graph,i)
        assets = par.assets
        type = par.type_assets[i]

        N_Ai = length(Ai)
        N_ωi = length(ωi)

        @assert N_Ai == 1 "Only one asset associated with agent i !"

        if N_ωi ≠ 1
            ### Using OSQP to solve the local problem.
            ### OSQP solves quadratic problem that has the form :
            ###         min x'Px + q'x
            ###         s.t. l ≤ Ax ≤ u
            ### with P and A sparse matrix
            
            # Setting OSQP matrix
            # P1 = ones(N_ωi, N_ωi).*2*par.gencost_a[i] + ρ*I###
            P1 = ones(N_ωi, N_ωi).*4*par.gencost_a[i] + ρ*I
            P2 = Matrix(γ*I, N_ωi, N_ωi)
            P = sparse(P1+P2)
            # P = sparse(P1)

            q = [par.gencost_b[i] - ρ*(tij[j]-tji[j])/2 - λij[j] for j∈ωi]

            # With trade limitations
            A1 = ones(1, N_ωi)
            A2 = Matrix(1.0I, N_ωi, N_ωi)
            A = sparse(vcat(A1,A2))

            if type == "cons"
                l = Float64.(fill(par.Pmin[i], N_ωi+1))
                u = Float64.(fill(0, N_ωi+1))
                u[1] = par.Pmax[i]
            elseif type == "prod"
                l = Float64.(fill(0, N_ωi+1))
                u = Float64.(fill(par.Pmax[i], N_ωi+1))
                l[1] = par.Pmin[i]
            end

            # Without trade limitations
                # A1 = ones(1, N_ωi)
                # A = sparse(A1)
                # l = [par.Pmin[i]]
                # u = [par.Pmax[i]]

            # OSQP resolution
            m = OSQP.Model()    # Creating a quadratic programming optimization model
            OSQP.setup!(m, P=P, q=q,
                A=A, l=l, u=u, verbose=false,
                eps_rel = 1e-10, eps_abs = 1e-10)   # Model setup with the right arrays
            results = OSQP.solve!(m)            # Solve !
            # (i==5) && println(results.info.status==:Solved)

            T_sol = results.x                   # x = Arg min(problem)
        elseif N_ωi == 1
            ### Simple case where agent i trades with only one other agent
            ### Analytic resolution for speed
            T_sol = []
            for j∈ωi
                t = (-par.gencost_b[i] + ρ*( (tij[j]-tji[j])/2 + λij[j]/ρ )) /
                    (4*par.gencost_a[i] + ρ)
                t = min( par.Pmax[i], max( par.Pmin[i], t))
                push!(T_sol, t)
            end
        end

        Ti = copy(tij)
        Ti[ωi] = T_sol
        return Ti
    end

### Penalty factor tuning
    function penalty_factor_tuning(pn::power_network, Eprim2_rel::Number, γ::Number)
        ### Plots and return the number of iteration to reach convergence with several
        ### values of the penalty factor ρ. Gives the final social welfare value to estimate
        ### if the algorithm actually reached convergence.
        ### Start with γ = 0

        println("Tuning penalty factor value.\nThis may take a while...")

        R = [0.15, 0.3, 0.5, 0.8, 1.0, 2.0, 5.0, 8.0, 10.0, 
                15.0, 30.0, 50.0, 75.0, 100.0, 300.0]
        # R = [0.15, 0.3, 0.5, 0.8, 1.0, 2.0, 5.0, 8.0, 10.0, 
        #         15.0, 30.0, 50.0, 75.0, 100.0, 300.0].*10
        # R = [0.15, 0.3, 0.5, 0.8, 1.0, 2.0, 5.0, 8.0, 10.0, 
        #         15.0, 30.0, 50.0, 75.0, 100.0, 300.0, 1000.0, 2000.0].*0.1
        Kmax = zeros(length(R))
        SW = zeros(length(R))

        for (r,ρ) ∈ enumerate(R)
          println(" Iteration ", r, " out of ", length(R))
          T, Lambda, kmax = PowerNetwork.optimal_res_admm(pn, ρ, Eprim2_rel, γ)
          Kmax[r] = kmax
          SW[r] = sum(abs.(T).*Lambda)
          display(Lambda)
        end

        Df = DataFrame(:penalty_factor => R, 
                        :conv_iter => Kmax,
                        :social_welfare => SW
                        )

        # Plotting results
        data = GenericTrace[]
        trace = scatter(Df, x=:penalty_factor, y=:conv_iter,
            name = "Convergence iteration", 
            yaxis = "y", 
            showlegend = false
            )
        push!(data, trace)
        trace = scatter(Df, x=:penalty_factor, y=:social_welfare,
            name = "Social welfare", 
            yaxis = "y2",
            showlegend = false
            )
        push!(data, trace)

        layout = Layout(
                    xaxis = attr(
                        title = "penalty factor ρ",
                        ),
                    yaxis = attr(
                        title="Conv Iter",
                        titlefont=attr(
                            color="#1f77b4"
                            ),
                        tickfont=attr(
                            color="#1f77b4"
                            ),
                        ),
                    yaxis2 = attr(
                        titlefont=attr(
                            color="#ff7f0e"
                            ),
                        tickfont=attr(
                            color="#ff7f0e"
                            ),
                        title = "SW",
                        overlaying = "y",
                        side = "right",
                        anchor = "x"
                        )
                )
        display(plot(data,layout))

        Df
    end

end
