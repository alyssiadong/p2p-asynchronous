### Quantile regression test

using PlotlyJS
using ORCA
using JLD, HDF5
using DataFrames
using Statistics
using Printf
using StatsBase

include("write_results.jl")

JLDfile = WR.JLDFile( "n110_p30_c80_0x000f_all.jld",
        "D:/Julia/n110_p30_c80_0x000f_all.jld",
        "n110_p30_c80_0x000f.csv"
        )

df = JLDfile.df_params
sigmas = unique(df.sigma)
sig = [sigmas[1], sigmas[4], sigmas[7], sigmas[10]]
deltas = unique(df.delta)
df = df[(df.delta .== deltas[2]) .& (df.sigma .== sigmas[1]) .& (df.tirages .==1),:]

Df = WR.DfSweepTradewiseConvergence(JLDfile, 1e-9, df)
Df.log_trade_res = log10.(Df.trade_res)

# Bin separation
Nbin = 7
dist_min = 0.0
dist_max = 2.5
dist_range = range(dist_min, dist_max, length = Nbin+1)
df_array = map(x->Df[(Df.distance .>= x[1]) .& (Df.distance .<x[2]),:],
	zip(dist_range[1:end-1],dist_range[2:end])) #Map an anonymous function that gets every row between two numbers specified by a tuple called x, and map that anonymous function to an array of tuples generated using the zip function.

# Quantiles 
Nquant = 9		# number of printed quantiles
quantRange = range(0,1,length=Nquant)
df1 = DataFrame(:quantile => quantRange)
df2 = DataFrame(:bin_number => 1:Nbin)
Df_quant = join(df1, df2, kind = :cross, makeunique = true)
Df_quant.bin_meandist = (dist_range[Df_quant.bin_number]+dist_range[Df_quant.bin_number.+1])/2
Df_quant.trade_res_quant = zeros(size(Df_quant,1))


i = 1
for df0 in df_array
	global i
	for j in 1:length(quantRange)
		quant = quantRange[j]
		q_res = quantile(df0.log_trade_res, quant)
		Df_quant[(Df_quant.bin_number .== i) .& (Df_quant.quantile .== quant),:trade_res_quant] .= q_res
	end
	i = i + 1
end

# Plot
data = GenericTrace[]
for quant in quantRange[2:end-1]
	df = Df_quant[Df_quant.quantile.==quant,:]
	trace = scatter(df, 
					x = :bin_meandist, 
					y = :trade_res_quant,
					name = string("quantile : ", @sprintf("%.3f",quant))
					)
	push!(data, trace)
end
plot(data)