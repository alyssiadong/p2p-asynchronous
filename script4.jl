##### Test pour comparer les résultats de calcul avec des solvers différents 'OSQP'/'JuMP'

include("PowerNetwork.jl")
include("CommunicationDelays.jl")
include("market_sim.jl")

N_prodcons = [10,21]
type = "full_P2P"

par = PowerNetwork.power_network("n31_p10_c21_0xeeee.csv", type)
rho = par.preferred_rho	
ΔT = 8000.0
sendType = "sendback"
iterMatching = "yesmatching"
priceUpdate = "new"
tradeUpdate = "new"
δt = 5
Tsimu = 1500
OSQPeps=1e-5

α = 5.0
β = 1.0
σ = 0.0
gamma = 1
solver = "OSQP"
delta = 1.0
tirage = 1

Ntrig = vcat(fill(ceil(Int,N_prodcons[2]*delta), N_prodcons[1]), 
		fill(ceil(Int,N_prodcons[1]*delta), N_prodcons[2]))

Tmax = fill(ΔT, sum(N_prodcons))

aPar = MarketSimulation.algoParams(
		rho, gamma, OSQPeps, solver, Ntrig, Tmax, sendType, 
		# rho, gamma, OSQPeps, Ntrig, Tmax, sendType, 
		iterMatching, priceUpdate, tradeUpdate)
nPar = CommunicationDelays.commParams(α, β, σ)

mem2 = MarketSimulation.run_simulation(Tsimu, δt, par, nPar, aPar)

