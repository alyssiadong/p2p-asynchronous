module Cartographie
### Save results from the 'MarketSimulation' module ('market_sim.jl').

	export carto_asyn, display_comparison
	using HDF5
	using DataFrames, Statistics
	using ..PowerNetwork
	using ..CommunicationDelays
	using ..MarketSimulation


################################################################################
# Save results
################################################################################

	function carto(filepath::String, par::PowerNetwork.power_network, 
	aPar::MarketSimulation.algoParams)

		# Market simulation parameters
		N_Ω = length(par.Ω)

		# Creating hdf5 file
		fid = h5open(filepath, "cw")
		g = g_create(fid , string(powNetType, "/",
			Ntrig_central,"/algoParams"))

		N_points = 30

		A = range(0,30, length=N_points)
		B = range(0,10, length=N_points)

		write(g, "penalty_factor", aPar.ρ)
		write(g, "N_trig", aPar.Ntrig)
		write(g, "T_max", aPar.Tmax)
		write(g, "sendType", aPar.sendType)
		write(g, "iterMatching", aPar.iterMatching)
		write(g, "priceUpdate", aPar.priceUpdate)
		write(g, "tradeUpdate", aPar.tradeUpdate)

		write(g, "A", collect(A))
		write(g, "B", collect(B))
		close(fid)

		for a∈eachindex(A), b∈eachindex(B)
			(a==1) && (b==1) && continue
			println("	a =$a, b=$b")

			α, β = A[a], B[b]

			# Communication model
			comParam = CommunicationDelays.commParams(α, β)

			# Market simulation
			try 
				mem = Market.run_simulation(1000.0, 2.0, 
					par, comParam, aPar)

				social_welfare = dropdims(sum(abs.(mem.trade_array[1:31,:,:]).*mem.prices_array[1:31,:,:], dims=(1,2)), dims=(1,2))

				# Saving results
				fid = h5open(filepath, "cw")
				filename = string("alpha",a, "_beta", b)
				try
					g = g_create(fid,filename)

					g["prices" , "shuffle", (), "deflate", 3] =  mem.prices_array
					g["power" , "shuffle", (), "deflate", 3] =  mem.power_array
					g["trades" , "shuffle", (), "deflate", 3] =  mem.trade_array
					g["length_mailbox" , "shuffle", (), "deflate", 3] =  mem.length_mailbox_array
					g["primal_res" , "shuffle", (), "deflate", 3] =  mem.primal_res_array
					g["dual_res" , "shuffle", (), "deflate", 3] =  mem.dual_res_array
					g["soc_welfare", "shuffle", (), "deflate", 3]= social_welfare
					g["message", , "shuffle", (), "deflate", 3]= mem.message_array
					close(fid)
				catch exc
					close(fid)
					rethrow(exc)
				end
			catch
				println("	Warning : error thrown.")
			end
		end

		println("Computing Conv sweep...")
		compute_conv_sweep(powNetType, filepath, 5.0, Ntrig_central)
	end

	function carto_asyn_p2p(powNetType::String, file::String, mes_density::Float64)

		@assert powNetType ∈ ["P2P"] "'powNetType' must be 'P2P'."

		# Power network parameters
		pow_network = PowerNetwork.power_network(powNetType)

		# Market simulation parameters
		N_Ω = length(pow_network.Ω)
		ρ = 0.3
		Tmax = fill(500.0, N_Ω)
		Ntrig = vcat(fill(Int(round(10*mes_density)),21), 
				fill(Int(round(10*mes_density)),10))

		sendType = "sendback"
		iterMatching = "yesmatching"
		priceUpdate = "new"
		tradeUpdate = "new"

		algoPar = Market.algoParams(ρ, Ntrig, Tmax,
			sendType, iterMatching, priceUpdate, tradeUpdate)

		# Creating hdf5 file
		fid = h5open(file, "cw")
		g = g_create(fid , string(powNetType, "/",
			string(mes_density),"/algoParams"))

		N_points = 30

		A = range(0,30, length=N_points)
		B = range(0,10, length=N_points)
		σ = 0.0

		write(g, "penalty_factor", ρ)
		write(g, "N_trig", Ntrig)
		write(g, "T_max", Tmax)
		write(g, "sendType", sendType)
		write(g, "iterMatching", iterMatching)
		write(g, "priceUpdate", priceUpdate)
		write(g, "tradeUpdate", tradeUpdate)

		write(g, "A", collect(A))
		write(g, "B", collect(B))
		close(fid)

		for a∈eachindex(A), b∈eachindex(B)
			(a==1) && (b==1) && continue
			println("	a =$a, b=$b")

			α, β = A[a], B[b]

			# Communication model
			comParam = CommunicationDelays.commParams(
			    α, β, σ, "Sto")

			# Market simulation 
			try 
				mem = Market.run_simulation(1000.0, 2.0, 
					pow_network, comParam, algoPar)

				social_welfare = dropdims(sum(abs.(mem.trade_array[1:31,:,:]).*mem.prices_array[1:31,:,:], dims=(1,2)), dims=(1,2))

				# Saving results
				fid = h5open(file, "cw")
				filename = string(powNetType, "/",string(mes_density),
					"/alpha",a, "_beta", b)
				try
					g = g_create(fid,filename)

					g["prices" , "shuffle", (), "deflate", 3] =  mem.prices_array
					g["power" , "shuffle", (), "deflate", 3] =  mem.power_array
					g["trades" , "shuffle", (), "deflate", 3] =  mem.trade_array
					g["length_mailbox" , "shuffle", (), "deflate", 3] =  mem.length_mailbox_array
					g["primal_res" , "shuffle", (), "deflate", 3] =  mem.primal_res_array
					g["dual_res" , "shuffle", (), "deflate", 3] =  mem.dual_res_array
					g["soc_welfare", "shuffle", (), "deflate", 3]= social_welfare
					close(fid)
				catch exc
					close(fid)
					rethrow(exc)
				end
			catch
				println("	Warning : error thrown.")	
			end
		end
	end

	function carto_asyn_syn(powNetType::String, ΔT::Float64, file::String)
		### !!! Toujours vérifier le nom du fichier avant de lancer le programme !!!
		### ΔT doit avoir l'écriture la plus compacte possible (ex : 0.5, 1.25, 5.0)

		@assert powNetType ∈ ["central", "P2P"] "Uncorrect type for 'powNetType'."

		# Power network parameters
		pow_network = PowerNetwork.power_network(powNetType)

		# Market simulation parameters
		N_Ω = length(pow_network.Ω)
		ρ = 0.1
		Tmax = vcat(fill(1000.0, N_Ω-1), ΔT)
		Ntrig = vcat(fill(1,31), 1000)

		N_points = 30
		A = range(0,30, length=N_points)
		B = range(0,10, length=N_points)
		σ = 0.0

		sendType = "sendback"
		iterMatching = "yesmatching"
		priceUpdate = "new"
		tradeUpdate = "global"

		algoPar = Market.algoParams(ρ, Ntrig, Tmax,
			sendType, iterMatching, priceUpdate, tradeUpdate)

		# Creating hdf5 file
		fid = h5open(file, "cw")
		g = g_create(fid , string(powNetType, "/", ΔT,"/algoParams"))

		# N_points = 50

		A = range(0,30, length=N_points)
		B = range(0,10, length=N_points)
		σ = 0.0

		write(g, "penalty_factor", ρ)
		write(g, "N_trig", Ntrig)
		write(g, "T_max", Tmax)

		write(g, "A", collect(A))
		write(g, "B", collect(B))
		write(g, "sendType", sendType)
		write(g, "iterMatching", iterMatching)
		write(g, "priceUpdate", priceUpdate)
		write(g, "tradeUpdate", tradeUpdate)
		close(fid)

		for a∈eachindex(A), b∈eachindex(B)
			α, β = A[a], B[b]
			(a==1) && (b==1) && continue

			# Communication model
			comParam = CommunicationDelays.commParams(
			    α, β, σ, "Sto")

			# Market simulation 
			try 
				fid = h5open(file, "cw")
				filename = string(powNetType, "/", ΔT,
					"/alpha",a, "_beta", b)

				if !exists(fid, filename)
					println("	a =$a, b=$b")
					mem = Market.run_simulation(1000.0, 2.0, 
						pow_network, comParam, algoPar)

					social_welfare = dropdims(sum(
						abs.(mem.trade_array[1:31,:,:]).*mem.prices_array[1:31,:,:], dims=(1,2)), dims=(1,2))

					# Saving results
					try
						g = g_create(fid,filename)

						g["prices" , "shuffle", (), "deflate", 3] =  mem.prices_array
						g["power" , "shuffle", (), "deflate", 3] =  mem.power_array
						g["trades" , "shuffle", (), "deflate", 3] =  mem.trade_array
						g["length_mailbox" , "shuffle", (), "deflate", 3] =  mem.length_mailbox_array
						g["primal_res" , "shuffle", (), "deflate", 3] =  mem.primal_res_array
						g["dual_res" , "shuffle", (), "deflate", 3] =  mem.dual_res_array
						g["soc_welfare", "shuffle", (), "deflate", 3]= social_welfare   
					catch ex
						close(fid)
						rethrow(ex)
					finally 
						close(fid)
					end
				end
			catch
				println("	Warning : simulation error thrown.")	
			end
		end

		println("	Computing Conv Sweep...")
		compute_conv_sweep(powNetType, file, 5.0, ΔT)
	end

################################################################################
# Approximate and compute results
################################################################################
	
	# Central, asynchronous (Ntrig sweep) 
	function compute_conv_sweep(powNetType::String, file::String, Eprim::Number, Ntrig::Int)

		@assert (file ∈ ["market_central_asyn.h5", 
				"market_central_asyn2.h5", "market_central_ntrig.h5", 
				"market_central_ntrig2.h5", "market_central_ntrig3.h5",
				"market_central_ntrig4.h5"]) "Uncorrect file name."

		# Read results
		fid = h5open(file, "r")
		name = string(powNetType, "/", Ntrig)

		g = fid[name]
		g1 = g["algoParams"]

		A = read(g1, "A")
		B = read(g1, "B")

		# Compute convergence time
		Doz = DataFrame(A = Float64[], B = Float64[], Conv = Int64[])
		Conv = 500
		for a∈eachindex(A), b∈eachindex(B)
			name2 = string("alpha", a, "_beta", b)
			if exists(g, name2)
				g1 = g[name2]

				primal_res = read(g1, "primal_res")
				dual_res = read(g1, "dual_res")

				primal_res_tot = sum(primal_res, dims=1)[2:end]
				dual_res_tot = sum(dual_res, dims=1)[2:end]
				try
					Conv = maximum([i for i=1:length(primal_res_tot) 
						if primal_res_tot[i]>Eprim])
				catch
					Conv = 500
				end
				push!(Doz, (A[a],B[b],Conv))
			end
		end
		close(fid)

		# Write results back in file
		fid = h5open(file, "cw")
		g = fid[name]

		if exists(g, "conv_prim")
			g1 = g["conv_prim"]
			Eprims = read(g1, "Eprims")

			Ns = findall(x-> x==Eprim, Eprims)

			if length(Ns)≠0
				try
					N = Ns[1]
					name_conv = string("Conv", N)
					o_delete(g1, name_conv)

					g2 = g_create(g1, name_conv)
					g2["A", "shuffle", (), "deflate", 3] = Doz[:A]
					g2["B", "shuffle", (), "deflate", 3] = Doz[:B]
					g2["Conv", "shuffle", (), "deflate", 3] = Doz[:Conv]
					g2["Eprim"] = Eprim
				finally
					close(fid)
				end
			else
				try 
					N = length(Eprims) + 1
					push!(Eprims, Eprim)

					o_delete(g1, "Eprims")
					g1["Eprims"] = Eprims

					name2 = string("Conv",N)
					g2 = g_create(g1, name2)
					g2["A", "shuffle", (), "deflate", 3] = Doz[:A]
					g2["B", "shuffle", (), "deflate", 3] = Doz[:B]
					g2["Conv", "shuffle", (), "deflate", 3] = Doz[:Conv]
					g2["Eprim"] = Eprim
				catch ex
					rethrow(ex)
				finally
					close(fid)
				end
			end
		else
			try 
				g1 = g_create(g, "conv_prim")
				g1["Eprims"] = [ Eprim ]

				g2 = g_create(g1, "Conv1")
				g2["A", "shuffle", (), "deflate", 3] = Doz[:A]
				g2["B", "shuffle", (), "deflate", 3] = Doz[:B]
				g2["Conv", "shuffle", (), "deflate", 3] = Doz[:Conv]
				g2["Eprim"] = Eprim
			catch ex 
				rethrow(ex)
			finally 
				close(fid)
			end
		end
		close(fid)

		return Doz
	end

	# function compute_conv_sweep(powNetType::String, Eprim::Number, Ntrig::Int) # VERY SLOW

		# 	# Sweeping parameters
		# 	fid = h5open("central_totally_asynchronous/algoParams.h5", "r")
		# 	A = read(fid, "A")
		# 	B = read(fid, "B")
		# 	close(fid)

		# 	folder_name = string("central_totally_asynchronous/Ntrig", Ntrig)

		# 	# Compute convergence time
		# 	Doz = DataFrame(A = Float64[], B = Float64[], Conv = Int64[])
		# 	Conv = 500
		# 	for a∈eachindex(A), b∈eachindex(B)
		# 		file_name = string(folder_name, "/alpha", a, "_beta", b, ".h5")
		# 		if isfile(file_name)
		# 			fid = h5open(file_name, "r")

		# 			primal_res = read(fid, "primal_res")
		# 			dual_res = read(fid, "dual_res")

		# 			close(fid)

		# 			primal_res_tot = sum(primal_res, dims=1)[2:end]
		# 			dual_res_tot = sum(dual_res, dims=1)[2:end]
		# 			try
		# 				Conv = maximum([i for i=1:length(primal_res_tot) 
		# 					if primal_res_tot[i]>Eprim])
		# 			catch
		# 				Conv = 500
		# 			end
		# 			push!(Doz, (A[a],B[b],Conv))
		# 		end
		# 	end

		# 	# Write results back in file
		# 	file_name = string(folder_name, "/conv_prim/Eprim_list.h5")		
		# 	fid = h5open(file_name, "cw")

		# 	d = read(fid)
		# 	N = length(d)
		# 	Keys = [k for (k,v) in d if v==Eprim]

		# 	if length(Keys) == 0
		# 		fid[string(N+1)] = Eprim
		# 		close(fid)
		# 		key = string(N+1)

		# 		file_name = string(folder_name,"/conv_prim/Eprim", key, ".h5")
		# 		fid = h5open(file_name, "w")

		# 		fid["A", "shuffle", (), "deflate", 3] = Doz[:A]
		# 		fid["B", "shuffle", (), "deflate", 3] = Doz[:B]
		# 		fid["Conv", "shuffle", (), "deflate", 3] = Doz[:Conv]
		# 		fid["Eprim"] = Eprim

		# 		close(fid)
		# 	else
		# 		key = Keys[1]
		# 		close(fid)

		# 		# nothing
		# 	end

		# 	return Doz
		# end

	# Central, asynchronous, partially synchronous or p2p Ntrig
	function compute_conv_sweep(powNetType::String, file::String, Eprim::Number, ΔT::Float64)

		@assert file ∈ ["market_asyn_syn.h5", "market_asyn_syn2.h5", 
					"market_p2p.h5", "market_asyn_syn3.h5", "market_asyn_syn4.h5"] "Uncorrect file name."

		# Read results
		fid = h5open(file, "r")

		g = fid[string(powNetType, "/",ΔT)]
		g1 = g["algoParams"]

		A = read(g1, "A")
		B = read(g1, "B")

		# Compute convergence time
		Doz = DataFrame(A = Float64[], B = Float64[], Conv = Int64[])
		Conv = 500
		for a∈eachindex(A), b∈eachindex(B)
			name2 = string("alpha", a, "_beta", b)
			if exists(g, name2)
				g1 = g[name2]

				primal_res = read(g1, "primal_res")
				dual_res = read(g1, "dual_res")

				primal_res_tot = sum(primal_res, dims=1)[2:end]
				dual_res_tot = sum(dual_res, dims=1)[2:end]
				try
					Conv = maximum([i for i=1:length(primal_res_tot) 
						if primal_res_tot[i]>Eprim])
				catch
					Conv = 500
				end
				push!(Doz, (A[a],B[b],Conv))
			end
		end
		close(fid)

		# Write results back in file
		fid = h5open(file, "cw")
		g = fid[string(powNetType, "/",ΔT)]

		if exists(g, "conv_prim")
			g1 = g["conv_prim"]
			Eprims = read(g1, "Eprims")

			Ns = findall(x-> x==Eprim, Eprims)

			if length(Ns)≠0 # already computed
				# try
				# 	N = Ns[1]
				# 	name_conv = string("Conv", N)
				# 	o_delete(g1, name_conv)

				# 	g2 = g_create(g1, name_conv)
				# 	g2["A", "shuffle", (), "deflate", 3] = Doz[:A]
				# 	g2["B", "shuffle", (), "deflate", 3] = Doz[:B]
				# 	g2["Conv", "shuffle", (), "deflate", 3] = Doz[:Conv]
				# 	g2["Eprim"] = Eprim
				# finally
				# 	close(fid)
				# end

			else
				try 
					N = length(Eprims) + 1
					push!(Eprims, Eprim)

					o_delete(g1, "Eprims")
					g1["Eprims"] = Eprims

					name2 = string("Conv",N)
					g2 = g_create(g1, name2)
					g2["A", "shuffle", (), "deflate", 3] = Doz[:A]
					g2["B", "shuffle", (), "deflate", 3] = Doz[:B]
					g2["Conv", "shuffle", (), "deflate", 3] = Doz[:Conv]
					g2["Eprim"] = Eprim
				finally
					close(fid)
				end
			end
		else
			try 
				g1 = g_create(g, "conv_prim")
				g1["Eprims"] = [ Eprim ]

				g2 = g_create(g1, "Conv1")
				g2["A", "shuffle", (), "deflate", 3] = Doz[:A]
				g2["B", "shuffle", (), "deflate", 3] = Doz[:B]
				g2["Conv", "shuffle", (), "deflate", 3] = Doz[:Conv]
				g2["Eprim"] = Eprim
			finally 
				close(fid)
			end
		end
		close(fid)

		return Doz
	end

	# Central, asynchronous, partially synchronous
	function compute_social_welfare(powNetType::String, file::String, ΔT::Float64)

		@assert file ∈ ["market_asyn_syn.h5", "market_asyn_syn2.h5"] "Uncorrect filename."

		# Read results
		fid = h5open(file, "r")

		g = fid[string(powNetType, "/",ΔT)]
		g1 = g["algoParams"]

		A = read(g1, "A")
		B = read(g1, "B")

		close(fid)

		# Compute social welfare
		for a∈eachindex(A), b∈eachindex(B)
			(a==1) && (b==1) && continue
			(a%5==0) && (b==1) && println("a=$a")
			trades = read_res(powNetType, ΔT, a,b, "trades")[1:31, :, :]
			prices = read_res(powNetType, ΔT, a,b, "prices")[1:31, :, :]

			social_welfare = dropdims(sum(abs.(trades).*prices, dims=(1,2)), dims=(1,2))

			fid = h5open(file, "cw")
			g = fid[string(powNetType, "/", ΔT, "/alpha", a, "_beta", b)]

			try 
				# o_delete(g, "social_welfare")
				o_delete(g, "soc_welfare")
			catch ex
				close(fid)
				rethrow(ex)
			end

			try 
				g = fid[string(powNetType, "/", ΔT, "/alpha", a, "_beta", b)]
				g["soc_welfare", "shuffle", (), "deflate", 3]= social_welfare
			catch ex
				close(fid)
				rethrow(ex)
			end
			# (a%5==0) && (b==1) && display(names(g))
			close(fid)
		end
	end

	# Central, asynchronous (Ntrig sweep)
	function compute_social_welfare(powNetType::String, file::String, Ntrig::Int)
		@assert file ∈ ["market_central_asyn.h5", 
		"market_central_asyn2.h5", "market_central_ntrig.h5"] "Uncorrect file name."

		# Read resultsu
		fid = h5open(file, "r")

		g = fid[string(powNetType, "/",Ntrig)]
		g1 = g["algoParams"]

		A = read(g1, "A")
		B = read(g1, "B")

		close(fid)

		# Compute social welfare
		for a∈eachindex(A), b∈eachindex(B)
			(a==1) && (b==1) && continue
			(a%5==0) && (b==1) && println("a=$a")
			trades = read_res(powNetType, Ntrig, a,b, "trades")[1:31, :, :]
			prices = read_res(powNetType, Ntrig, a,b, "prices")[1:31, :, :]

			social_welfare = dropdims(sum(abs.(trades).*prices, dims=(1,2)), dims=(1,2))

			fid = h5open(file, "cw")
			g = fid[string(powNetType, "/", Ntrig, "/alpha", a, "_beta", b)]

			# try 
			# 	o_delete(g, "social_welfare")
			# 	o_delete(g, "soc_welfare")
			# catch ex
			# 	close(fid)
			# 	rethrow(ex)
			# end

			try 
				# g = fid[string(powNetType, "/", Ntrig, "/alpha", a, "_beta", b)]
				g["soc_welfare", "shuffle", (), "deflate", 3]= social_welfare
			catch ex
				close(fid)
				rethrow(ex)
			end
			# (a%5==0) && (b==1) && display(names(g))
			close(fid)
		end
	end


end

