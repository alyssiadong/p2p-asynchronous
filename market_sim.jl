module MarketSimulation
### Code for simulating the asynchronous 'prosumer market'.
### This version is specific to the case where each agent only 
### has one asset.

    export run_simulation, algoParams

    using OSQP
    # using JuMP, SCS
    # using MathOptInterface
    # const MOI = MathOptInterface
    # const MOIU = MOI.Utilities
    using LinearAlgebra, SparseArrays
    using ResumableFunctions, SimJulia
    using HDF5
    using LightGraphs, SimpleWeightedGraphs
    using DataFrames
    using ..PowerNetwork
    using ..CommunicationDelays
    using ..Planification

    import Base.show
    export show

################################################################################
# Power network parameters and topology
################################################################################
    ### Notes on what is inside a power_network structure
    # assets= Array{Int}([])          # assets[n] List of all assets
    #                                 # cost function : fn(pn) = an*pn^2 + bn*pn
    #                                 #   s.t. Pmin_n ≤ pn ≤ Pmax_n
    # gencost_a= Array{Float64}([])   # gencost_a[n] = an
    # gencost_b= Array{Float64}([])   # gencost_b[n] = bn
    # Pmin= Array{Float64}([])        # Pmin[n] = Pmin_n
    # Pmax= Array{Float64}([])        # Pmax[n] = Pmax_n
    # loc= Array{Float64}([])         # loc[n, 1:2] = x and y position of asset n
    #
    # Ω = Array{Int}([])              # Ω[:] Set of market participants
    # A = Vector{Vector{Int}}([])     # A[i][:] Available assets of market participant i
    # comm_graph = SimpleWeightedGraph      # Graph of all commercial links between agents, weighted 
                                            # by the distance between those links

################################################################################
# ADMM algorithm parameters, exceptions, information structures
################################################################################

    struct algoParams
        ρ::Float64              # ADMM penalty factor
        γ::Number
        OSQPeps::Number
        solver::String          # "OSQP" or "Convex" or "JuMP"
        N_trig::Array{Int64}    # Trigger parameters
        T_max::Array{Float64}           
        sendType::String        # "sendback" or "broadcast"
        iterMatching::String    # "yesmatching" or "nomatching"
        priceUpdate::String     # "global" or "new"
        tradeUpdate::String     # "global" or "new"
        learningAgents::Array{Bool}
        Q0::Array{DataFrame}    # Warm start Q LUT

        # Constructor
        function algoParams(ρ::Number, γ::Number, OSQPeps::Number, solver::String, 
            N_trig::Array{Int64}, T_max::Array{Float64},
            sendType::String, iterMatching::String, priceUpdate::String, tradeUpdate::String, 
            learningAgents::Array{Bool}, Q0=[DataFrame()]::Array{DataFrame})

            @assert sendType ∈ ["broadcast", "sendback"] "sendType uncorrect."
            @assert iterMatching ∈ ["nomatching", "yesmatching"] "iterMatching uncorrect."
            @assert priceUpdate ∈ ["global", "new"] "priceUpdate uncorrect."
            @assert tradeUpdate ∈ ["global", "new"] "tradeUpdate uncorrect."
            @assert length(learningAgents) == length(N_trig) "Unmatching dimensions."

            @assert ! ((iterMatching == "yesmatching") &&
                        (sendType == "broadcast"))  "Impossible combination."
            # @assert ! ( (varUpdate == "new") &&
            #             ( sendType== "broadcast")) "Impossible combination."
            @assert solver ∈ ["OSQP", "Convex", "JuMP"] "Wrong solver."

            new(float(ρ), γ, OSQPeps, solver, N_trig, T_max, sendType, 
                iterMatching, priceUpdate, tradeUpdate, learningAgents, Q0)
        end
    end
    _unpack(x::algoParams) = (x.ρ, x.γ, x.OSQPeps, x.solver, x.sendType,
     x.iterMatching, x.priceUpdate, x.tradeUpdate)

    struct UnconsistentPartner <: Exception end
    struct UnconsistentIteration <: Exception end

    struct OptimizationError <: Exception 
        errorType::Symbol
    end

################################################################################
# Markov Decision Process for agent trigger
################################################################################

    struct MDP{T<:String}
        S::Array{<:Any,1}           # states
        A::Array{T,1}               # actions
        P::Dict{T, AbstractSparseMatrix{<:Number,Int64}}      # transition probability given action a  P[a][s,s'] = P(s'|s,a)
        R::Dict{T, AbstractSparseVector{<:Number,Int64}}      # expected reward R[a][s] = R(s,a)

        function MDP{T}(S::Array{<:Any,1},A::Array{T,1},P::Dict{T, <:AbstractSparseMatrix{<:Number,Int64}}) where T<:String
            @assert allunique(S)  "State vector must contain unique elements only."
            @assert allunique(A)  "Action vector must contain unique elements only."
            @assert length(collect(keys(P))) == length(A) "Number of actions from A and P must match."
            @assert Set(collect(keys(P))) == Set(A) "Actions from A and P must match."

            for (key, value) in P
                @assert all(sum(value,dims=2) .== 1) "Sum of probabilities over second dimension must be equal to 1."
            end

            # println("Initializing reward as zero...")
            R = Dict(
                    k => spzeros(length(S)) for k in keys(P)
                )

            new(S,A,P,R)
        end
    end

    MDP(S::Array{<:Any,1},A::Array{T,1},P::Dict{T, <:AbstractSparseMatrix{<:Number,Int64}}) where {T<:String} = MDP{T}(S,A,P)

    mutable struct MessageQueueMDP
        mdp::MDP
        current_state::Any
        next_state::Any##
        next_action::String
        previous_state::Any
        previous_action::String
        N::Dict{String,Array{Int64,1}}          # N[a][s] = N(s,a) number of times from state s given action a
        Q::DataFrame                            # Q - s | a | q(s,a)
        policy::Dict{String,Array{<:Number,1}}  # policy[a][s] = π(a|s)
        ϵ::Number
        γ::Number
        α::Number
        learning_agent::Bool

        function MessageQueueMDP(S::Array{<:Any,1},A::Array{String,1},
            P::Dict{String, <:AbstractSparseMatrix{<:Number,Int64}}, s0::Any, 
            learning_agent::Bool, Q0::DataFrame)
            @assert s0 ∈ S "Initial state must be part of MDP states."

            mdp = MDP(S, A, P)
            N = Dict(
                    k => zeros(Int64, length(S)) for k in mdp.A
                )
            policy = Dict(
                    k => zeros(Float64, length(S)) for k in mdp.A
                )
            ϵ = 0.2
            γ = 0.5
            α = 0.2
            new(mdp, s0, nothing,"wait", nothing, "wait" ,N, Q0, policy, ϵ, γ, α, learning_agent)
        end

        function MessageQueueMDP(N_ωi::Int64, learning_agent::Bool; Q0=DataFrame(S = Any[], A = String[], Q = Number[], N=Int[])::DataFrame)
            @assert N_ωi > 1 "N_ωi must be strictly greater than one."

            # Classic case only taking into account the number of messages in the agent's queue
            S = collect(0:N_ωi)
            s0 = 0
            A = ["wait", "trigger"]
            P = Dict(
                    "wait" => sparse(1:N_ωi+1, vcat(2:N_ωi+1, 1), ones(N_ωi+1), N_ωi+1, N_ωi+1),
                    "trigger" => sparse(1:N_ωi+1, ones(Int64, N_ωi+1), ones(N_ωi+1), N_ωi+1, N_ωi+1),
            )
            # Initializing Q to 0 if empty
            if size(Q0,1)==0
                for (s,a) in Iterators.product(S, A)
                    push!(Q0, [s,a,0,0])
                end
            end

            MessageQueueMDP(S,A,P,s0, learning_agent, Q0)
        end
    end

    function setState!(mmdp::MessageQueueMDP, state::Any)
        @assert state ∈ mmdp.mdp.S     "Unvalid state."

        mmdp.current_state = state
    end

    function setState!(mmdp::MessageQueueMDP, state_in::Any, state_out::Any, action::String)
        @assert state_in ∈ mmdp.mdp.S     "Unvalid state."
        @assert state_in == mmdp.current_state "State_in is not current state."
        @assert action ∈ mmdp.mdp.A     "Unvalid action."

        mmdp.previous_state = mmdp.current_state
        mmdp.previous_action = action
        setState!(mmdp, state_out)
        mmdp.N[action][findfirst(mmdp.mdp.S.==state_in)] = mmdp.N[action][findfirst(mmdp.mdp.S.==state_in)] + 1
    end

    function setState!(mmdp::MessageQueueMDP, state_in::Any, state_out::Any, action::String, reward::Number)
        setState!(mmdp, state_in, state_out, action)
        updateReward!(mmdp, state_in, action, reward)
    end

    function updateReward!(mmdp::MessageQueueMDP, s::Any, a::T, r::Number) where {T<:String}
        @assert a ∈ mmdp.mdp.A "Unvalid action."
        @assert s ∈ mmdp.mdp.S "Unvalid state."

        ks = findfirst(mmdp.mdp.S .== s)
        rPrev = mmdp.mdp.R[a][ks]
        N = mmdp.N[a][ks]
        if N != 0
            mmdp.mdp.R[a][ks] = rPrev + 1/N*(r-rPrev)
        else
            throw(ErrorException("Cannot divide by 0."))
        end

        mmdp.mdp.R[a][ks]
    end

    function updateSARSA!(mmdp::MessageQueueMDP, s::Any, sp::Any, a::T, ap::T, r::Number) where {T<:String}
        @assert a ∈ mmdp.mdp.A "Unvalid action."
        @assert s ∈ mmdp.mdp.S "Unvalid state."
        @assert ap ∈ mmdp.mdp.A "Unvalid action."
        @assert sp ∈ mmdp.mdp.S "Unvalid state."

        # Updating Q LUT
        q = mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:Q][1]
        qp = mmdp.Q[(mmdp.Q.A .== ap) .& (mmdp.Q.S .== sp),:Q][1] 

        if mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:N] == 0     # first time visiting state-action 
            if a=="wait"
                mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:Q] .= r + mmdp.γ*qp
            else # a == "trigger"
                mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:Q] .= r
            end
        else
            if a=="wait"
                mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:Q] .= q + mmdp.α*(r + mmdp.γ*qp - q)
            else # a == "trigger"
                mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:Q] .= q + mmdp.α*(r - q)
            end
        end
        mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:N] = mmdp.Q[(mmdp.Q.A .== a) .& (mmdp.Q.S .== s),:N] .+ 1
    end

    function updateReward!(mmdp::MessageQueueMDP, r::Number)
        s = mmdp.previous_state
        a = mmdp.previous_action
        updateReward!(mmdp, s, a, r)
    end

    function updateSARSA!(mmdp::MessageQueueMDP, r::Number)
        s = mmdp.previous_state
        a = mmdp.previous_action
        sp = mmdp.current_state
        ap = mmdp.next_action
        updateSARSA!(mmdp, s, sp, a, ap, r)
    end

    function setPolicy!(mmdp::MessageQueueMDP, pol::Dict{String,<:Array{<:Number,1}})
        @assert Set(collect(keys(pol))) == Set(mmdp.mdp.A) "Unmatching actions."
        @assert all(sum(hcat([pol[k] for k in keys(pol)]...),dims=2) .==1) "Sum of probabilities must be equal to 1."
        for (k,v) in pol
            @assert length(v) == length(mmdp.mdp.S) "Unmatching state dimension."
            @assert all(0 .<= v .<= 1) "Probability must be between 0 and 1."
        end
        mmdp.policy = pol
    end

    function setQTable!(mmdp::MessageQueueMDP, Q0::DataFrame)
        @assert unique(Q0.A) ⊆ mmdp.mdp.A "Unvalid actions in Q0."
        @assert unique(Q0.S) ⊆ mmdp.mdp.S "Unvalid states in Q0."

        mmdp.Q = Q0
    end

    function takeAction(mmdp::MessageQueueMDP)
        if mmdp.learning_agent
            action = takeActionGreedy(mmdp)
        else
            action = takeActionFixedPolicy(mmdp)
        end

        if (mmdp.current_state == maximum(mmdp.mdp.S)) & (action == "wait")
            action = "trigger"
        elseif (mmdp.current_state == 0) & (action == "trigger")
            action = "wait"
        end

        action
    end

    function takeActionFixedPolicy(mmdp::MessageQueueMDP)
        # Takes action according to policy by sampling 
        s0 = mmdp.current_state
        ind = findfirst(mmdp.mdp.S .== s0)
        cumprob = cumsum([ v[ind] for (k,v) in mmdp.policy])

        ind2 = findfirst(rand() .< cumprob)
        action = mmdp.mdp.A[ind2]

        # println("Action : ", action)

        action
    end

    function takeActionGreedy(mmdp::MessageQueueMDP)
        # Takes action according to policy by sampling 
        s0 = mmdp.current_state
        ind = findfirst(mmdp.mdp.S .== s0)

        # ϵ-greedy policy
        if rand()>mmdp.ϵ/2
            df = mmdp.Q[mmdp.Q.S .== s0,:]
            # display(mmdp.Q)
            # display(s0)
            # display(df)
            action = df[df.Q .== maximum(df.Q), :A][1]
        else
            action = rand(["wait", "trigger"])
        end

        # println("Greedy action : ", action, " at state ", s0)

        action
    end

    function nextState(mmdp::MessageQueueMDP, action::String)
        # Takes action according to policy by sampling 
        s0 = mmdp.current_state
        ind = findfirst(mmdp.mdp.S .== s0)
        cumprob = cumsum([mmdp.mdp.P[action][ind, s] for s in 1:size(mmdp.mdp.P[action], 2)])

        ind2 = findfirst(rand() .< cumprob)
        next_state = mmdp.mdp.S[ind2]

        # println("Next state : ", next_state)

        next_state
    end

################################################################################
# Message structure and trigger  + Memory Arrays
################################################################################
    
    struct Message
        i::Int          # Agent sending the message
        j::Int          # Agent receiving the message
        tij::Float64    # Information tij 
        kij::Int        # Iteration of information tij

        # Blank constructor
        function Message()
            new(0,0,0.0,0)
        end
        # Regular constructor
        function Message(i::Int, j::Int, tij::Float64, kij::Int)
            new(i,j,tij,kij)
        end
    end

    function show(io::IO, mes::Message)
        println(mes.kij, " th message from ", mes.i, " to ", mes.j)
    end

    struct MessageBuffer
        i::Int64
        queue_out::Store{Message}
        buffer::Store{Message}
        mesMDP::MessageQueueMDP

        function MessageBuffer(env::Environment, i::Int64, N_ωi::Int64, learning_agent::Bool)
            queue_out = Store{Message}(env)
            buffer = Store{Message}(env)
            mesMDP = MessageQueueMDP(N_ωi, learning_agent)

            new(i, queue_out, buffer, mesMDP)
        end
    end

    mutable struct memoryArrays
        Nmax::Int
        N_Ω::Int

        prices_array::Array{Float64}
        trade_array::Array{Float64}
        power_array::Array{Float64}
        length_mailbox_array::Array{Int}
        primal_res_array::Array{Float64}
        dual_res_array::Array{Float64}
        message_array::Array{Int64}

        trade::Array{Float64}
        prices::Array{Float64}
        power::Array{Float64}
        length_mailbox::Array{Int64}
        matching_length_mailbox::Array{Int64}
        iteration::Array{Int64}
        primal_res::Array{Float64}
        dual_res::Array{Float64}
        message::Array{Int64}
        MBs::Array{MessageBuffer,1}

        temp::Array{Number,1}
        temp2::Array{Number,1}
        temp3::Array{Number,1}
        
        δt::Float64
        computation_time::Float64
        filename::String

        function memoryArrays(Nmax::Int, N_Ω::Int, warm_start::Dict{String, <:Array{<:Number, 2}}, MBs::Array{MessageBuffer,1})
            T0 = warm_start["T"]
            Λ0 = warm_start["Λ"]

            prices_array = zeros(N_Ω,N_Ω,Nmax)
            trade_array = zeros(N_Ω,N_Ω,Nmax)
            power_array = zeros(N_Ω, Nmax)
            length_mailbox_array = zeros(Int, N_Ω, Nmax)
            primal_res_array = zeros(N_Ω,Nmax)
            dual_res_array = zeros(N_Ω,Nmax)
            message_array = zeros(Int64, N_Ω, N_Ω, Nmax)

            prices_array[:,:,1] = Λ0
            trade_array[:,:,1] = T0
            power_array[:,1] = dropdims(sum(T0, dims=2), dims=2)

            trade = T0                                  # trade[i,j] = tij computed by i
            prices = Λ0                                 # prices[i,j] = λij
            power = dropdims(sum(T0, dims=2), dims=2)   # power[i] = pi
            length_mailbox = zeros(Int, N_Ω)
            matching_length_mailbox = zeros(Int, N_Ω)
            iteration = zeros(Int,N_Ω,N_Ω)# iteration[i,j] = kij
            primal_res = zeros(N_Ω)
            dual_res = zeros(N_Ω)
            message = zeros(Int64, N_Ω, N_Ω)

            temp = Array{Number,1}()
            temp2 = Array{Number,1}()
            temp3 = Array{Number,1}()

            new(Nmax, N_Ω, 
                prices_array, trade_array, power_array, length_mailbox_array, 
                primal_res_array, dual_res_array, message_array,
                trade, prices, power, length_mailbox, matching_length_mailbox,
                iteration, primal_res, dual_res, message, MBs,temp, temp2,temp3,
                0.0, 0.0, "")
        end
    end

    @resumable function messageIn(env::Environment, mb::MessageBuffer, mes::Message, par::algoParams, mem::memoryArrays)
        sender = mes.i
        receiver = mes.j
        kij = mes.kij

        if par.iterMatching == "yesmatching"
            if kij == mem.iteration[receiver, sender]
                @yield put(mb.queue_out, mes)
                # Change MDP state and next action
                length_queue = length(mb.queue_out.items)
                setState!(mb.mesMDP, mb.mesMDP.current_state, length_queue, mb.mesMDP.next_action)
                action = takeAction(mb.mesMDP)
                mb.mesMDP.next_action = action
                updateSARSA!(mb.mesMDP, 0)
            else
                @yield put(mb.buffer, mes)
                action = mb.mesMDP.next_action
            end
        elseif par.iterMatching == "nomatching"
            @yield put(mb.queue_out, mes)
            # Change MDP state and next action
            length_queue = length(mb.queue_out.items)
            setState!(mb.mesMDP, mb.mesMDP.current_state, length_queue, mb.mesMDP.next_action)
            action = takeAction(mb.mesMDP)
            mb.mesMDP.next_action = action
            updateSARSA!(mb.mesMDP, 0)
        else 
            throw(ArgumentError("Uncorrect iterMatching."))
        end

        mem.length_mailbox[receiver] = length(mb.queue_out.items) + length(mb.buffer.items)

        return (action == "trigger")
    end

    @resumable function messageOut(env::Environment, mb::MessageBuffer, mem::memoryArrays)
        # Collect messages
        mes_out = Array{Message,1}()
        iter_info = Array{Tuple{Int, Int},1}()
        n_mes = length(mb.queue_out.items)
        for i in 1:n_mes
            mes = @yield get(mb.queue_out)
            push!(mes_out, mes)
            push!(iter_info, (mes.i, mes.kij))
        end

        # Put buffer messages into queue_out
        n_mes = length(mb.buffer.items)
        buffer = Array{Message,1}()
        for i in 1:n_mes
            mes = @yield get(mb.buffer)
            if (mes.i, mes.kij-1) in iter_info
                @yield put(mb.queue_out, mes)
            else
                push!(buffer, mes)
            end
        end
        for i in 1:length(buffer)   # put back messages in buffer
            @yield put(mb.buffer, buffer[i])
        end

        # Update mdp state
        if mb.mesMDP.next_action != "trigger"
            println("Warning, 'next action' should be 'trigger'!")
        end
        setState!(mb.mesMDP, mb.mesMDP.current_state, length(mb.queue_out.items), mb.mesMDP.next_action)
        action = takeAction(mb.mesMDP)
        mb.mesMDP.next_action = action
        # note: the SARSA update will be done once the proper reward is computed 
        # at the end of the agent's iteration

        mem.length_mailbox[mb.i] = length(mb.queue_out.items) + length(mb.buffer.items)

        mes_out
    end

    @resumable function waitForTrigger(env::Environment, i::Int, algoPar::algoParams)
        ### Next iteration trigger for agent i : 
        ###     has received N = N_trig messages (by interruption)
        ###     OR t ≥ Tmax

        T_max = algoPar.T_max[i]
        @yield timeout(env, T_max)        
    end

    @resumable function sendMessage(env::Environment, i::Int, j::Int,
    tij::Float64, kij::Int, delay::Number, mem::memoryArrays,
    MBs::Array{MessageBuffer}, Trigs::Array{Process}, PauseFlags::Array{Bool}, 
    par::algoParams)
        ### Send message after delay and interrupt agent j's current process 
        ### if max number of message is reached.
        ### Takes into account only messages with iteration matching if 'yesmatching'.
        
            trig_j = Trigs[j]
            mb = MBs[j]

            # Total number of messages sent from i to j 
            mem.message[i,j] = mem.message[i,j] + 1    
            @yield timeout(env, delay)

            trig = @yield @process messageIn(env, mb, Message(i,j,tij, kij), par, mem)

            pause_j = PauseFlags[j]
                if pause_j
                    # println("message received from ", i, ", matching length: ", mem.matching_length_mailbox[j], ", at ", now(env))
                else
                    if trig 
                        mem.matching_length_mailbox[j] = 0
                        @yield SimJulia.interrupt(trig_j)
                    end
                end
    end

################################################################################
# Simulation functions
################################################################################

    @resumable function saveMemory(env::Environment, ΔT::Number, 
    MBs::Array{MessageBuffer,1}, mem::memoryArrays)
        # Save 'trade', 'prices', etc, accross along the simulation,
        # every δT time unit.

        k = 1
        while k ≤ mem.Nmax
            mem.prices_array[:,:,k] = mem.prices
            mem.trade_array[:,:,k] = mem.trade
            mem.power_array[:,k] = mem.power

            mem.primal_res_array[:,k] = mem.primal_res
            mem.dual_res_array[:,k] = mem.dual_res

            mem.length_mailbox_array[:,k] = mem.length_mailbox
            mem.message_array[:,:,k] = mem.message

            mem.MBs = MBs

            k += 1
            @yield timeout(env, ΔT)
        end
    end

    @resumable function market_participant(env::Environment, i::Int,
    Queues::Array{Store{Message}}, Trigs::Array{Process},
    par::power_network, nPar::commParams, algoPar::algoParams, mem::memoryArrays, 
    trig_plan::Array{<:Planification.Trigger,1}, PauseFlags::Array{Bool},
    warm_start::Dict{String, <:Array{<:Number, 1}}, MBs::Array{MessageBuffer})
        #######################################################
        ### Initialization
        #######################################################
        ρ, γ, OSQPeps, solver, sendType, iterMatching,
        priceUpdate, tradeUpdate = _unpack(algoPar)

        Ω = par.Ω                               # Set of all agents
        Ai = par.A[i]                           # Agent i's assets
        ωi = outneighbors(par.comm_graph,i)     # Agent i's market partners
        N_Ω = length(Ω)
        N_Ai = length(Ai)
        N_ωi = length(ωi)

        # tij[j], tji[j]
        tij = warm_start["tij"]
        tji = warm_start["tji"]
        tij_temp = zeros(N_Ω)
        tij_previous = zeros(N_Ω)

        # pi[a]
        Pi = sum(tij)

        # Local iteration
        ki = 0

        # kij[j], kij[j]
        kij = zeros(Int, N_Ω)
        kji = zeros(Int, N_Ω)

        # λij[j]
        λij = warm_start["λij"]

        #######################################################
        ### Pause planification
        #######################################################

        timestamp_pause = [trig for trig in trig_plan 
                                if trig isa Planification.Timestamp]
        localIteration_pause = [trig for trig in trig_plan 
                                if trig isa Planification.LocalIteration]
        tradewiseIteration_pause = [trig for trig in trig_plan 
                                if trig isa Planification.TradewiseIteration]
        localPrimalResidual_pause = [trig for trig in trig_plan 
                                if trig isa Planification.LocalPrimalResidual]
        tradewisePrimalResidual_pause = [trig for trig in trig_plan 
                                if trig isa Planification.TradewisePrimalResidual]
        
        triggerPause = false        # Flag that triggers the agent to pause
        pauseDuration = 0


        #######################################################
        ### First iteration
        #######################################################
        # Local problem resolution
        tij[:] = local_problem_resolution(i, tij, tji, λij, par, PauseFlags, ρ, γ, OSQPeps, solver)

        # Send results to every partners
        for j∈ωi
            delay = sampleDelayValues(i, j, nPar, par)
            @process sendMessage(env, i, j, tij[j], 0,
                delay, mem, MBs, Trigs, PauseFlags, algoPar)
        end
        time3 = 0.0

    
        #######################################################
        ### Agent's process
        #######################################################
        while true
            ki += 1

            # Pause the agent
            if triggerPause
                triggerPause = false        # set the flag back to false
                PauseFlags[i] = true

                time0 = now(env)
                time1 = time0 + pauseDuration
                println("Agent ", i, " pause...")
                # using try/catch to avoid the messages coming 
                # to trigger the iteration, which shouldn't happen!
                while now(env) < time1                  
                    try
                        @yield timeout(env, time1-now(env))
                    catch ex
                        println("Warning, agent ", i,"'s pause has been stopped at ", now(env))
                    end
                end
                PauseFlags[i] = false
            end

            # If the number of received messages exceeds ntrig, no need to wait for the trigger.         
            cond = (MBs[i].mesMDP.next_action == "trigger")

            if cond
                mem.matching_length_mailbox[i] = 0
            else
                # Wait for iteration trigger: number of messages received ntrig
                trig = @process waitForTrigger(env, i, algoPar)
                try
                    time1 = now(env)
                    @yield trig                    
                catch
                    # println("Agent ", i, " interrupted.")
                end
                time2 = now(env)
                
            end

            # Read messages 
            J1 = Array{Int,1}()         # J1 lists all agents id 
                                        # from which we received messages
            messages = Array{Message,1}()
            try 
                messages_process = @process messageOut(env, MBs[i], mem)
                messages = @yield messages_process

                Nmessages = length(messages)

                # Processing each message at a time
                for k in 1:Nmessages
                    mes = messages[k]
                    # Filter incoming messages { j∈ωi/ kij^i = kji^i }
                    if (mes.kij == kij[mes.i]) || (iterMatching == "nomatching") 
                        j = mes.i
                        !(j ∈ ωi) && throw(UnconsistentPartner())
                        push!(J1, j)
                        tji[j] = mes.tij
                        kji[j] = mes.kij
                    elseif (mes.kij != kij[mes.i])
                        throw(UnconsistentIteration())
                    else
                        println("kij = ", kij[mes.i],". Trash ", mes.kij,  "th message from ", mes.i, " to ", mes.j)
                    end
                end
            catch exc
                println("Agent ", i,  " could not retrieve messages.")
                println("   Queue[i]:")
                display(messages)
                display(line)
                rethrow(exc)
            end

            M = unique(J1)      # M = {j∈ωi/ new message from j}
            # println("Agent ", i, " received: ", M, " at time ", now(env))

            # Price update : P ⊂ ωi
            if priceUpdate == "new"
                P = M
            elseif priceUpdate == "global"
                # M = ωi
                P = ωi
                P_id = collect(1:N_ωi)
            end

            # Trade update : T ⊂ ωi
            if tradeUpdate == "new"
                T = M
            elseif tradeUpdate == "global"
                # M = ωi
                T = ωi
                T_id = collect(1:N_ωi)
            end

            # Sending plan : J ⊂ ωi, J = ωi[J_id]
            if sendType == "broadcast"
                J = ωi
                J_id = collect(1:N_ωi)
            elseif sendType == "sendback"
                # J = { j∈ωi/ new tji received}
                J = M
            end

            # Save tij^(k-1)
            tij_previous[:] = tij[:]

            # Save previous primal residual 
            primres_previous = mem.primal_res[i]

            # Prices updates
            λij[:] = lambda_update(P, λij, tij, tji, ρ)

            # Local problem resolution
            tij_temp = local_problem_resolution(i, tij, tji, λij, par, PauseFlags, ρ, γ, OSQPeps, solver)
            tij[T] = tij_temp[T]

            # Iteration number of sending tij from i to j
            kij[J] = kij[J] .+ 1

            # Update mdp statistics
            deltaT = now(env)-time3
            time3 = now(env)
            reward = -(log10(sum([(tij[k]+tji[k])^2  for k∈eachindex(tij)])) - log10(primres_previous))/deltaT
            if -Inf < reward < Inf
                updateReward!(MBs[i].mesMDP, reward)
                updateSARSA!(MBs[i].mesMDP,reward)
            else
                # println("Warning: reward = ±Inf !")
            end

            # println(now(env),"   i:", i, "  reward:", reward)

            # Send results
            for j∈J
                try 
                    delay = sampleDelayValues(i, j, nPar, par)
                    @process sendMessage(env, i, j, tij[j], kij[j], 
                        delay, mem, MBs, Trigs, PauseFlags, algoPar)
                catch exc 
                    println("Agent ", i, " couldn't send message to agent ",
                        j, ", iteration ", kij[j])
                    rethrow(exc)
                end
            end

            # Save results
            mem.trade[i,1:N_Ω] = tij
            mem.prices[i,1:N_Ω] = λij
            mem.power[i] = sum(tij)
            mem.iteration[i,1:N_Ω] = kij
            mem.primal_res[i] = sum([(tij[k]+tji[k])^2  for k∈eachindex(tij)])
            mem.dual_res[i] = sum([(tij[k]-tij_previous[k])^2  for k∈eachindex(tij)])
            if i ==2
                mem.temp = vcat(mem.temp, mem.primal_res[i])
                mem.temp2 = vcat(mem.temp2, Nmessages)
                mem.temp3 = vcat(mem.temp3, reward)
            end

            # Check triggers
            #    Timestamp trigger
            to_remove = Array{Planification.Trigger,1}()
            for trig in timestamp_pause
                if trig.abs_time ≤ now(env)
                    triggerPause = true
                    to_remove = [trig]
                    pauseDuration = trig.timeout
                    break
                end
            end
            setdiff!(timestamp_pause, to_remove)      # Removing this trigger from the list
            
            #    LocalIteration trigger
            to_remove = Array{Planification.Trigger,1}()
            for trig in localIteration_pause
                if trig.k ≤ ki
                    if triggerPause
                        break
                    end
                    triggerPause = true
                    to_remove = [trig]
                    pauseDuration = trig.timeout
                    break
                end
            end
            setdiff!(localIteration_pause, to_remove)      # Removing this trigger from the list

            #    TradewiseIteration trigger
            to_remove = Array{Planification.Trigger,1}()
            for trig in tradewiseIteration_pause
                if trig.k ≤ kij[trig.j]
                    if triggerPause
                        break
                    end
                    triggerPause = true
                    to_remove = [trig]
                    pauseDuration = trig.timeout
                    break
                end
            end
            setdiff!(tradewiseIteration_pause, to_remove)      # Removing this trigger from the list

            #    LocalPrimalResidual trigger
            to_remove = Array{Planification.Trigger,1}()
            for trig in localPrimalResidual_pause
                if trig.ε_trig ≥ mem.primal_res[i]
                    if triggerPause
                        break
                    end
                    triggerPause = true
                    to_remove = [trig]
                    pauseDuration = trig.timeout
                    break
                end
            end
            setdiff!(localIteration_pause, to_remove)      # Removing this trigger from the list

            #    TradewisePrimalResidual trigger
            to_remove = Array{Planification.Trigger,1}()
            for trig in tradewisePrimalResidual_pause
                if trig.ε_trig ≥ (tij[trig.j]+tji[trig.j])^2
                    if triggerPause
                        break
                    end
                    triggerPause = true
                    to_remove = [trig]
                    pauseDuration = trig.timeout
                    break
                end
            end
            setdiff!(tradewisePrimalResidual_pause, to_remove)      # Removing this trigger from the list
        end
    end

    function run_simulation(T_final::Number, δt::Number, par::power_network,
    nPar::commParams, algoPar::algoParams, plan::planStruct)

        # Number of points of the simulation
        Nmax = Int(div(T_final, δt))

        # Number of agents
        N_Ω = length(par.Ω)

        # Simulation environment
        sim = Simulation()

        # Create mailboxes for each agent
        Queues = [ Store{Message}(sim) for i∈par.Ω]

        # Array that saves trigger processes for every agent
        Trigs = Array{Process, 1}(undef, N_Ω)

        # Array that contains the pause flag for every agent
        PauseFlags = fill(false,N_Ω)

        # Array that contains each agent's message trigger MDP
        MBs = [MessageBuffer(sim, i, length(neighbors(par.comm_graph,i)), algoPar.learningAgents[i])  for i in 1:N_Ω]
        for i in 1:N_Ω
            # Initialization : n_trig compliant policy
            n_trig = algoPar.N_trig[i]
            n_max = length(neighbors(par.comm_graph,i))

            trigger_array = zeros(n_max+1)
            wait_array = ones(n_max+1)
            trigger_array[n_trig+1:end] .= 1
            wait_array[n_trig+1:end] .= 0
            
            policy = Dict(
                "trigger" => trigger_array,
                "wait" => wait_array,
                )

            setPolicy!(MBs[i].mesMDP, policy)

            # Q LUT initialization
            (length(algoPar.Q0)≥i) && (size(algoPar.Q0[i],1) != 0) && setQTable!(MBs[i].mesMDP, algoPar.Q0[i])
        end
        # Initilialize arrays
        mem = memoryArrays(Nmax, N_Ω, deepcopy(plan.warm_start), MBs)

        # # Save memory
        @process saveMemory(sim, δt, MBs, mem)

        # Launch process for every agent
        for i∈eachindex(par.Ω)
            trig_list = plan.trigger_list[i]
            warm_start = Dict(
                            "tij"=>plan.warm_start["T"][i,:],
                            "tji"=>plan.warm_start["T"][:,i],
                            "λij"=>plan.warm_start["Λ"][i,:]
                )
            Trigs[i] = @process market_participant(
                sim, i, Queues, Trigs, par, nPar, algoPar, mem, 
                plan.trigger_list[i], PauseFlags,
                warm_start, MBs)
        end
        # println("   Market process launched.")

        # res, t, bytes, gctime, memallocs = @timed run(sim, T_final)
        res, comp_time, bytes, gctime, memallocs = @timed begin
            # while now(sim) < T_final
                try
                    run(sim, T_final)
                catch ex 
                    # (now(sim)==0.0) && println(now(sim))
                    rethrow(ex)
                end
            # end
        end
        mem.δt = δt
        mem.computation_time = comp_time
        mem.filename = string("results/", par.filename[1:end-4],".jld2")
        
        println("   Market process done.")

        return mem
    end

################################################################################
# ADMM functions
################################################################################
    ### Local resolution if there is one asset per agent
    function local_problem_resolution(i::Int,
    tij::Array{Float64}, tji::Array{Float64}, λij::Array{Float64},
    par::power_network, PauseFlags::Array{Bool,1}, 
    ρ::Float64, γ::Number, OSQPeps::Number, solver::String)
        # Solves agent n's local problem :
        #   (pi^k+1, ti^k+1) = argmin sum_a∈Ai( fi^a(p_i^a) )
        #               + sum_j∈ωi( ρ/2( (tij^k - tji^k)/2 - tij + λij^k/ρ)^2 )
        #           s.t.    sum_a∈Ai( pi^a ) = sum_j∈( tij )
        #                   pi^a∈Pi^a
        # !!!! Solve the particular case where the agent is associated to one
        # asset only.
        # Inputs :
        #   tij[j] = tij^k    (j∈[1, N_Ω])
        #   tji[j] = tji^k
        #   λij[j] = λij^k
        # Output :
        #   Ti[j] = tij^k+1   (j∈[1, N_Ω])

        ### Unpacking necessary parameters
        Ai = par.A[i]
        ωi = outneighbors(par.comm_graph,i)
        assets = par.assets
        type = par.type_assets[i]

        N_Ai = length(Ai)
        N_ωi = length(ωi)

        @assert N_Ai == 1 "Only one asset associated with agent i !"


        if N_ωi ≠ 1
            if solver == "OSQP"
                # With trade limitations
                ### Using OSQP to solve the local problem.
                ### OSQP solves quadratic problem that has the form :
                ###         min x'Px + q'x
                ###         s.t. l ≤ Ax ≤ u
                ### with P and A sparse matrix
                
                # Setting OSQP matrix
                # P1 = ones(N_ωi, N_ωi).*2*par.gencost_a[i] + ρ*I###
                P1 = ones(N_ωi, N_ωi).*4*par.gencost_a[i] + ρ*I
                P2 = Matrix(2*γ*I, N_ωi, N_ωi)
                P = sparse(P1+P2)

                q = [par.gencost_b[i] - ρ*(tij[j]-tji[j])/2 - λij[j] for j∈ωi]

                A1 = ones(1, N_ωi)
                A2 = Matrix(1.0I, N_ωi, N_ωi)
                A = sparse(vcat(A1,A2))

                # l1 = fill(par.Pmin[i], N_ωi+1)
                # u1 = fill(par.Pmax[i], N_ωi+1)

                # Set trade limit to zero if partner is paused
                if type == "cons"
                    l = vcat(par.Pmin[i], [if PauseFlags[j] 0.0 else par.Pmin[i] end for j∈ωi])
                    u = vcat(par.Pmax[i], [if PauseFlags[j] 0.0 else 0.0 end for j∈ωi])
                elseif type == "prod"
                    l = vcat(par.Pmin[i], [if PauseFlags[j] 0.0 else 0.0 end for j∈ωi])
                    u = vcat(par.Pmax[i], [if PauseFlags[j] 0.0 else par.Pmax[i] end for j∈ωi])
                end

                # Without trade limitations
                    # A1 = ones(1, N_ωi)
                    # A = sparse(A1)
                    # l = [par.Pmin[i]]
                    # u = [par.Pmax[i]]

                # OSQP resolution
                m = OSQP.Model()    # Creating a quadratic programming optimization model
                OSQP.setup!(m, P=P, q=q,
                    A=A, l=l, u=u, verbose=false,
                    eps_rel = OSQPeps, eps_abs = OSQPeps)   # Model setup with the right arrays
                results = OSQP.solve!(m)            # Solve !
                !(results.info.status == :Solved) && println(results.info.status)
                T_sol = results.x                   # x = Arg min(problem)
            # elseif solver == "Convex"

                # # Suboptimal
                # x = Variable(N_ωi+1)

                # A = ones(Int64,N_ωi, N_ωi)
                # ai = par.gencost_a[i]
                # bi = par.gencost_b[i]
                # tij_k = [tij[j] for j∈ωi]
                # tji_k = [tji[j] for j∈ωi]
                # λij_k = [λij[j] for j∈ωi]
                # Pmin = par.Pmin[i]
                # Pmax = par.Pmax[i]
                # P1 = ones(N_ωi, N_ωi).*4*par.gencost_a[i] + ρ*I
                # P2 = Matrix(γ*I, N_ωi, N_ωi)
                # P = P1+P2

                # expr = 2*ai*square(x[1]) + bi*x[1] + 
                #     γ*sumsquares(x[2:end]) + 
                #     ρ/2*sumsquares(0.5*tij_k-0.5*tji_k-x[2:end]+λij_k/ρ)
                # # expr = 1/2*quadform(x,P)+dot(q,x)

                # constr = [ x[1]-sum(x[2:end])==0, x[1] >= Pmin, x[1] <= Pmax ]

                # problem = minimize(expr, constr)

                # solve!(problem, ECOSSolver(verbose=false))

                # # !(problem.status == :Optimal) && throw(OptimizationError(problem.status))
                # T_sol = x.value[2:end]
            # elseif solver == "JuMP"

                # A = ones(Int64,N_ωi, N_ωi)
                # ai = par.gencost_a[i]
                # bi = par.gencost_b[i]
                # tij_k = [tij[j] for j∈ωi]
                # tji_k = [tji[j] for j∈ωi]
                # λij_k = [λij[j] for j∈ωi]
                # Pmin = par.Pmin[i]
                # Pmax = par.Pmax[i]

                # model = Model(with_optimizer(SCS.Optimizer, verbose=false))
                # # model = Model(with_optimizer(Ipopt.Optimizer, 
                # #     print_level=0), caching_mode = JuMP.MOIU.MANUAL)
                # # set_silent(model)
                # @variable(model, Pmin <= T[i=1:N_ωi] <= Pmax)

                # af_ex = AffExpr(0.0)
                # for j ∈ 1:N_ωi
                #     add_to_expression!(af_ex, 
                #         bi-ρ*(0.5*tij_k[j]-0.5*tji_k[j]+λij_k[j]/ρ), T[j])
                # end
                # qu_ex = QuadExpr(af_ex)
                # for (i,j) ∈ Iterators.product(1:N_ωi, 1:N_ωi)
                #     if i==j
                #         add_to_expression!(qu_ex, γ+ρ/2 + 2*ai, T[i], T[j])
                #     else 
                #         add_to_expression!(qu_ex, 2*ai, T[i], T[j])
                #     end
                # end
                # @objective(model, Min, qu_ex)
                # # @constraint(model, Pmin <= sum(T) <= Pmax)
                # # for j∈1:N_ωi
                # #     @constraint(model, Pmin<= T[j] <= Pmax)
                # # end
                # # adding constraints all at once since some optimizer cannot handle it properly
                # # @constraint(model, con, l.<= A*T .<=u)
                # @constraint(model, Pmin <= sum(T) <= Pmax)

                # optimize!(model)

                # # !(termination_status(model) ∈[MOI.OPTIMAL, MOI.ALMOST_OPTIMAL]) && println(termination_status(model))
                # # !(termination_status(model) ∈[MOI.OPTIMAL, MOI.ALMOST_OPTIMAL]) && error()
                # T_sol = JuMP.value.(T)
            else
                error("Other solvers than OSQP are commented for speed.")
            end
        elseif N_ωi == 1
            ### Simple case where agent i trades with only one other agent
            ### Analytic resolution for speed
            T_sol = []
            for j∈ωi
                t = (-par.gencost_b[i] + ρ*( (tij[j]-tji[j])/2 + λij[j]/ρ )) /
                    (4*par.gencost_a[i] + ρ)
                t = min( par.Pmax[i], max( par.Pmin[i], t))
                push!(T_sol, t)
            end
        end

        Ti = copy(tij)
        Ti[ωi] = T_sol
        return Ti
    end

    ### Global problem (several assets per agent)
        # function local_problem_resolution(i::Int,
        #         tij::Array{Float64}, tji::Array{Float64}, λij::Array{Float64},
        #         par::PowerNetwork.power_network, ρ::Float64)
        #     # Solves agent n's local problem :
        #     #   (pi^k+1, ti^k+1) = argmin sum_a∈Ai( fi^a(p_i^a) )
        #     #               + sum_j∈ωi( ρ/2( (tij^k - tji^k)/2 - tij + λij^k/ρ)^2 )
        #     #           s.t.    sum_a∈Ai( pi^a ) = sum_j∈( tij )
        #     #                   pi^a∈Pi^a
        #     # Inputs :
        #     #   tij[j] = tij^k    (j∈[1, N_Ω])
        #     #   tji[j] = tji^k
        #     #   λij[j] = λij^k
        #     # Output :
        #     #   Pi[a] = (pi^a)^k+1      a∈1:N_Ai
        #     #   Ti[j] = tij^k+1         j∈1:N_ωi
        #
        #     ### Unpacking necessary parameters
        #     Ai = par.A[i]
        #     ωi = par.ω[i]
        #     assets = par.assets
        #     type = par.type_assets[i]
        #
        #     N_Ai = length(Ai)
        #     N_ωi = length(ωi)
        #
        #     ### Using OSQP to solve the local problem.
        #     ### OSQP solves quadratic problem that has the form :
        #     ###         min x'Px + q'x
        #     ###         s.t. l ≤ Ax ≤ u
        #     ### with P and A sparse matrix
        #     # Setting OSQP matrix
        #     p1 = [ par.gencost_a[a] for a∈Ai ]
        #     p2 = fill(ρ/2, N_ωi)
        #     P = spdiagm( 0 => vcat(p1,p2))
        #
        #     q1 = [ par.gencost_b[a] for a∈Ai ]
        #     q2 = [ -ρ*( (tij[j]-tji[j])/2 + λij[j]/ρ ) for j∈ωi]
        #     q = vcat(q1, q2)
        #
        #     ai = vcat(1:N_Ai+N_ωi, fill(N_Ai+N_ωi+1, N_Ai+N_ωi))
        #     aj = vcat(1:N_Ai+N_ωi, 1:N_Ai+N_ωi)
        #     av = ones(2*N_Ai+2*N_ωi)
        #     av[2*N_Ai+N_ωi+1:end] .= -1.0
        #     A = sparse(ai, aj, av)
        #
        #     l1 = [par.Pmin[a] for a∈Ai]
        #     if type == "Prod"
        #         l2 =  fill(0.0, N_ωi)
        #     elseif type == "Cons"
        #         l2 = fill(-Inf, N_ωi)
        #     else
        #         l2 = Vector{Float64}([])
        #         for j∈1:N_ωi
        #             if par.type_assets[ωi[j]] == "Prod"
        #                 push!(l2, -Inf)
        #             else
        #                 push!(l2, 0.0)
        #             end
        #         end
        #     end
        #     l = vcat(l1,l2, 0.0)
        #
        #     u1 = [par.Pmax[a] for a∈Ai]
        #     u2 = (type == "Prod") ? fill(Inf, N_ωi) : fill(0.0, N_ωi)
        #     if type == "Prod"
        #         u2 =  fill(Inf, N_ωi)
        #     elseif type == "Cons"
        #         u2 = fill(0.0, N_ωi)
        #     else
        #         u2 = Vector{Float64}([])
        #         for j∈1:N_ωi
        #             if par.type_assets[ωi[j]] == "Prod"
        #                 push!(u2, 0.0)
        #             else
        #                 push!(u2, Inf)
        #             end
        #         end
        #     end
        #     u = vcat(u1,u2, 0.0)
        #
        #     # OSQP resolution
        #     m = OSQP.Model()    # Creating a quadratic programming optimization model
        #     OSQP.setup!(m, P=P, q=q,
        #         A=A, l=l, u=u, verbose=false)   # Model setup with the right arrays
        #     results = OSQP.solve!(m)            # Solve !
        #
        #     PT_sol = results.x                  # x = Arg min(problem)
        #
        #     Pi = PT_sol[1:N_Ai]
        #     Ti = PT_sol[N_Ai+1:end]
        #     return Pi, Ti
        # end
        #
        # function local_problem_resolution(i::Int,
        #         tij::Array{Float64}, tji::Array{Float64}, λij::Array{Float64},
        #         par::power_network, ρ::Float64)
        #     # Solves agent n's local problem :
        #     #   (pi^k+1, ti^k+1) = argmin sum_a∈Ai( fi^a(p_i^a) )
        #     #               + sum_j∈ωi( ρ/2( (tij^k - tji^k)/2 - tij + λij^k/ρ)^2 )
        #     #           s.t.    sum_a∈Ai( pi^a ) = sum_j∈( tij )
        #     #                   pi^a∈Pi^a
        #     # Inputs :
        #     #   tij[j] = tij^k    (j∈[1, N_ωi])
        #     #   tji[j] = tji^k
        #     #   λij[j] = λij^k
        #     # Output :
        #     #   Pi[a] = (pi^a)^k+1
        #     #   Ti[j] = tij^k+1
        #
        #     ### Unpacking necessary parameters
        #     Ai = par.A[i]
        #     ωi = par.ω[i]
        #     assets = par.assets
        #     type = par.type_assets[i]
        #
        #     N_Ai = length(Ai)
        #     N_ωi = length(ωi)
        #
        #     ### Using OSQP to solve the local problem.
        #     ### OSQP solves quadratic problem that has the form :
        #     ###         min x'Px + q'x
        #     ###         s.t. l ≤ Ax ≤ u
        #     ### with P and A sparse matrix
        #     # Setting OSQP matrix
        #     p1 = [ par.gencost_a[a] for a∈Ai ]
        #     p2 = fill(ρ/2, N_ωi)
        #     P = spdiagm( 0 => vcat(p1,p2))
        #
        #     q1 = [ par.gencost_b[a] for a∈Ai ]
        #     q2 = [ -ρ*( (tij[j]-tji[j])/2 + λij[j]/ρ ) for j∈1:N_ωi]
        #     q = vcat(q1, q2)
        #
        #     ai = vcat(1:N_Ai+N_ωi, fill(N_Ai+N_ωi+1, N_Ai+N_ωi))
        #     aj = vcat(1:N_Ai+N_ωi, 1:N_Ai+N_ωi)
        #     av = ones(2*N_Ai+2*N_ωi)
        #     av[2*N_Ai+N_ωi+1:end] .= -1.0
        #     A = sparse(ai, aj, av)
        #
        #     l1 = [par.Pmin[a] for a∈Ai]
        #     l2 = (type == "Prod") ? fill(0.0, N_ωi) : fill(-Inf, N_ωi)
        #     l = vcat(l1,l2, 0.0)
        #
        #     u1 = [par.Pmax[a] for a∈Ai]
        #     u2 = (type == "Prod") ? fill(Inf, N_ωi) : fill(0.0, N_ωi)
        #     u = vcat(u1,u2, 0.0)
        #
        #     # OSQP resolution
        #     m = OSQP.Model()    # Creating a quadratic programming optimization model
        #     OSQP.setup!(m, P=P, q=q,
        #         A=A, l=l, u=u, verbose=false)   # Model setup with the right arrays
        #     results = OSQP.solve!(m)            # Solve !
        #
        #     PT_sol = results.x                  # x = Arg min(problem)
        #
        #     Pi = PT_sol[1:N_Ai]
        #     Ti = PT_sol[N_Ai+1:end]
        #     return Pi, Ti
        # end

    ### Λ updates
    function lambda_update(λij::Array{Float64},
    tij::Array{Float64}, tji::Array{Float64}, ρ::Float64)
        # Updates all of agent i's λij.
        # Inputs :
        #   tij[j] = tij^k+1    (j∈[1, N_Ω])
        #   tji[j] = tji^k+1
        #   λij[j] = λij^k
        # Output :
        #   λ[j] = λij^k+1
        λ = copy(λij)
        λ[:] = λ[:] - ρ/2*(tij[:]+tji[:])
        return λ
    end

    function lambda_update(J::Array{Int}, λij::Array{Float64},
    tij::Array{Float64}, tji::Array{Float64}, ρ::Float64)
        # Updates all of agent i's λij.
        # Inputs :
        #   J ⊂ [1,N_Ω]
        #   tij[j] = tij^k+1    (j∈[1, N_Ω])
        #   tji[j] = tji^k+1
        #   λij[j] = λij^k
        # Output :
        #   λ[j] = λij^k+1      (j∈[1, N_Ω])
        λ = copy(λij)
        λ[J] = λ[J] - ρ/2*(tij[J]+tji[J])
        return λ
    end

    ### Solve the synchronous version of the prosumer market model.
    function prosumer_market(par::power_network, ρ::Float64)
        Ω = par.Ω
        N_Ω = length(Ω)

        T = zeros(N_Ω, N_Ω)
        Λ = zeros(N_Ω, N_Ω)
        P = zeros(N_Ω)

        tij = zeros(N_Ω)
        tji = zeros(N_Ω)
        λij = zeros(N_Ω)

        for n∈1:100
            for i∈eachindex(Ω)
                ωi = par.ω[i]
                tij = T[i, :]
                tji = T[:, i]
                λij = Λ[i, :]

                Ti = local_problem_resolution(i, tij, tji, λij, par, ρ)
                T[i, ωi] = Ti
            end
            Λ[:,:] = Λ - ρ/2*(T+T')
        end
        return T,Λ
    end
end
