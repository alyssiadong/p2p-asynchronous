﻿module CommunicationDelays
### Functions that return communication delay values depending on the type of
### model chosen : proportionnal to distance (for now).
 
    export commParams, sampleDelayValues
    using LightGraphs, SimpleWeightedGraphs
    using ..PowerNetwork

### Defining a structure to easily pass network link parameters
    struct commParams
        # Delay = (α*Dist + β)*(1 + (σ/3)*randn())
        α::Float64
        β::Float64
        σ::Float64

        function commParams(α::Number, β::Number, σ::Number)
            @assert (σ ≤ 1) && (σ≥0) "Sigma is the relative value of the standard deviation : \n   sigma_AB = meanDelay_AD*(1+sigma/3 * randn() )\n   Hence sigma must be between 0 and 1."
            new(Float64(α), Float64(β), Float64(σ))
        end
    end
    _unpack(x::commParams) = (x.α, x.β, x.σ)     # method to easily unpack commParams

    param = commParams(5.0, 1.0, 0.0)            # Initializing network parameters example


### Communication delay
    function commDelay(distance::Float64, param::commParams)
        ### The value μ of the deterministic models depends on the
        ### distance between two agents : μ = β + α*distance (affine model).
            
        α, β, σ = _unpack(param)   # Delay coefficients
        return max(1e-8, (α*distance + β)*(1+ (σ/3)*randn()) )
    end

    function commDelay(id_exchange::Array{Int}, param::commParams, 
        nParams::PowerNetwork.power_network)
        ### The value μ of the deterministic models depends on the
        ### distance between two agents : μ = β + α*distance (affine model).
        ### Distance is computed from the 2 agents indexes in 'id_exchange'.
            
        # Distance
        dist = distance(id_exchange, nParams)

        # Mean value of the (gaussian part of the) stochastic model
        return commDelay(dist, param)
    end

    function distance(id_exchange::Array{Int}, nParams::PowerNetwork.power_network)
        @assert length(id_exchange) == 2 "Can only compute distance between two agents."

        return nParams.comm_graph.weights[id_exchange[1], id_exchange[2]]
    end

### Group delay values
    function sampleDelayValues(id_agents::Array{Int}, id_master::Int,
        param::commParams, nParams::PowerNetwork.power_network)
        ### Sample one delay value between each agent identified in 'id_agents'
        ### and the master identified by 'id_master'. Deterministic model.

            # Computes the deterministic delay for each agent
            Delays = [commDelay([id_ag, id_master], param, nParams) for id_ag∈id_agents]
            return Delays
    end

### Single delay value
    function sampleDelayValues(id_agent1::Int, id_agent2::Int,
         param::commParams, nParams::PowerNetwork.power_network)
        ### Sample delay value between two agents. Deterministic model.
            Delay = commDelay([id_agent1, id_agent2], param, nParams)
            return Delay
    end

### Minimum delay
    function minimumDelay(param::commParams, nParams::PowerNetwork.power_network)
        ### Computes the minimum delay in the deterministic model, associated
        ### to the smallest distance between two agents that are commercial partners.
            
        comm_distances = nParams.comm_graph.weights.nzval
        dist_min = minimum(comm_distances)

        return commDelay(dist_min, param)
    end

### Mean delay
    function meanDelay(param::commParams, nParams::PowerNetwork.power_network)
        ### Computes the mean delay in the deterministic model between all 
        ### commercial bonds.

        comm_distances = nParams.comm_graph.weights.nzval
        dist_mean = 1/length(comm_distances)*sum(comm_distances)

        return commDelay(dist_mean, param)
    end

end
