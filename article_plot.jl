# include("PowerNetwork.jl")
# include("CommunicationDelays.jl")
# include("market_sim.jl")
using PlotlyJS
using ORCA
using JLD, HDF5
using DataFrames
using Statistics
using Printf
using Colors
using LaTeXStrings

include("write_results.jl")

#########################################################################
### Gamma sweep
#########################################################################

    # JLDfile = WR.JLDFile("n110_p30_c80_0x000f_gammasweep.jld",
    #   "results/n110_p30_c80_0x000f_gammasweep.jld", 
    #   "n110_p30_c80_0x000f.csv")

    # refDict = Dict("delta" => 1.0, "OSQPeps" => 1e-5)
    # # Df = WR.DfCompareTrades(JLDfile, 1e-9, refDict)
    # sort!(Df, :delta, rev=false)
    # Df.loggamma = log10.(Df.gamma)

    # # Reference value: sum(|tij,δ=1,γ|)
    # Eprim_rel2 = 1e-9
    # par = PowerNetwork.power_network("n110_p30_c80_0x000f.csv", "full_P2P")
    # pmin = par.Pmin
    # pmax = par.Pmax
    # optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))        # Needed to compute absolute Epsilon_prim
    # Eprim_abs2 = Eprim_rel2*optimal_power_exchange

    # df = JLDfile.df_params
    # df = df[ (df.delta.==1),:]
    # dic = WR.readAtConvergence(JLDfile, df, ["trades"], Eprim_abs2)

    # Df.tradewise_distance_normalized = zeros(size(Df,1))
    # for g in unique(df.gamma)
    #     nid = df[df.gamma .== g,:NID][1]
    #     t0 = dic[nid]["trades"]
    #     sum0 = sum(abs.(t0[1:30,31:110]))
    #     # println(sum0)

    #     Df[Df.gamma .== g, :tradewise_distance_normalized] = Df[Df.gamma .== g, :compare_trades] ./sum0
    # end

    # m_color =  Dict(
    #         1.0 => "#1f77b4",  # muted blue
    #         0.8 => "#ff7f0e",  # safety orange
    #         0.7 => "#2ca02c",  # cooked asparagus green
    #         0.5 => "#d62728",  # brick red
    #         0.2 => "#9467bd",  # muted purple
    #         # =>'#8c564b',  # chestnut brown
    #         # =>'#e377c2',  # raspberry yogurt pink
    #         # =>'#7f7f7f',  # middle gray
    #         # =>'#bcbd22',  # curry yellow-green
    #         # =>'#17becf'   # blue-teal
    #     )
    # m_symbol = Dict(
    #         1.0 => "circle", 
    #         0.8 => "triangle-down", 
    #         0.7 => "triangle-up", 
    #         0.5 => "diamond",
    #         0.2 => "square",
    #         )

    # data = GenericTrace[]
    # for df in groupby(Df, [:delta])
    #   # if df.delta[1] == 1.0
    #       trace = scatter(x=df.gamma, y=df.objective_function, 
    #           # mode = "lines+markers", 
    #           mode = "markers", 
    #           # line_width = 2,
    #           # marker_size = 5, 
    #           # name = string("OF_delta =", df.delta[1]),
    #           marker = attr(
    #                     symbol = m_symbol[df.delta[1]],
    #                     color = m_color[df.delta[1]],
    #             ),
    #           showlegend = false,
    #           yaxis = "y",
    #           xaxis = "x")
    #       push!(data, trace)
    #   # end
    #   trace = scatter(x=df.gamma, y=df.tradewise_distance_normalized, 
    #       # mode = "lines+markers", 
    #       mode = "markers", 
    #       # line_width = 2,
    #       # marker_size = 5, 
    #       marker = attr(
    #                     symbol = m_symbol[df.delta[1]],
    #                     color = m_color[df.delta[1]],
    #             ),
    #       name = string("δ = ", df.delta[1]),
    #       yaxis = "y2",
    #       xaxis = "x")
    #   push!(data, trace)
    # end

    # layout = Layout(
    #       height = 300,
    #       width = 328,
    #       margin = attr(
    #                       l = 0,
    #                       r = 5,
    #                       t = 5,
    #                       b = 20,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                       x = 0.7,
    #                       y = 0.1,
    #                       font_size = 10,
    #                       font_family = "PT Sans Narrow",
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y2",
    #                       type = "log",
    #                       range = [-4,1],
    #                       title = attr(
    #                                       text = "γ",
    #                                       font_size = 10,
    #                                       font_color = "#444444",
    #                                       font_family = "PT Sans Narrow",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 1,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis2 =attr(   
    #                       domain = [0,0.6],
    #                       # range = [sw_min, sw_max],
    #                       # side = "right",
    #                       # title = attr(
    #                       #               standoff = 80,
    #                       #               text = "Trades comparison", 
    #                       #               font_size = 17,
    #                       #               font_color = "#ff7f0e"
    #                       #   ), 
    #                       # overlaying="y",
    #                       # color = "#ff7f0e",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       # tickfont=attr(
    #                       #             color="#ff7f0e"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       domain = [0.65,1],
    #                       # title = attr(
    #                       #               standoff = 80,
    #                       #               text = "Objective function", 
    #                       #               font_size = 17,
    #                       #               font_color = "#1f77b4"
    #                       #   ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 40000,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = true,
    #                               text = "Global cost value C<sub>δ</sub>",
    #                               font = attr( 
    #                                   size = 11, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = -1.5,
    #                               y = -72500,
    #                           ),
    #                       attr(
    #                               visible = true,
    #                               text = "Trades comparison v<sub>δ</sub>",
    #                               font = attr( 
    #                                   size = 11, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y2",
    #                               x = -1.5,
    #                               y = 0.23,
    #                           ),
    #                   ],
    #       )

    # plot(data, layout)
    # savefig(plot(data, layout), "images/gamma_sweep.pdf", format = "pdf")

#########################################################################
### Influence of communication network state on convergence time
#########################################################################

    # JLDfile = WR.JLDFile("n110_p30_c80_0x000f_alphabeta.jld",
    #     "D:/Julia/n110_p30_c80_0x000f_alphabeta.jld", 
    #     "n110_p30_c80_0x000f.csv")

    # # Lancement et enregistrement des calculs
    # WR.divideAndCompute_SweepConvergence(JLDfile, 
    #   "results/Df_alphabeta_1e-9.jld",
    #   1e-9)

    # Df = load("results/Df_alphabeta_1e-9.jld", "delta0.2")

    # df1 = JLDfile.df_params
    # df2 = df1[(df1.alpha .== 7.5) .& (df1.beta .== 1.5), :]
    # # res = WR.readResidual(JLDfile, df2)
    # res = WR.readAtConvergence(JLDfile, df2, ["trades"], 1e-9)

    # function contours_alphabetasweep(delta::Number, savepdf::Bool = false)

    #     @assert delta ∈ [0.2, 0.4, 0.6, 0.8, 1.0] "Invalid delta."

    #     if delta == 0.2 
    #         d = "0_2"
    #     elseif delta == 0.4
    #         d = "0_4"
    #     elseif delta == 0.6
    #         d = "0_6"
    #     elseif delta == 0.8
    #         d = "0_8"
    #     elseif delta == 1.0
    #         d = "1_0"
    #     end

    #     df = load("results/Df_alphabeta_1e-9.jld", string("delta", delta))

    #     minConv = 0
    #     # maxConv = maximum(Df.conv_time)
    #     maxConv = 3000

    #     # df = Df[Df.delta .== 0.2, :]
    #     # df = Df[Df.conv_time.>60,:]

    #     data = GenericTrace[]
    #     # trace = PlotlyJS.heatmap(;
    #     #                         x = df.alpha, y= df.beta, z = df.conv_time,
    #     #                         xaxis = "x",
    #     #                         yaxis = "y",
    #     #                         zauto = false,
    #     #                         zmin = minConv,
    #     #                         zmax = maxConv,
    #     #                         colorscale = "Portland", 
    #     #                         connectgaps = true,
    #     #                         zsmooth = "best",
    #     #                         # showscale = true,
    #     #                             )
    #     # push!(data, trace)

    #     trace = PlotlyJS.contour(; 
    #                             x = df.alpha, y= df.beta, z = df.conv_time,
    #                             xaxis = "x",
    #                             yaxis = "y",
    #                             # zauto = false,
    #                             # zmin = minConv,
    #                             # zmax = maxConv,
    #                             colorscale = "Portland", 
    #                             contours = Dict(
    #                                         :type => "levels",
    #                                         :coloring => "heatmap",
    #                                         :start => minConv,
    #                                         :end => maxConv, 
    #                                 ),
    #                             line = attr(
    #                                         width = 1,
    #                                         color = "#333333"
    #                                 ),
    #                             showscale = false,
    #                                 )
    #     push!(data, trace)

    #     layout = Layout(
    #                     height = 328/2,
    #                     width = 328/2,
    #                     margin = attr(
    #                                   l = 20,
    #                                   r = 15,
    #                                   b = 20,
    #                                   t = 10
    #                                   ),
    #                     showlegend = false,
    #                     showcolorbar = false,
    #                     xaxis = attr(
    #                                   constraintowards = "bottom",
    #                                   anchor = "y",
    #                                   range = [1.5,30],
    #                                   title = attr(
    #                                                   text = "α",
    #                                                   font_size = 10,
    #                                                   font_color = "#444444",
    #                                                   font_family = "PT Sans Narrow",
    #                                       ), 
    #                                   tickfont_size = 10,
    #                                   tickfont_family = "PT Sans Narrow",
    #                                   ticks = "inside",
    #                                   dtick = 10,
    #                                   showline = true,
    #                                   linecolor= "black",
    #                                   linewidth= 0.5,
    #                                   mirror= "allticks",
    #                                   ),
    #                     yaxis = attr(   
    #                                   # domain = [0.65,1],
    #                                   title = attr(
    #                                                 # standoff = 80,
    #                                                 text = "β", 
    #                                                 font_size = 10,
    #                                                 font_color = "#444444",
    #                                                 font_family = "PT Sans Narrow",
    #                                     ), 
    #                                   range = [0,30],
    #                                   anchor = "x",
    #                                   # color = "#1f77b4",
    #                                   # nticks = 4, 
    #                                   tickfont_size = 10,
    #                                   tickfont_family = "PT Sans Narrow",
    #                                   # tickfont = attr(
    #                                   #             color="#1f77b4"
    #                                   #         ),
    #                                   # automargin = true,
    #                                   ticks = "inside",
    #                                   dtick = 10,
    #                                   showline = true,
    #                                   linecolor= "black",
    #                                   linewidth= 0.5,
    #                                   mirror= "allticks",
    #                                   ),
    #                     # legend = attr(
    #                     #                 x = 0.8,
    #                     #                 y = 0.1,
    #                     #                 font_size = 18,
    #                     #                 font_family = "PT Sans Narrow",
    #                     #   ),
    #                     annotations = [
    #                                     attr(
    #                                             visible = true,
    #                                             text = string("δ = ", delta),
    #                                             font = attr( 
    #                                                 size = 10, 
    #                                                 # color = "black", 
    #                                                 family = "PT Sans Narrow"),
    #                                             showarrow = false,
    #                                             yref = "y",
    #                                             x = 26,
    #                                             y = 28.5,
    #                                         ),
    #                                   ],
    #                     shapes = [
    #                                     attr(
    #                                             visible = true,
    #                                             type = "rect",
    #                                             layer = "above",
    #                                             x0 = 22,
    #                                             x1 = 30,
    #                                             y0 = 27,
    #                                             y1 = 30,
    #                                             fillcolor = "white",
    #                                             line = attr(
    #                                                             width = 0.5,
    #                                                             color = "#444444"
    #                                                 ),
    #                                         ),
    #                                 ],
    #                     )

    #     if savepdf
    #         name = string("images/alphabetasweep_delta", d, ".pdf")
    #         savefig(plot(data, layout), name, format = "pdf")
    #         println(string(name), "  saved.")
    #     end

    #     plot(data, layout)
    # end

    # for d in [0.2, 0.4, 0.6, 0.8, 1.0]
    #     contours_alphabetasweep(d, true)
    # end

    # contours_alphabetasweep(0.8, false)

    # # COLORSCALE
    # Df = load("results/Df_alphabeta_1e-9.jld", "delta0.2")

    # minConv = 0
    # maxConv = 3000

    # # df = Df[Df.delta .== 0.2, :]
    # df = Df[Df.conv_time.>60,:]

    # data = GenericTrace[]
    # trace = PlotlyJS.contour(; 
    #                             x = df.alpha, y= df.beta, z = df.conv_time,
    #                             xaxis = "x",
    #                             yaxis = "y",
    #                             # zauto = false,
    #                             # zmin = minConv,
    #                             # zmax = maxConv,
    #                             colorscale = "Portland", 
    #                             contours = Dict(
    #                                         :type => "levels",
    #                                         :coloring => "heatmap",
    #                                         :start => minConv,
    #                                         :end => maxConv, 
    #                                 ),
    #                             line = attr(
    #                                         width = 1,
    #                                         color = "#333333"
    #                                 ),
    #                             showscale = true,
    #                             colorbar = attr(
    #                                         tickangle = 270,
    #                                         tickfont_family = "PT Sans Narrow",
    #                                         tickfont_size = 10,
    #                                         # tickfont_color = "#333333",
    #                                         thickness = 15,
    #                                         outlinewidth = 0.5,
    #                                 ),
    #                             )
    # push!(data, trace)

    # layout = Layout(
    #           height = 328,
    #           width = 300,
    #           margin = attr(
    #                           l = 0,
    #                           r = 20,
    #                           b = 40,
    #                           t = 10
    #                           ),
    #           showlegend = false,
    #           showcolorbar = true,
    #           legend = attr(
    #                             x = 0.8,
    #                             y = 0.1,
    #                             font_size = 18,
    #                             font_family = "PT Sans Narrow",

    #               ),
    #           xaxis = attr(
    #                           constraintowards = "bottom",
    #                           anchor = "y2",
    #                           title = attr(
    #                                           text = "α",
    #                                           font_size = 20,
    #                                           font_color = "#444444",
    #                                           font_family = "PT Sans Narrow",
    #                               ), 
    #                           tickfont_size = 16,
    #                           tickfont_family = "PT Sans Narrow",
    #                           ),
    #           yaxis = attr(   
    #                           # domain = [0.65,1],
    #                           title = attr(
    #                                         # standoff = 80,
    #                                         text = "β", 
    #                                         font_size = 17,
    #                                         font_color = "#444444"
    #                             ), 
    #                           # color = "#1f77b4",
    #                           nticks = 4, 
    #                           tickfont_size = 16,
    #                           tickfont_family = "PT Sans Narrow",
    #                           # tickfont = attr(
    #                           #             color="#1f77b4"
    #                           #         ),
    #                           automargin = true,
    #                           ),
    #           annotations = [
    #                           attr(
    #                                   visible = false,
    #                                   text = "Objective function",
    #                                   font = attr( 
    #                                       size = 22, 
    #                                       # color = "black", 
    #                                       family = "PT Sans Narrow"),
    #                                   showarrow = false,
    #                                   yref = "y",
    #                                   x = -1.5,
    #                                   y = -73000,
    #                               ),
    #                         ],
    #           )

    # plot(data, layout)
    # savefig(plot(data, layout), "images/colorbar.pdf", format = "pdf")

#########################################################################
### Influence of delta on convergence time: v0
#########################################################################

    # # JLDfile = WR.JLDFile("n110_p30_c80_0x000f_all.jld",
    # #   "D:/Julia/n110_p30_c80_0x000f_all.jld", 
    # #   "n110_p30_c80_0x000f.csv")

    # # Lancement et enregistrement des calculs
    # # WR.divideAndCompute_SweepConvergence(JLDfile, 
    # #     "results/Df_all_1e-9.jld",
    # #     1e-9)

    # # Lecture des résultats
    # deltas = range(0.0, 1.0, length=21)[2:end]
    # sigmas = range(0.0, 1.0, length=10)
    # Df = vcat([load("results/Df_all_1e-9.jld", string("delta",d)) for d in deltas]...)
    # Df2 = by(Df, [:sigma, :delta], 
    #   conv_time_mean = :conv_time => mean, 
    #   sum_messages_mean = :sum_messages => mean,
    #   # conv_time_std = :conv_time => std,
    #   )

    # # MEAN VALUE
    # sig = [sigmas[1], sigmas[4], sigmas[7], sigmas[10]]

    # data = GenericTrace[]
    # for df in groupby(Df2, [:sigma])
    #   if df.sigma[1] in sig
    #       trace = scatter(x=df.delta, y=df.conv_time_mean, 
    #           mode = "markers+lines", 
    #           # marker_size = 6, 
    #           name = string("σ = ", @sprintf("%.2f",df.sigma[1])),
    #           yaxis = "y")
    #       push!(data, trace)
    #   end
    # end

    # layout = Layout(
    #       height = 220,
    #       width = 328,
    #       margin = attr(
    #                       l = 35,
    #                       r = 10,
    #                       t = 10,
    #                       b = 20,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                       x = 0.1,
    #                       y = 0.6,
    #                       font_size = 10,
    #                       font_family = "PT Sans Narrow",
    #           ),
    #       xaxis = attr(
    #                     constraintowards = "bottom",
    #                     range = [0,1.1],
    #                     anchor = "y2",
    #                     title_text = "δ",
    #                     title_font_size = 12,
    #                     title_font_family = "PT Sans Narrow",
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     ticks = "inside",
    #                     tickmode = "auto",
    #                     nticks = 7,
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       yaxis = attr(   
    #                     # domain = [0.5,1],
    #                     title_text = "Convergence time",
    #                     title_font_size = 12,
    #                     title_font_family = "PT Sans Narrow",
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     ticks = "inside",
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = false,
    #                               text = "Mean value of convergence time",
    #                               font = attr( 
    #                                   size = 10, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 0.5,
    #                               y = 630,
    #                           ),
    #                   ],
    #       )
    # plot(data, layout)
    # savefig(plot(data, layout), "images/mean_convtime_deltasweep.pdf", format = "pdf")

    # # VIOLIN PLOT

    # # Lecture des resultats, déjà sauvegardés à la convergence
    # Df = WR.readAtConvergence("D:/Julia/n110_p30_c80_0x000f_deltasweep_new.jld") 
    # sigmas = unique(Df.sigma)
    # deltas = collect(range(0.0, 1.0, length=6)[2:end] )
    # Df2 = Df[map(x-> x∈deltas, Df.delta) ,:]

    # sort!(Df2, :sigma)

    # data = GenericTrace[]
    # for df in groupby(Df2, [:sigma])
    #     # trace = box(;       x = df.delta,
    #     #                     y=df.conv_time, 
    #     #                     name = string("σ = ", df.sigma[1]),
    #     #                     boxmean = true, 
    #     #                     boxpoints = false,
    #     #                     )
    #     # push!(data, trace)

    #     trace = violin(;       x = df.delta,
    #                         y=df.conv_time, 
    #                         name = string("σ = ", df.sigma[1]),
    #                         boxmean = true, 
    #                         boxpoints = false,
    #                         line_width = 1,
    #                         box_visible = false,
    #                         points = false,
    #                         )
    #     push!(data, trace)
    # end

    # layout = Layout(;
    #             height = 150,
    #             width = 328,
    #             margin = attr(
    #                       l = 35,
    #                       r = 10,
    #                       t = 10,
    #                       b = 20,
    #                       ),
    #             legend = attr(
    #                         x = 0.1,
    #                         y = 0.9,
    #                         font_size = 10,
    #                         font_family = "PT Sans Narrow",
    #                 ),
    #             yaxis=attr(
    #                     title = attr(
    #                                     text = "Convergence time",
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "PT Sans Narrow",
    #                         ), 
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     ticks = "inside",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                 ),
    #             xaxis=attr(
    #                     title = attr(
    #                                     text = "δ",
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "PT Sans Narrow",
    #                         ), 
    #                     range = [0,1.1],
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     ticks = "inside",
    #                     tickmode = "auto",
    #                     nticks = 7,
    #                     # gridcolor = "#ddd",
    #                     # gridwidth = 0.8,
    #                 ),
    #             violinmode="group"
    #                 )
    # plot(data, layout)
    # savefig(plot(data, layout), "images/box_plot_1e-9.pdf", format = "pdf")

    # plot(data,layout)
    # # plot(data)

#########################################################################
### Influence of delta on convergence time: v1
#########################################################################

    # # JLDfile = WR.JLDFile("n110_p30_c80_0x000f_all.jld",
    # #   "D:/Julia/n110_p30_c80_0x000f_all.jld", 
    # #   "n110_p30_c80_0x000f.csv")

    # # Lancement et enregistrement des calculs
    # # WR.divideAndCompute_SweepConvergence(JLDfile, 
    # #     "results/Df_all_1e-9.jld",
    # #     1e-9)

    # # Lecture des résultats
    # deltas = range(0.0, 1.0, length=21)[2:end]
    # sigmas = range(0.0, 1.0, length=10)
    # Df = vcat([load("results/Df_all_1e-9.jld", string("delta",d)) for d in deltas]...)
    # Df2 = by(Df, [:sigma, :delta], 
    #   conv_time_mean = :conv_time => mean, 
    #   sum_messages_mean = :sum_messages => mean,
    #   # conv_time_std = :conv_time => std,
    #   )

    # comm_delay_mean = Df.alpha[1] * 1.0 + Df.beta[1]  # median distance≃1.0
    # conv_time_mean_ref = Df2[(Df2.sigma .== 0) .& (Df2.delta .== 1), :conv_time_mean]
    # Df2.conv_time_mean_normalized = Df2.conv_time_mean./conv_time_mean_ref[1]
    # Df2.Rconv = Df2.conv_time_mean ./ comm_delay_mean

    # # MEAN VALUE
    # sig = [sigmas[1], sigmas[4], sigmas[7], sigmas[10]]
    # m_symbol = ["circle", "triangle-down", "triangle-up", "diamond"]
    # k = 0
    # data = GenericTrace[]
    # for df in groupby(Df2, [:sigma])
    #   if df.sigma[1] in sig
    #     global k
    #     k = k+1
    #     trace = scatter(
    #                         x=df.delta, 
    #                         y=df.conv_time_mean_normalized, 
    #                         # y=df.Rconv, 
    #                         mode = "markers", 
    #                         marker = attr(
    #                                     symbol = m_symbol[k],
    #                                     # size = 5,
    #                         ),
    #                         name = string("σ = ", @sprintf("%.2f", df.sigma[1])),
    #                         yaxis = "y")
    #     push!(data, trace)
    #   end
    # end

    # layout = Layout(
    #       height = 220,
    #       width = 328,
    #       margin = attr(
    #                     l = 20,
    #                     r = 10,
    #                     t = 20,
    #                     b = 20,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                     x = 0.14,
    #                     y = 0.6,
    #                     font_size = 10,
    #                     font_family = "PT Sans Narrow",
    #           ),
    #       title = attr(
    #                     text = "Normalized average convergence time: E[ct(δ, σ)]",
    #                     font_family = "PT Sans Narrow",
    #                     font_size = 12,
    #         ),
    #       xaxis = attr(
    #                     constraintowards = "bottom",
    #                     range = [0,1.1],
    #                     anchor = "y2",
    #                     title_text = "δ",
    #                     title_font_size = 12,
    #                     title_font_family = "PT Sans Narrow",
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     ticks = "inside",
    #                     tickmode = "auto",
    #                     nticks = 7,
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       yaxis = attr(   
    #                     # domain = [0.5,1],
    #                     # title_text = "Normalized average convergence time",
    #                     # title_font_size = 12,
    #                     # title_font_family = "PT Sans Narrow",
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     ticks = "inside",
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = false,
    #                               text = "Mean value of convergence time",
    #                               font = attr( 
    #                                   size = 10, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 0.5,
    #                               y = 630,
    #                           ),
    #                   ],
    #       )
    # plot(data, layout)
    # savefig(plot(data, layout), "images/mean_convtime_deltasweep.pdf", format = "pdf")

    # # BOX PLOT

    # # Lecture des resultats, déjà sauvegardés à la convergence
    # Df = WR.readAtConvergence("D:/Julia/n110_p30_c80_0x000f_deltasweep_new.jld") 
    # sigmas = unique(Df.sigma)[1:end-1]
    # deltas = collect(range(0.0, 1.0, length=6)[2:end] )
    # Df2 = Df[map(x-> x∈deltas, Df.delta),:]
    # conv_time_mean_ref = mean(Df2[(Df2.sigma .== 0) .& (Df2.delta .== 1), :conv_time])

    # Df2 = Df2[map(x->x∈sigmas, Df.sigma),:]

    # box_color =  Dict(
    #         # 1.0 => "#1f77b4",  # muted blue
    #         # 0.8 => "#ff7f0e",  # safety orange
    #         # 0.7 => "#2ca02c",  # cooked asparagus green
    #         # 0.5 => "#d62728",  # brick red
    #         # 0.2 => "#9467bd",  # muted purple
    #         0.2 =>"#8c564b",  # chestnut brown
    #         0.8 =>"#e377c2",  # raspberry yogurt pink
    #         # =>'#7f7f7f',  # middle gray
    #         # =>'#bcbd22',  # curry yellow-green
    #         # =>'#17becf'   # blue-teal
    #     )

    # sort!(Df2, :sigma)

    # Df2.centered_conv_time = zeros(size(Df2,1))

    # for (s,d) in Iterators.product(sigmas, deltas)
    #     ids = (Df2.sigma .== s) .& (Df2.delta .== d)
    #     mean_convtime = mean(Df2[ids, :conv_time])
    #     Df2[ids, :centered_conv_time] = Df2[ids,:conv_time] .- mean_convtime
    # end

    # data = GenericTrace[]
    # for df in groupby(Df2, [:sigma])
    #     trace = box(;       
    #                     x = string.("δ = ",df.delta),
    #                     y = df.centered_conv_time ./ conv_time_mean_ref, 
    #                     name = string("σ = ", df.sigma[1]),
    #                     marker_color = box_color[df.sigma[1]],
    #                     boxmean = true, 
    #                     boxpoints = false,
    #                     line_width = 1,
    #                     box_visible = false,
    #                     points = false,
    #                         )
    #     push!(data, trace)
    # end

    # layout = Layout(;
    #             height = 150,
    #             width = 328,
    #             margin = attr(
    #                         l = 25,
    #                         r = 10,
    #                         t = 20,
    #                         b = 20,
    #                       ),
    #             title = attr(
    #                         text = string("Boxplot of centered normalized convergence time: ct'(δ, σ)"),
    #                         font_family = "PT Sans Narrow",
    #                         font_size = 12,
    #             ),
    #             legend = attr(
    #                         x = 0.0,
    #                         y = 1.0,
    #                         font_size = 9,
    #                         font_family = "PT Sans Narrow",
    #                         tracegroupgap = 0,
    #                         bgcolor = "rgba(255,255,255,1)",
    #                         itemsizing = "constant",
    #                 ),
    #             yaxis=attr(
    #                     # title = attr(
    #                     #                 text = "Centered normalized<br>convergence time",
    #                     #                 font_size = 12,
    #                     #                 font_color = "#444444",
    #                     #                 font_family = "PT Sans Narrow",
    #                     #     ), 
    #                     range = [-0.032,0.0445],
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     ticks = "inside",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                 ),
    #             xaxis=attr(
    #                     # title = attr(
    #                     #                 text = "δ",
    #                     #                 font_size = 12,
    #                     #                 font_color = "#444444",
    #                     #                 font_family = "PT Sans Narrow",
    #                     #     ), 
    #                     # type = "category",
    #                     # range = [0,1.1],
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     # ticks = "inside",
    #                     # tickmode = "array",
    #                     # tickvals = unique(Df2.delta),
    #                     # nticks = 5,
    #                     showgrid = false,
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                 ),
    #             boxmode = "group",
    #                 )
    # plot(data, layout)
    # savefig(plot(data, layout), "images/box_plot_1e-9.pdf", format = "pdf")

#########################################################################
### Number of exchanged messages vs convergence time: v1
#########################################################################

    # # JLDfile = WR.JLDFile("n110_p30_c80_0x000f_all.jld",
    # #   "D:/Julia/n110_p30_c80_0x000f_all.jld", 
    # #   "n110_p30_c80_0x000f.csv")

    # # Lancement et enregistrement des calculs
    # # WR.divideAndCompute_SweepConvergence(JLDfile, 
    # #     "results/Df_all_1e-9.jld",
    # #     1e-9)

    # Lecture des résultats
    deltas = range(0.0, 1.0, length=21)[2:end]
    sigmas = range(0.0, 1.0, length=10)
    Df = vcat([load("results/Df_all_1e-9.jld", string("delta",d)) for d in deltas]...)
    Df2 = by(Df, [:sigma, :delta], 
      conv_time_mean = :conv_time => mean, 
      sum_messages_mean = :sum_messages => mean,
      # conv_time_std = :conv_time => std,
      )

    comm_delay_mean = Df.alpha[1] * 0.4153261339134113 + Df.beta[1]
    conv_time_mean_ref = Df2[(Df2.sigma .== 0) .& (Df2.delta .== 1), :conv_time_mean][1]
    comm_links = 30*80
    number_messages_ref = Df2[(Df2.sigma .== 0) .& (Df2.delta .== 1), :sum_messages_mean][1]
    Df2.conv_time_mean_normalized = Df2.conv_time_mean./conv_time_mean_ref
    Df2.Rconv = Df2.conv_time_mean ./ comm_delay_mean

    Df2 = Df2[map(x->x != 1,Df2.delta),:]

    # MEAN VALUE
    sig = [sigmas[1], sigmas[4], sigmas[7], sigmas[10]]
    m_symbol = ["circle", "triangle-down", "triangle-up", "diamond"]
    k = 0
    data = GenericTrace[]
    for df in groupby(Df2, [:sigma])
      if df.sigma[1] in sig
        global k
        k = k+1
        trace = scatter(
                            y=df.sum_messages_mean./number_messages_ref, 
                            x=df.conv_time_mean_normalized, 
                            # y=df.Rconv, 
                            mode = "markers", 
                            marker = attr(
                                        symbol = m_symbol[k],
                                        # size = 5,
                            ),
                            name = string("σ = ", @sprintf("%.2f", df.sigma[1])),
                            yaxis = "y")
        push!(data, trace)
      end
    end

    layout = Layout(
          height = 220,
          width = 328,
          margin = attr(
                        l = 30,
                        r = 10,
                        t = 5,
                        b = 35,
                          ),
          # showlegend = false,
          legend = attr(
                        x = 0.64,
                        y = 0.6,
                        font_size = 10,
                        font_family = "PT Sans Narrow",
              ),
          # title = attr(
          #               text = "Normalized average convergence time",
          #               font_family = "PT Sans Narrow",
          #               font_size = 12,
          #   ),
          xaxis = attr(
                        constraintowards = "bottom",
                        # range = [0,1.1],
                        anchor = "y2",
                        title_text = "Normalized convergence time",
                        title_font_size = 12,
                        title_font_family = "PT Sans Narrow",
                        tickfont_size = 10,
                        tickfont_family = "PT Sans Narrow",
                        zeroline=false,
                        ticks = "inside",
                        tickmode = "auto",
                        nticks = 7,
                        showline = true,
                        linecolor= "black",
                        linewidth= 0.5,
                        mirror= "allticks",
                        gridcolor = "#ddd",
                        gridwidth = 0.8,
                          ),
          yaxis = attr(   
                        # domain = [0.5,1],
                        title_text = "Normalized number of messages",
                        title_font_size = 12,
                        title_font_family = "PT Sans Narrow",
                        tickfont_size = 10,
                        tickfont_family = "PT Sans Narrow",
                        zeroline=false,
                        ticks = "inside",
                        showline = true,
                        linecolor= "black",
                        linewidth= 0.5,
                        mirror= "allticks",
                        gridcolor = "#ddd",
                        gridwidth = 0.8,
                          ),
          annotations = [
                          attr(
                                  visible = false,
                                  text = "Mean value of convergence time",
                                  font = attr( 
                                      size = 10, 
                                      # color = "black", 
                                      family = "PT Sans Narrow"),
                                  showarrow = false,
                                  yref = "y",
                                  x = 0.5,
                                  y = 630,
                              ),
                      ],
          )
    plot(data, layout)
    # savefig(plot(data, layout), "images/messages_convtime.pdf", format = "pdf")

#########################################################################
### Influence of delta on number of exchanged messages: v0
#########################################################################
    # # JLDfile = WR.JLDFile("n110_p30_c80_0x000f_all.jld",
    # #   "D:/Julia/n110_p30_c80_0x000f_all.jld", 
    # #   "n110_p30_c80_0x000f.csv")

    # # # Lancement et enregistrement des calculs
    # # WR.divideAndCompute_SweepConvergence(JLDfile, 
    # #     "results/Df_all_1e-9.jld",
    # #     1e-9)

    # # Lecture des résultats
    # deltas = range(0.0, 1.0, length=21)[2:end]
    # sigmas = range(0.0, 1.0, length=10)
    # Df = vcat([load("results/Df_all_1e-9.jld", string("delta",d)) for d in deltas]...)
    # Df2 = by(Df, [:sigma, :delta], 
    #   conv_time_mean = :conv_time => mean, 
    #   sum_messages_mean = :sum_messages => mean,
    #   # conv_time_std = :conv_time => std,
    #   )

    # # MEAN VALUE
    # sig = [sigmas[1], sigmas[4], sigmas[7], sigmas[10]]

    # data = GenericTrace[]
    # for df in groupby(Df2, [:sigma])
    #   if df.sigma[1] in sig
    #       trace = scatter(x=df.delta, y=df.sum_messages_mean, 
    #           mode = "markers+lines", 
    #           # marker_size = 6, 
    #           name = string("σ = ", @sprintf("%.2f",df.sigma[1])),
    #           yaxis = "y")
    #       push!(data, trace)
    #   end
    # end

    # layout = Layout(
    #       height = 160,
    #       width = 328,
    #       margin = attr(
    #                       l = 40,
    #                       r = 10,
    #                       t = 10,
    #                       b = 25,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                       x = 0.67,
    #                       y = 0.3,
    #                       font_size = 9,
    #                       font_family = "PT Sans Narrow",
    #           ),
    #       xaxis = attr(
    #                     constraintowards = "bottom",
    #                     range = [0,1.1],
    #                     anchor = "y2",
    #                     title_text = "δ",
    #                     title_font_size = 12,
    #                     title_font_family = "PT Sans Narrow",
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     ticks = "inside",
    #                     tickmode = "auto",
    #                     nticks = 7,
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       yaxis = attr(   
    #                     # domain = [0.5,1],
    #                     title_text = "# exchanged messages",
    #                     title_font_size = 12,
    #                     title_font_family = "PT Sans Narrow",
    #                     tickfont_size = 10,
    #                     tickfont_family = "PT Sans Narrow",
    #                     zeroline=false,
    #                     ticks = "inside",
    #                     showline = true,
    #                     linecolor= "black",
    #                     linewidth= 0.5,
    #                     mirror= "allticks",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = false,
    #                               text = "Mean value of convergence time",
    #                               font = attr( 
    #                                   size = 10, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 0.5,
    #                               y = 630,
    #                           ),
    #                   ],
    #       )
    # plot(data, layout)
    # savefig(plot(data, layout), "images/messages_deltasweep.pdf", format = "pdf")

#########################################################################
### Influence of distance over local primal residual:v0
#########################################################################

    # JLDfile = WR.JLDFile( "n110_p30_c80_0x000f_all.jld",
    #     "D:/Julia/n110_p30_c80_0x000f_all.jld",
    #     "n110_p30_c80_0x000f.csv"
    #     )

    # df = JLDfile.df_params
    # sigmas = unique(df.sigma)
    # sig = [sigmas[1], sigmas[4], sigmas[7], sigmas[10]]
    # deltas = unique(df.delta)
    # df = df[(df.delta .== deltas[2]) .& (df.sigma .== sigmas[1]).&(df.tirages .==1),:]

    # Df = WR.DfSweepTradewiseConvergence(JLDfile, 1e-9, df)
    # Df.log_trade_res = log10.(Df.trade_res)


    # ### ALL POINTS
    # data = GenericTrace[]
    # for gf in groupby(Df, :tirages)
    #     trace = scatter( x=gf.distance, y=(gf.trade_res), 
    #         mode = "markers",
    #         marker = attr(
    #                     # size = 2,
    #                     opacity = 1,
    #                     color = "#C4C0B7",
    #             ),
    #         name = string(gf.tirages[1]),
    #         )
    #     push!(data, trace)
    # end

    # ### QUANTILES
    # # Bin separation
    # Nbin = 12
    # dist_min = 0.0
    # dist_max = 2.5
    # dist_range = range(dist_min, dist_max, length = Nbin+1)
    # df_array = map(x->Df[(Df.distance .>= x[1]) .& (Df.distance .<x[2]),:],
    #     zip(dist_range[1:end-1],dist_range[2:end])) #Map an anonymous function that gets every row between two numbers specified by a tuple called x, and map that anonymous function to an array of tuples generated using the zip function.

    # # Quantiles 
    # Nquant = 5      # number of printed quantiles
    # quantRange = range(0,1,length=Nquant)
    # df1 = DataFrame(:quantile_number => 1:Nquant)
    # df2 = DataFrame(:bin_number => 1:Nbin)
    # Df_quant = join(df1, df2, kind = :cross, makeunique = true)
    # Df_quant.bin_meandist = (dist_range[Df_quant.bin_number]+dist_range[Df_quant.bin_number.+1])/2
    # Df_quant.quantile = quantRange[Df_quant.quantile_number]
    # Df_quant.trade_res_quant = zeros(size(Df_quant,1))

    # # Quantiles colors
    # hue = 230
    # l_min = 0.3
    # l_max = 1.2
    # o_min = 0.5
    # o_max = 1
    # lightRange = [(4*(l_max-l_min)*q*(q-1)+l_max)^0.5 for q in quantRange]
    # satRange = [4*(o_min-o_max)*q*(q-1)+o_min for q in quantRange]
    # # colors = [convert(RGBA,HSLA(hue, 0.5, light, opa)) for (light,opa) in zip(lightRange, opacityRange)]
    # colors = [convert(RGB,HSL(hue, sat, light)) for (light,sat) in zip(lightRange, satRange)]
    # Df_quant.colors = colors[Df_quant.quantile_number]
    # Df_quant.lightness = lightRange[Df_quant.quantile_number]

    # # Compute quantiles
    # i = 1
    # for df0 in df_array
    #     global i
    #     for j in 1:length(quantRange)
    #         quant = quantRange[j]
    #         q_res = quantile(df0.log_trade_res, quant)
    #         Df_quant[(Df_quant.bin_number .== i) .& (Df_quant.quantile .== quant),:trade_res_quant] .= q_res
    #     end
    #     i = i + 1
    # end
    # Df_quant.log_trade_res_quant = 10 .^Df_quant.trade_res_quant

    # # Plot quantiles
    # sort!(Df_quant, :lightness, rev=true)
    # for df in groupby(Df_quant, :quantile)
    #     if df.quantile[1] ∈ [quantRange[1], quantRange[end]]
    #         continue
    #     end
    #     trace = scatter(df, 
    #                     x = :bin_meandist, 
    #                     y = :log_trade_res_quant,
    #                     mode = "lines",
    #                     name = string("quantile : ", @sprintf("%.3f",df.quantile[1])),
    #                     line_color = df.colors[1],
    #                     xaxis = "x",
    #                     yaxis = "y",
    #                     line_width = 1.5,
    #                     )
    #     push!(data, trace)
    # end

    # layout = Layout(
    #       # height = 140,
    #       # width = 328/2-5,
    #       margin = attr(
    #                       l = 25,
    #                       r = 10,
    #                       t = 10,
    #                       b = 25,
    #                       ),
    #       showlegend = false,
    #       legend = attr(
    #                       x = 0.1,
    #                       y = 0.6,
    #                       font_size = 10,
    #                       font_family = "PT Sans Narrow",
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y2",
    #                       # range = [0, 2.5],
    #                       title_text = "Distance between agents",
    #                       title_font_size = 10,
    #                       title_font_family = "PT Sans Narrow",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       zeroline=false,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       side = "right",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.5,1],
    #                       # range = [-14, -2],
    #                       range = [0, 350e-9],
    #                       # type = "log",
    #                       title_text = "Tradewise primal residual",
    #                       title_font_size = 10,
    #                       title_font_family = "PT Sans Narrow",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 6,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       side = "top",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = true,
    #                               text = string("δ = ", @sprintf("%.2f",Df.delta[1])),
    #                               font = attr( 
    #                                   size = 8, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 2.2,
    #                               y = -3.2,
    #                           ),
    #                       attr(
    #                               visible = true,
    #                               text = string("σ = ", @sprintf("%.2f",Df.sigma[1])),
    #                               font = attr( 
    #                                   size = 8, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 2.2,
    #                               y = -4,
    #                           ),
    #                     ],
    #       shapes = [
    #                       attr(
    #                               visible = true,
    #                               type = "rect",
    #                               layer = "above",
    #                               x0 = 1.9,
    #                               x1 = 2.5,
    #                               y0 = 3e-5,
    #                               y1 = 0.2e-2,
    #                               fillcolor = "white",
    #                               line_width = 0.5,
    #                           ),
    #                   ],
    #       )
    # plot(data, layout)
    # savefig(plot(data, layout), "images/distance_delta0_1_sigma0.pdf", format = "pdf")

#########################################################################
### Influence of distance over local primal residual:v1
#########################################################################

    # JLDfile = WR.JLDFile( "n110_p30_c80_0x000f_all.jld",
    #     "D:/Julia/n110_p30_c80_0x000f_all.jld",
    #     "n110_p30_c80_0x000f.csv"
    #     )

    # df = JLDfile.df_params
    # sigmas = unique(df.sigma)
    # sig = [sigmas[1], sigmas[4], sigmas[7], sigmas[10]]
    # deltas = unique(df.delta)
    # df = df[(df.delta .== deltas[20]) .& (df.sigma .== sigmas[1]).&(df.tirages .==1),:]

    # Df = WR.DfSweepTradewiseConvergence(JLDfile, 1e-9, df)
    # Df.log_trade_res = log10.(Df.trade_res)
    # Df.distance = Df.distance.*2
    # max_distance = maximum(Df.distance)

    # Df.distance_normalized = Df.distance ./ max_distance

    # ### ALL POINTS
    # data = GenericTrace[]
    # for gf in groupby(Df, :tirages)
    #     trace = scatter( x=gf.distance_normalized, y=(gf.trade_res), 
    #         mode = "markers",
    #         marker = attr(
    #                     size = 2,
    #                     opacity = 1,
    #                     color = "#C4C0B7",
    #             ),
    #         name = string(gf.tirages[1]),
    #         )
    #     push!(data, trace)
    # end

    # ### QUANTILES
    # # Bin separation
    # Nbin = 12
    # dist_min = 0.0
    # dist_max = 1.0
    # dist_range = range(dist_min, dist_max, length = Nbin+1)
    # df_array = map(x->Df[(Df.distance_normalized .>= x[1]) .& (Df.distance_normalized .<x[2]),:],
    #     zip(dist_range[1:end-1],dist_range[2:end])) #Map an anonymous function that gets every row between two numbers specified by a tuple called x, and map that anonymous function to an array of tuples generated using the zip function.

    # # Quantiles 
    # Nquant = 5      # number of printed quantiles
    # quantRange = range(0,1,length=Nquant)
    # df1 = DataFrame(:quantile_number => 1:Nquant)
    # df2 = DataFrame(:bin_number => 1:Nbin)
    # Df_quant = join(df1, df2, kind = :cross, makeunique = true)
    # Df_quant.bin_meandist = (dist_range[Df_quant.bin_number]+dist_range[Df_quant.bin_number.+1])/2
    # Df_quant.quantile = quantRange[Df_quant.quantile_number]
    # Df_quant.trade_res_quant = zeros(size(Df_quant,1))

    # # Quantiles colors
    # hue = 230
    # l_min = 0.3
    # l_max = 1.2
    # o_min = 0.5
    # o_max = 1
    # lightRange = [(4*(l_max-l_min)*q*(q-1)+l_max)^0.5 for q in quantRange]
    # satRange = [4*(o_min-o_max)*q*(q-1)+o_min for q in quantRange]
    # # colors = [convert(RGBA,HSLA(hue, 0.5, light, opa)) for (light,opa) in zip(lightRange, opacityRange)]
    # colors = [convert(RGB,HSL(hue, sat, light)) for (light,sat) in zip(lightRange, satRange)]
    # Df_quant.colors = colors[Df_quant.quantile_number]
    # Df_quant.lightness = lightRange[Df_quant.quantile_number]

    # # Compute quantiles
    # i = 1
    # for df0 in df_array
    #     global i
    #     for j in 1:length(quantRange)
    #         quant = quantRange[j]
    #         q_res = quantile(df0.log_trade_res, quant)
    #         Df_quant[(Df_quant.bin_number .== i) .& (Df_quant.quantile .== quant),:trade_res_quant] .= q_res
    #     end
    #     i = i + 1
    # end
    # Df_quant.log_trade_res_quant = 10 .^Df_quant.trade_res_quant

    # # Plot quantiles
    # sort!(Df_quant, :lightness, rev=true)
    # for df in groupby(Df_quant, :quantile)
    #     if df.quantile[1] ∈ [quantRange[1], quantRange[end]]
    #         continue
    #     end
    #     trace = scatter(df, 
    #                     x = :bin_meandist, 
    #                     y = :log_trade_res_quant,
    #                     mode = "lines",
    #                     name = string("quantile : ", @sprintf("%.3f",df.quantile[1])),
    #                     line_color = df.colors[1],
    #                     xaxis = "x",
    #                     yaxis = "y",
    #                     line_width = 1.5,
    #                     )
    #     push!(data, trace)
    # end

    # layout = Layout(
    #       height = 140,
    #       width = 328/2-5,
    #       margin = attr(
    #                       l = 25,
    #                       r = 10,
    #                       t = 10,
    #                       b = 25,
    #                       ),
    #       showlegend = false,
    #       legend = attr(
    #                       x = 0.1,
    #                       y = 0.6,
    #                       font_size = 10,
    #                       font_family = "PT Sans Narrow",
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y2",
    #                       range = [0, 1.0],
    #                       title_text = "Normalized distance between agents",
    #                       title_font_size = 10,
    #                       title_font_family = "PT Sans Narrow",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       zeroline=false,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       side = "right",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.5,1],
    #                       # range = [-14, -2],
    #                       range = [0, 350e-9],
    #                       # type = "log",
    #                       title_text = "Tradewise primal residual",
    #                       title_font_size = 10,
    #                       title_font_family = "PT Sans Narrow",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       tickangle = 90+180,
    #                       # dtick = 6,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       side = "top",
    #                     gridcolor = "#ddd",
    #                     gridwidth = 0.8,
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = true,
    #                               text = string("δ = ", @sprintf("%.2f",Df.delta[1])),
    #                               font = attr( 
    #                                   size = 8, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 2.2,
    #                               y = -3.2,
    #                           ),
    #                       attr(
    #                               visible = true,
    #                               text = string("σ = ", @sprintf("%.2f",Df.sigma[1])),
    #                               font = attr( 
    #                                   size = 8, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 2.2,
    #                               y = -4,
    #                           ),
    #                     ],
    #       shapes = [
    #                       attr(
    #                               visible = true,
    #                               type = "rect",
    #                               layer = "above",
    #                               x0 = 1.9,
    #                               x1 = 2.5,
    #                               y0 = 3e-5,
    #                               y1 = 0.2e-2,
    #                               fillcolor = "white",
    #                               line_width = 0.5,
    #                           ),
    #                   ],
    #       )
    # plot(data, layout)
    # savefig(plot(data, layout), "images/distance_delta1_0_sigma0.pdf", format = "pdf")


   